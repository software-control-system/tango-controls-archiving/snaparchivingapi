package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr2Val;

public class ScStr2ValDAOTest extends AbstractValDAOTest<ScStr2Val> {
	@Autowired
	@Qualifier("scStr2ValDAO")
	protected ValDAO<ScStr2Val> dao;

    @Override
    protected void compare(ScStr2Val original, ScStr2Val copy) {
        super.compare(original, copy);
		assertEquals("shoudl be equals", original.getReadValue(), copy.getReadValue());
		assertEquals("shoudl be equals", original.getWriteValue(), copy.getWriteValue());
    }

    @Override
    protected ScStr2Val buildLine() {
        ScStr2Val line = new ScStr2Val();

        CompositeId compositeId = new CompositeId();
        int idAtt = 999;
        int idSnap = 888;
        compositeId.setIdAtt(idAtt);
        compositeId.setIdSnap(idSnap);
        line.setCompositeId(compositeId);

        String readValue = "ScStr2Val example readValue";
        String writeValue = "ScStr2Val example writeValue";
        line.setReadValue(readValue);
        line.setWriteValue(writeValue);

        return line;
    }


    @Override
    protected ScStr2Val[] buildEmptyLines(int numberOfLines) {
        return new ScStr2Val[numberOfLines];
    }

	@Override
	protected ValDAO<ScStr2Val> getDao() {
		return dao;
	}
}
