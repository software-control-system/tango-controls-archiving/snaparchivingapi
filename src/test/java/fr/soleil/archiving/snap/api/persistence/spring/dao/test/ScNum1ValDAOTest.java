package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScNum1Val;

public class ScNum1ValDAOTest extends AbstractValDAOTest<ScNum1Val> {

	@Autowired
	@Qualifier("scNum1ValDAO")
	protected ValDAO<ScNum1Val> dao;

    @Override
    protected void compare(ScNum1Val original, ScNum1Val copy) {
        super.compare(original, copy);
		assertEquals(original.getValue(), copy.getValue(), 0);
    }

    @Override
    protected ScNum1Val buildLine() {
        ScNum1Val line = new ScNum1Val();

        CompositeId compositeId = new CompositeId();
        int idAtt = 999;
        int idSnap = 888;
        compositeId.setIdAtt(idAtt);
        compositeId.setIdSnap(idSnap);
        line.setCompositeId(compositeId);

        double value = 1.234;
        line.setValue(value);

        return line;
    }



    @Override
    protected ScNum1Val[] buildEmptyLines(int numberOfLines) {
        return new ScNum1Val[numberOfLines];
    }

	@Override
	protected ValDAO<ScNum1Val> getDao() {
		return dao;
	}

	@Test
	public void testFetchFromBdd() {
		CompositeId compositeId = new CompositeId();
		int idAtt = 245;
		int idSnap = 235;
		compositeId.setIdAtt(idAtt);
		compositeId.setIdSnap(idSnap);
		ScNum1Val val = getDao().findByKey(compositeId);
		assertEquals(val.getValue(), 23.0, 0);
	}
}
