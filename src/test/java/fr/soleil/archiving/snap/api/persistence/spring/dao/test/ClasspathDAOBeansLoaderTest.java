package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ClasspathDAOBeansLoader;
import fr.soleil.archiving.snap.api.persistence.spring.dao.DAOBeansLoader;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr1Val;


@ContextConfiguration({ "classpath:beans.xml" })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ClasspathDAOBeansLoaderTest {

	@Test
	public void construtorTest() {
		DAOBeansLoader beans = new ClasspathDAOBeansLoader(null, "snap", "snap");
		assertNotNull(beans.getScStr1ValDAO());
		ScStr1Val line = new ScStr1Val();

		CompositeId compositeId = new CompositeId();
		int idAtt = 245;
		int idSnap = 235;
		compositeId.setIdAtt(idAtt);
		compositeId.setIdSnap(idSnap);
		line.setCompositeId(compositeId);

		String value = "ScStr1Val example value";
		line.setValue(value);

		line = beans.getScStr1ValDAO().create(line);
		assertNotNull(beans.getScStr1ValDAO().findByKey(compositeId));
		beans.getScStr1ValDAO().delete(line);
		assertNull(beans.getScStr1ValDAO().findByKey(compositeId));
	}

}
