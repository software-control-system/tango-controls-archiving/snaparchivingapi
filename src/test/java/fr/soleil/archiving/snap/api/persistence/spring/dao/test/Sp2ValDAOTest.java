package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Sp2Val;

public class Sp2ValDAOTest extends AbstractValDAOTest<Sp2Val> {

	@Autowired
	@Qualifier("sp2ValDAO")
	protected ValDAO<Sp2Val> dao;

    @Override
    protected void compare(Sp2Val original, Sp2Val copy) {
        super.compare(original, copy);
		assertEquals(original.getReadValue(), copy.getReadValue());
		assertEquals(original.getWriteValue(), copy.getWriteValue());
		assertEquals(original.getDimX(), copy.getDimX());
    }

    @Override
    protected Sp2Val buildLine() {
        Sp2Val line = new Sp2Val();

        CompositeId compositeId = new CompositeId();
        int idAtt = 999;
        int idSnap = 888;
        compositeId.setIdAtt(idAtt);
        compositeId.setIdSnap(idSnap);
        line.setCompositeId(compositeId);

        String readValue = "A,B,C";
        String writeValue = "D";
        line.setReadValue(readValue);
        line.setWriteValue(writeValue);

        line.setDimX(3);

        return line;
    }

    @Override
    protected Sp2Val[] buildEmptyLines(int numberOfLines) {
        return new Sp2Val[numberOfLines];
    }

	@Override
	protected ValDAO<Sp2Val> getDao() {
		return dao;
	}
}
