package fr.soleil.archiving.snap.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.SnapshotLight;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.lib.project.ObjectUtils;

public class DataBaseAPITest extends AbstractTest {
    DataBaseAPI dbApi;

    @Before
    public void initDataBaseAPI() throws ArchivingException {
        DataBaseParameters parameters = new DataBaseParameters();
        parameters.setHost(ObjectUtils.EMPTY_STRING);
        parameters.setName("public");
        parameters.setSchema("public");
        parameters.setUser("snap");
        parameters.setPassword("snap");
        dbApi = new DataBaseAPI(parameters);
    }

    @Test
    public void getAttIDTest() throws SnapshotingException {
        assertFalse(dbApi.isRegistered(ObjectUtils.EMPTY_STRING));
        assertTrue(dbApi.isRegistered("tango/tangotest/1/short_scalar_ro"));
        int id = dbApi.getAttID("tango/tangotest/1/short_scalar_ro");
        assertEquals(245, id);
    }

    @Test
    public void getMaxContextIDTest() throws SnapshotingException {
        assertTrue(dbApi.getMaxContextID() > 0);
    }

    @Test
    public void getContextTest() throws SnapshotingException {
        List<SnapContext> result = dbApi.getContext(" WHERE id_context = ? ", 12);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = dbApi.getContext(" WHERE id_context = ? ", 76);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());

        result = dbApi.getContext(" WHERE name LIKE 'test%' ", -1);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(5, result.size());

        result = dbApi.getContext(ObjectUtils.EMPTY_STRING, -1);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(6, result.size());
    }

    @Test
    public void getContextAssociatedSnapshotsTest() throws SnapshotingException {
        List<SnapshotLight> result = dbApi.getContextAssociatedSnapshots(ObjectUtils.EMPTY_STRING, -1, -1);
        assertFalse(result.isEmpty());
        assertEquals(31, result.size());

        result = dbApi.getContextAssociatedSnapshots(" WHERE ID_CONTEXT=? ", 76, -1);
        assertFalse(result.isEmpty());
        assertEquals(28, result.size());

        result = dbApi.getContextAssociatedSnapshots(" WHERE ID_SNAP=? AND ID_CONTEXT=?", 76, 228);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());

        result = dbApi.getContextAssociatedSnapshots(" WHERE ID_SNAP=? ", -1, 228);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
    }

    @Test
    public void getContextIDTest() throws SnapshotingException {
        int result = dbApi.getContextID(-1);
        assertEquals(-1, result);

        result = dbApi.getContextID(228);
        assertEquals(76, result);
    }

    @Test
    public void getSnapResultsTest() throws SnapshotingException {
        int id_snap = 237;
        final int contextID = dbApi.getContextID(id_snap);
        final List<SnapAttributeExtract> theoricList = dbApi.getContextAssociatedAttributes(contextID);
        final List<SnapAttributeExtract> result = new ArrayList<SnapAttributeExtract>(theoricList);
        SnapAttributeExtract scRO = null;
        SnapAttributeExtract scROstr = null;
        SnapAttributeExtract scRW = null;
        SnapAttributeExtract spRO = null;
        SnapAttributeExtract spRW = null;

        for (SnapAttributeExtract item : result) {
            if (item.getDataFormat() == AttrDataFormat._SCALAR) {
                if (item.getWritable() == AttrWriteType._READ && item.getDataType() != TangoConst.Tango_DEV_STRING
                        && scRO == null
                        && !item.getAttributeCompleteName().equals("tango/tangotest/titan/throw_exception")) {
                    scRO = item;
                }
                if (item.getWritable() == AttrWriteType._READ && item.getDataType() == TangoConst.Tango_DEV_STRING
                        && scROstr == null) {
                    scROstr = item;
                }
                if (item.getWritable() == AttrWriteType._READ_WRITE && scRW == null) {
                    scRW = item;
                }
            }
            if (item.getDataFormat() == AttrDataFormat._SPECTRUM) {
                if (item.getWritable() == AttrWriteType._READ && spRO == null) {
                    spRO = item;
                }
                if (item.getWritable() == AttrWriteType._READ_WRITE && spRW == null) {
                    spRW = item;
                }
            }
        }
        assertNull(scRO.getValue());
        assertNull(scROstr.getValue());
        assertNull(scRW.getReadValue());
        assertNull(spRO.getValue());
        assertEquals("NaN", spRW.getReadValue());
        dbApi.getSnapResults(result, id_snap);
        assertNotNull(scRO.getValue());
        assertNotNull(scROstr.getValue());
        assertNotNull(scRW.getReadValue());
        assertNotNull(spRO.getValue());
        assertNotEquals("NaN", spRW.getReadValue());
    }

}
