package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Sp1Val;

public class Sp1ValDAOTest extends AbstractValDAOTest<Sp1Val> {

	@Autowired
	@Qualifier("sp1ValDAO")
	protected ValDAO<Sp1Val> dao;

    @Override
    protected void compare(Sp1Val original, Sp1Val copy) {
        super.compare(original, copy);
		assertEquals(original.getValue(), copy.getValue());
		assertEquals(original.getDimX(), copy.getDimX());
    }

    @Override
    protected Sp1Val buildLine() {
        Sp1Val line = new Sp1Val();

        CompositeId compositeId = new CompositeId();
        int idAtt = 999;
        int idSnap = 888;
        compositeId.setIdAtt(idAtt);
        compositeId.setIdSnap(idSnap);
        line.setCompositeId(compositeId);

        String value = "A,B,C";
        line.setValue(value);

        line.setDimX(3);

        return line;
    }

    @Override
    protected Sp1Val[] buildEmptyLines(int numberOfLines) {
        return new Sp1Val[numberOfLines];
    }

	@Override
	protected ValDAO<Sp1Val> getDao() {
		return dao;
	}
}
