package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import fr.soleil.archiving.snap.api.AbstractTest;
import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Val;


public abstract class AbstractValDAOTest<V extends Val> extends AbstractTest {

    protected void compare(V original, V copy) {
		assertNotNull(copy);
    }

    protected V[] buildLines() {
        int numberOfLines = 1;
        V[] lines = this.buildEmptyLines(numberOfLines);

        V line = this.buildLine();

        for (int i = 0; i < numberOfLines; i++) {
            lines[i] = line;
        }

        return lines;
    }

    protected abstract V buildLine();

	protected abstract ValDAO<V> getDao();

    protected abstract V[] buildEmptyLines(int numberOfLines);

	@Test
	public void testInsert() {
		V[] lines = this.buildLines();
		for (int i = 0; i < lines.length; i++) {
			getDao().create(lines[i]);
			V inDB = getDao().findByKey(lines[i].getCompositeId());

			this.compare(lines[i], inDB);
		}
	}
}
