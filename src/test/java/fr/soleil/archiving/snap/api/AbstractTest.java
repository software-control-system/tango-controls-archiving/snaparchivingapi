package fr.soleil.archiving.snap.api;

import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Rollback(true)
@ActiveProfiles({ "test" })
@ContextConfiguration({ "classpath:beans.xml" })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractTest {

}
