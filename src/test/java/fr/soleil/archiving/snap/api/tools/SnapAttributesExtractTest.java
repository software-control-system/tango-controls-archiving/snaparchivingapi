package fr.soleil.archiving.snap.api.tools;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.lib.project.ObjectUtils;

public class SnapAttributesExtractTest {
    @Test
    public void ScalareToStingTest() {

        testReadWrite(99, AttrDataFormat._SCALAR, "0.0", "1.0", "NULL");
        testReadWrite(TangoConst.Tango_DEV_STRING, AttrDataFormat._SCALAR, "read", "write", "NULL");
        testReadWrite(TangoConst.Tango_DEV_STATE, AttrDataFormat._SCALAR, "0", "1", "NULL");
        testReadWrite(TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SCALAR, "0", "1", "NULL");
        testReadWrite(TangoConst.Tango_DEV_LONG, AttrDataFormat._SCALAR, "0", "1", "NULL");
        testReadWrite(TangoConst.Tango_DEV_ULONG, AttrDataFormat._SCALAR, "0", "1", "NULL");
        testReadWrite(TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SCALAR, "true", "false", "NULL");
        testReadWrite(TangoConst.Tango_DEV_SHORT, AttrDataFormat._SCALAR, "0", "1", "NULL");
        testReadWrite(TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SCALAR, "0.0", "1.0", "NULL");
        testReadWrite(TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SCALAR, "0.0", "1.0", "NULL");

    }

    @Test
    public void SpectrumToStringTest() {
        testReadWrite(TangoConst.Tango_DEV_STRING, AttrDataFormat._SPECTRUM, "[test,test]", "[test]", "[]");
        testReadWrite(TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SPECTRUM, "[0]", "[1]", "[]");
        testReadWrite(TangoConst.Tango_DEV_LONG, AttrDataFormat._SPECTRUM, "[0]", "[1]", "[]");
        testReadWrite(TangoConst.Tango_DEV_ULONG, AttrDataFormat._SPECTRUM, "[0]", "[1]", "[]");
        testReadWrite(TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SPECTRUM, "[true]", "[false]", "[]");
        testReadWrite(TangoConst.Tango_DEV_SHORT, AttrDataFormat._SPECTRUM, "[0]", "[1]", "[]");
        testReadWrite(TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SPECTRUM, "[0.0]", "[1.0]", "[]");
        testReadWrite(TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SPECTRUM, "[0.0]", "[1.0]", "[]");

        testReadWrite(TangoConst.Tango_DEV_STRING, AttrDataFormat._SPECTRUM, "[test,test,test3]", "[test,test2]", "[]");
        testReadWrite(TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SPECTRUM, "[0,1]", "[1,0]", "[]");
        testReadWrite(TangoConst.Tango_DEV_LONG, AttrDataFormat._SPECTRUM, "[0,1]", "[1,0]", "[]");
        testReadWrite(TangoConst.Tango_DEV_ULONG, AttrDataFormat._SPECTRUM, "[0,1]", "[1,0]", "[]");
        testReadWrite(TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SPECTRUM, "[true,false]", "[false,true]", "[]");
        testReadWrite(TangoConst.Tango_DEV_SHORT, AttrDataFormat._SPECTRUM, "[0,1]", "[1,0]", "[]");
        testReadWrite(TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SPECTRUM, "[0.0,1.0]", "[1.0,2.0]", "[]");
        testReadWrite(TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SPECTRUM, "[0.0,1.0]", "[1.0,2.0]", "[]");
    }

    private void testReadWrite(int dataType, int dataFormat, String readStringValue, String writeStringValue,
            String emptyValue) {
        String[] args = { "test", "0", ObjectUtils.EMPTY_STRING + dataType, ObjectUtils.EMPTY_STRING + dataFormat,
                ObjectUtils.EMPTY_STRING, readStringValue, writeStringValue };
        args[4] = ObjectUtils.EMPTY_STRING + AttrWriteType._READ;
        SnapAttributeExtract sae = new SnapAttributeExtract(args);
        assertEquals(readStringValue, sae.valueToString(0));
        assertEquals(emptyValue, sae.valueToString(1));

        args[4] = ObjectUtils.EMPTY_STRING + AttrWriteType._WRITE;
        sae = new SnapAttributeExtract(args);
        assertEquals(emptyValue, sae.valueToString(0));
        assertEquals(writeStringValue, sae.valueToString(1));

        args[4] = ObjectUtils.EMPTY_STRING + AttrWriteType._READ_WITH_WRITE;
        sae = new SnapAttributeExtract(args);
        assertEquals(readStringValue, sae.valueToString(0));
        assertEquals(writeStringValue, sae.valueToString(1));

        args[4] = ObjectUtils.EMPTY_STRING + AttrWriteType._READ_WRITE;
        sae = new SnapAttributeExtract(args);
        assertEquals(readStringValue, sae.valueToString(0));
        assertEquals(writeStringValue, sae.valueToString(1));
    }
}
