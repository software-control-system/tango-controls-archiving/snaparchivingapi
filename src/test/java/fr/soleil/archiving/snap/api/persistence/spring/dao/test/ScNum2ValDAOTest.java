package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScNum2Val;

public class ScNum2ValDAOTest extends AbstractValDAOTest<ScNum2Val> {

	@Autowired
	@Qualifier("scNum2ValDAO")
	protected ValDAO<ScNum2Val> dao;

    @Override
    protected void compare(ScNum2Val original, ScNum2Val copy) {
        super.compare(original, copy);
		assertEquals(original.getReadValue(), copy.getReadValue(), 0);
		assertEquals(original.getWriteValue(), copy.getWriteValue(), 0);
    }

    @Override
    protected ScNum2Val buildLine() {
        ScNum2Val line = new ScNum2Val();

        CompositeId compositeId = new CompositeId();
        int idAtt = 999;
        int idSnap = 888;
        compositeId.setIdAtt(idAtt);
        compositeId.setIdSnap(idSnap);
        line.setCompositeId(compositeId);

        double readValue = 1.234;
        double writeValue = 5.678;
        line.setReadValue(readValue);
        line.setWriteValue(writeValue);

        return line;
    }

    @Override
    protected ScNum2Val[] buildEmptyLines(int numberOfLines) {
        return new ScNum2Val[numberOfLines];
    }

	@Override
	protected ValDAO<ScNum2Val> getDao() {
		return dao;
	}
}
