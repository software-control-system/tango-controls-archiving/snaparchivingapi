package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.soleil.archiving.snap.api.model.Snapshot;
import fr.soleil.archiving.snap.api.persistence.spring.dao.SnapshotDAO;

@Rollback(true)
@ActiveProfiles({ "test" })
@ContextConfiguration({ "classpath:beans.xml" })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class SnapshotDAOTest {
	@Autowired
	SnapshotDAO dao;

	@Test
	public void findTest() {
		List<Snapshot> s = dao.findAll();
		for (Snapshot item : s) {
			System.out.println(item.getIdSnap() + " " + item.getIdContext() + " " + item.getTime());
		}
		assertNotNull(s);
	}

}
