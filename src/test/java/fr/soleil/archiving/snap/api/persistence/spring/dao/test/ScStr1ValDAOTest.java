package fr.soleil.archiving.snap.api.persistence.spring.dao.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.soleil.archiving.snap.api.persistence.spring.dao.ValDAO;
import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr1Val;


public class ScStr1ValDAOTest extends AbstractValDAOTest<ScStr1Val> {

	@Autowired
	@Qualifier("scStr1ValDAO")
	protected ValDAO<ScStr1Val> dao;


    @Override
    protected void compare(ScStr1Val original, ScStr1Val copy) {
        super.compare(original, copy);
		assertEquals("shoudl be equal", original.getValue(), copy.getValue());
    }

    @Override
    protected ScStr1Val buildLine() {
        ScStr1Val line = new ScStr1Val();

        CompositeId compositeId = new CompositeId();
        int idAtt = 999;
        int idSnap = 888;
        compositeId.setIdAtt(idAtt);
        compositeId.setIdSnap(idSnap);
        line.setCompositeId(compositeId);

        String value = "ScStr1Val example value";
        line.setValue(value);

        return line;
    }


    @Override
    protected ScStr1Val[] buildEmptyLines(int numberOfLines) {
        return new ScStr1Val[numberOfLines];
    }

	@Override
	protected ValDAO<ScStr1Val> getDao() {
		return dao;
	}
}
