package fr.soleil.actiongroup.collectiveaction.components.singleaction.atomic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.actiongroup.collectiveaction.components.singleaction.AbstractIndividualAction;
import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.MinimalistActionListener;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.ActionResult;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target.Target;

/**
 * Reads attributes from a device. Notifies its listener on completion.
 * 
 * @author CLAISSE
 */
public abstract class AtomicIndividualAction extends AbstractIndividualAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtomicIndividualAction.class);

    /**
     * @param listener The listener to notify on read completion
     * @param target The device to read from
     */
    public AtomicIndividualAction(final MinimalistActionListener listener, final Target target) {
        super(listener, target);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            final ActionResult result = executeAtomicAction();
            listener.actionSucceeded(target.get_name(), result);
        } catch (final Throwable t) {
            LOGGER.error("error on {}", target.get_name());
            LOGGER.error("error ", t);
            if (t instanceof DevFailed) {
                LOGGER.error("tango error {}", DevFailedUtils.toString((DevFailed) t));
            }
            listener.actionFailed(target.get_name(), null, t);
        } finally {
            listener.actionCompleted(); // Latch countdown in the finally clause
                                        // to avoid deadlocks in case of read
                                        // failure
        }
    }

    protected abstract ActionResult executeAtomicAction() throws Throwable;
}