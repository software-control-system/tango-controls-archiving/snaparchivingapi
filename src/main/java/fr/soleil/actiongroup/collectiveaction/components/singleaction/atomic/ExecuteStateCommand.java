package fr.soleil.actiongroup.collectiveaction.components.singleaction.atomic;

import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.MinimalistActionListener;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target.Target;

/**
 * Executes a void (no return) command on a device. Notifies its listener on completion.
 * 
 * @author CLAISSE
 */
public class ExecuteStateCommand extends ExecuteCommand {
    /**
     * @param listener The listener to notify on command execution completion
     * @param deviceToReadfrom The device to call the command on
     * @param commandName The command's name
     * @param commandParameters The command's parameters
     */
    public ExecuteStateCommand(MinimalistActionListener listener, Target deviceToReadfrom) {
        super(listener, deviceToReadfrom, "State", null);
    }
}