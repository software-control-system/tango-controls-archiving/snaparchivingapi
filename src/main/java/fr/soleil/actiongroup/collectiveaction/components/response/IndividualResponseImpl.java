package fr.soleil.actiongroup.collectiveaction.components.response;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DevErrorWrapper;

/**
 * Implementation of IndividualResponse
 * 
 * @author CLAISSE
 */
public class IndividualResponseImpl implements IndividualResponse {
    protected String deviceName;
    protected DevErrorWrapper[] err_stack;
    protected boolean has_failed;
    protected boolean has_timeout;

    public IndividualResponseImpl(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public String dev_name() {
        return this.deviceName;
    }

    @Override
    public DevErrorWrapper[] get_err_stack() {
        return this.err_stack;
    }

    @Override
    public boolean has_failed() {
        return this.has_failed;
    }

    @Override
    public boolean has_timeout() {
        return this.has_timeout;
    }

    @Override
    public void set_err_stack(DevErrorWrapper[] err_stack) {
        this.err_stack = err_stack;
    }

    @Override
    public void set_failed(boolean has_failed) {
        this.has_failed = has_failed;
    }

    @Override
    public void set_timeout(boolean has_timeout) {
        this.has_timeout = has_timeout;
    }
}
