package fr.soleil.actiongroup.collectiveaction.components.singleaction.atomic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.MinimalistActionListener;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.ActionResult;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceAttributeWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target.Target;

/**
 * Reads attributes from a device. Notifies its listener on completion.
 * 
 * @author CLAISSE
 */
public class ReadAttributes extends AtomicIndividualAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadAttributes.class);

    /**
     * The attributes to read
     */
    private final String[] attributesToRead;

    /**
     * @param listener The listener to notify on read completion
     * @param target The device to read from
     * @param attributesToRead The attributes to read
     */
    public ReadAttributes(MinimalistActionListener listener, Target target, String[] attributesToRead) {
        super(listener, target);
        this.attributesToRead = attributesToRead;
    }

    @Override
    protected ActionResult executeAtomicAction() throws Throwable {
        long before = System.currentTimeMillis();
        DeviceAttributeWrapper[] response = null;
        try {
            response = this.target.read_attribute(this.attributesToRead);
        } catch (Throwable t) {
            StringBuilder builder = new StringBuilder();
            if (attributesToRead == null) {
                builder.append("Failed to read attributes as attribute list is null");
            } else if (attributesToRead.length == 0) {
                builder.append("Failed to read attributes as there is no attribute to read");
            } else {
                builder.append("Failed to read following attributes:");
                for (String attr : attributesToRead) {
                    builder.append("\n- ").append(attr);
                }
                builder.append("\nReason:\n");
                if (t instanceof DevFailed) {
                    builder.append(DevFailedUtils.toString((DevFailed) t));
                } else {
                    builder.append(t.getMessage());
                }
            }
            LOGGER.error(builder.toString(), t);
            throw t;
        }
        long readTime = System.currentTimeMillis() - before;

        return new ActionResult(response, readTime);
    }
}