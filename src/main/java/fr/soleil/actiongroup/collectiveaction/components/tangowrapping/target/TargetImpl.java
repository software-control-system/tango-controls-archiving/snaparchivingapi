package fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.CommandInfo;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.AttributeInfoWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.CommandInfoWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DbDatumWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceAttributeWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceDataWrapper;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Target implementation
 * 
 * @author CLAISSE
 */
public class TargetImpl implements Target {

    private static final Logger LOGGER = LoggerFactory.getLogger(TargetImpl.class);

    private final DeviceProxy proxy;

    public TargetImpl(final DeviceProxy proxy) {
        this.proxy = proxy;
    }

    @Override
    public void set_timeout_millis(final int timeout) throws DevFailed {
        proxy.set_timeout_millis(timeout);
    }

    @Override
    public DeviceDataWrapper command_inout(final String commandName, final DeviceDataWrapper commandParameters)
            throws DevFailed {
        DeviceData response;
        if (commandParameters == null || commandParameters.getCommandArgument() == null) {
            response = proxy.command_inout(commandName);
        } else {
            response = proxy.command_inout(commandName, commandParameters.getCommandArgument());
        }
        return new DeviceDataWrapper(response);
    }

    @Override
    public DeviceAttributeWrapper[] read_attribute(final String[] attributesToRead) throws DevFailed {
        LOGGER.debug("reading attributes {}", Arrays.toString(attributesToRead));
        final DeviceAttribute[] response = proxy.read_attribute(attributesToRead);
        final DeviceAttributeWrapper[] wrappedResponse = wrapAttributes(response);
        return wrappedResponse;
    }

    private DeviceAttributeWrapper[] wrapAttributes(final DeviceAttribute[] response) {
        final DeviceAttributeWrapper[] ret;
        if (response == null) {
            ret = null;
        } else {
            ret = new DeviceAttributeWrapper[response.length];
            for (int i = 0; i < response.length; i++) {
                ret[i] = new DeviceAttributeWrapper(response[i]);
            }
        }
        return ret;
    }

    @Override
    public AttributeInfoWrapper get_attribute_info(final String attributeName) throws DevFailed {
        final AttributeInfo tmpAttributeInfo = proxy.get_attribute_info(attributeName);
        return new AttributeInfoWrapper(tmpAttributeInfo);
    }

    @Override
    public void set_attribute_info(final AttributeInfoWrapper wrapper) throws DevFailed {
        proxy.set_attribute_info(new AttributeInfo[] { wrapper.getAttributeInfo() });
    }

    @Override
    public void put_property(final String propertyName, final DbDatumWrapper propertyValueHolder) throws DevFailed {
        if (propertyName.isEmpty()) {
            proxy.delete_property(ObjectUtils.EMPTY_STRING);
        } else {
            final DbDatum propertyValue = propertyValueHolder.getDbDatum();
            proxy.put_property(new DbDatum[] { propertyValue });
        }
    }

    @Override
    public String get_name() {
        return proxy.get_name();
    }

    @Override
    public void write_attribute(final DeviceAttributeWrapper[] attributesToWrite, final Double newValue)
            throws DevFailed {
        final DeviceAttribute[] attrs = setNewValue(attributesToWrite, newValue);
        proxy.write_attribute(attrs);
    }

    private DeviceAttribute[] setNewValue(final DeviceAttributeWrapper[] attributesAnswer, final Double newValue)
            throws IllegalArgumentException, DevFailed {
        final DeviceAttribute[] ret = new DeviceAttribute[attributesAnswer.length];
        for (int i = 0; i < attributesAnswer.length; i++) {
            ret[i] = attributesAnswer[i].getAttribute();
            insertNewValue(ret[i], newValue);
        }
        return ret;
    }

    private void insertNewValue(final DeviceAttribute attribute, final Double newValue) throws DevFailed {
        switch (attribute.getType()) {
            case TangoConst.Tango_DEV_SHORT:
                attribute.insert(newValue.shortValue());
                break;

            case TangoConst.Tango_DEV_USHORT:
                attribute.insert(newValue.shortValue());
                break;

            case TangoConst.Tango_DEV_CHAR:
                attribute.insert(newValue.intValue());
                break;

            case TangoConst.Tango_DEV_UCHAR:
                attribute.insert_uc(newValue.shortValue());
                break;

            case TangoConst.Tango_DEV_LONG:
                attribute.insert(newValue.intValue());
                break;

            case TangoConst.Tango_DEV_ULONG:
                attribute.insert(newValue.intValue());
                break;

            case TangoConst.Tango_DEV_FLOAT:
                attribute.insert(newValue.floatValue());
                break;

            case TangoConst.Tango_DEV_DOUBLE:
                attribute.insert(newValue);
                break;

            case TangoConst.Tango_DEV_BOOLEAN:
                attribute.insert(newValue == 1);
                break;

            default:
                throw new IllegalArgumentException(
                        "WriteNumericValueListener/Unexpected attribute type/" + attribute.getType());
        }
    }

    @Override
    public CommandInfoWrapper[] command_list_query() throws DevFailed {
        final CommandInfo[] response = proxy.command_list_query();
        return wrapCommandInfos(response);
    }

    private CommandInfoWrapper[] wrapCommandInfos(final CommandInfo[] response) {
        final CommandInfoWrapper[] ret;
        if (response == null) {
            ret = null;
        } else {
            ret = new CommandInfoWrapper[response.length];
            for (int i = 0; i < response.length; i++) {
                ret[i] = new CommandInfoWrapper(response[i]);
            }
        }
        return ret;
    }
}
