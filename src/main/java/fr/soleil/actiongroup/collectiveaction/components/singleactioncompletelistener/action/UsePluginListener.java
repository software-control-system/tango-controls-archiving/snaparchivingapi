package fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.ActionListener;
import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.ActionListenerDecorator;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.ActionResult;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceAttributeWrapper;
import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.Plugin;
import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.context.PluginContext;

/**
 * A listener for the event
 * "the attributes (not necessarily numeric) reading operation from a device is complete"
 * . If the operation succeeded, a "plugin action" uses the read result. What
 * this plugin action does (if it does anything) depends on the implementation
 * of IPluginAction.
 * 
 * @author CLAISSE
 */
public class UsePluginListener extends ActionListenerDecorator implements ActionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(UsePluginListener.class);
    private final Plugin action;
    private final PluginContext context;

    public UsePluginListener(final ActionListener _decorator, final Plugin _action, final PluginContext _context) {
        super(_decorator);
        action = _action;
        context = _context;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.core.groupactions.apis.listener.IActionCompleteListener#
     * actionSucceeded(java.lang.String, java.lang.Object)
     */
    @Override
    public void actionSucceeded(final String targetName, final ActionResult actionResult) {
        final DeviceAttributeWrapper[] attributesAnswer = actionResult.getAttributesValue();
        for (final DeviceAttributeWrapper element : attributesAnswer) {
            try {
                action.execute(context, targetName, element);
            } catch (final Exception e) {
                LOGGER.error("error", e);
                if (e instanceof DevFailed) {
                    LOGGER.error("tango error {}", DevFailedUtils.toString((DevFailed) e));
                }
                super.actionFailed(targetName, actionResult, e);
            }
        }
        super.actionSucceeded(targetName, actionResult);
    }
}
