package fr.soleil.actiongroup.collectiveaction.components.response;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceDataWrapper;

/**
 * Implementation of IndividualCommandResponse
 * 
 * @author CLAISSE
 */
public class IndividualCommandResponseImpl extends IndividualResponseImpl implements IndividualCommandResponse {
    private DeviceDataWrapper data;

    public IndividualCommandResponseImpl(String deviceName) {
        super(deviceName);
    }

    @Override
    public DeviceDataWrapper get_data() {
        return this.data;
    }

    @Override
    public void setData(DeviceDataWrapper data) {
        this.data = data;
    }
}
