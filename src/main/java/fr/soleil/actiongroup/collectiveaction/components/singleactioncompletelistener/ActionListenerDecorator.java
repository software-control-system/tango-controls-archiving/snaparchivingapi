package fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener;

import java.util.Map;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.ActionResult;

/**
 * ActionListenerDecorator
 * 
 * @author CLAISSE
 */
public class ActionListenerDecorator implements ActionListener {
    private final ActionListener decorator;

    public ActionListenerDecorator(ActionListener decorator) {
        this.decorator = decorator;
    }

    @Override
    public synchronized void actionCompleted() {
        this.decorator.actionCompleted();
    }

    @Override
    public synchronized void actionFailed(String targetName, ActionResult actionResult, Throwable exception) {
        this.decorator.actionFailed(targetName, actionResult, exception);
    }

    @Override
    public synchronized void actionSucceeded(String targetName, ActionResult actionResult) {
        this.decorator.actionSucceeded(targetName, actionResult);
    }

    @Override
    public synchronized boolean hasBeenNotifiedOfFailedActions() {
        return decorator.hasBeenNotifiedOfFailedActions();
    }

    @Override
    public synchronized Map<String, String> getMessages() {
        return decorator.getMessages();
    }
}
