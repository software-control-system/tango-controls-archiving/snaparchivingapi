package fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.extraction.qualities;

import java.util.Map;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.AttrQualityWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceAttributeWrapper;

/**
 * A do-nothing implementation
 * 
 * @author CLAISSE
 */
public class DoNothingAttributeQualityRegister implements IAttributeQualityRegister {
    /* (non-Javadoc)
     * @see fr.soleil.core.groupactions.apis.listener.attributes.read.quality.IAttributeQualityReader#getQualities()
     */
    @Override
    public Map<String, AttrQualityWrapper> getQualities() {
        return null;
    }

    /* (non-Javadoc)
     * @see fr.soleil.core.groupactions.apis.listener.attributes.read.quality.IAttributeQualityReader#getQuality(java.lang.String)
     */
    @Override
    public AttrQualityWrapper getQuality(String attributeName) {
        return null;
    }

    @Override
    public void registerAttributeQuality(String deviceName, DeviceAttributeWrapper attribute) {

    }
}
