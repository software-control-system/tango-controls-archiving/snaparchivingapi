package fr.soleil.actiongroup.collectiveaction.components.response;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceAttributeWrapper;

/**
 * Implementation of IndividualAttributesResponse
 * 
 * @author CLAISSE
 */
public class IndividualAttributesResponseImpl extends IndividualResponseImpl implements IndividualAttributesResponse {
    private DeviceAttributeWrapper[] data;

    public IndividualAttributesResponseImpl(String deviceName) {
        super(deviceName);
    }

    @Override
    public DeviceAttributeWrapper[] get_data() {
        return this.data;
    }

    @Override
    public void setAttributes(DeviceAttributeWrapper[] attributes) {
        this.data = attributes;
    }
}
