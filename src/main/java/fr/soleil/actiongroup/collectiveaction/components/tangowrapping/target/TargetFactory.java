package fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target;

import fr.esrf.TangoApi.DeviceProxy;

public class TargetFactory {
    public static Target getTarget(DeviceProxy proxy) {
        return new TargetImpl(proxy);
    }
}
