package fr.soleil.actiongroup.collectiveaction.components.response;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceDataWrapper;

/**
 * IndividualCommandResponse
 * 
 * @author CLAISSE
 */
public interface IndividualCommandResponse extends IndividualResponse {
    public DeviceDataWrapper get_data();

    public void setData(DeviceDataWrapper data);
}
