package fr.soleil.actiongroup.collectiveaction.components.response;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;

/**
 * Implementation of CollectiveResponse
 * 
 * @author CLAISSE
 */
public class CollectiveResponseImpl implements CollectiveResponse {
    private final Map<String, IndividualResponse> individualResponses;
    private boolean hasFailed;
    private boolean isFailureDueToATimeout;

    public CollectiveResponseImpl() {
        this.individualResponses = new Hashtable<>();
    }

    @Override
    public synchronized Collection<IndividualResponse> getIndividualResponses() {
        return individualResponses.values();
    }

    @Override
    public synchronized IndividualResponse getIndividualResponse(String deviceName) {
        return individualResponses.get(deviceName);
    }

    @Override
    public synchronized void addIndividualResponse(IndividualResponse individualResponse) {
        individualResponses.put(individualResponse.dev_name(), individualResponse);
        if (individualResponse.has_failed()) {
            this.hasFailed = true;
            if (individualResponse.has_timeout()) {
                this.isFailureDueToATimeout = true;
            }
        }
    }

    @Override
    public synchronized boolean hasFailed() {
        return hasFailed;
    }

    @Override
    public synchronized boolean isFailureDueToATimeout() {
        return isFailureDueToATimeout;
    }
}
