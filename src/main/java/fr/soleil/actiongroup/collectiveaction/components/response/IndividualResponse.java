package fr.soleil.actiongroup.collectiveaction.components.response;

import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DevErrorWrapper;

/**
 * IndividualResponse
 * 
 * @author CLAISSE
 */
public interface IndividualResponse {
    public boolean has_failed();

    public String dev_name();

    public DevErrorWrapper[] get_err_stack();

    public boolean has_timeout();

    public void set_err_stack(DevErrorWrapper[] err_stack);

    public void set_failed(boolean has_failed);

    public void set_timeout(boolean has_timeout);
}
