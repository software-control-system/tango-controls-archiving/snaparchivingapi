package fr.soleil.actiongroup.collectiveaction.onattributes;

import fr.soleil.actiongroup.collectiveaction.CollectiveActionImpl;
import fr.soleil.actiongroup.collectiveaction.components.singleaction.IndividualAction;
import fr.soleil.actiongroup.collectiveaction.components.singleaction.atomic.ReadAttributes;
import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.ActionListener;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target.Target;

/**
 * A group that read attributes from its members
 * 
 * @author CLAISSE
 */
public abstract class ReadAttributesImpl extends CollectiveActionImpl {

    /**
     * @param proxies The devices to read from
     * @param attributes The list of attributes to read for each device
     */
    public ReadAttributesImpl(Target[] proxies, String[][] attributes) {
        super(proxies, attributes);
    }

    @Override
    protected IndividualAction getTask(int deviceIndex, ActionListener listener) {
        return new ReadAttributes(listener, targets[deviceIndex], attributes[deviceIndex]);
    }
}
