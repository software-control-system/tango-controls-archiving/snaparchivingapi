/*	Synchrotron Soleil 
 *  
 *   File          :  AnyAttribute.java
 *  
 *   Project       :  TangoParser
 *  
 *   Description   :  
 *  
 *   Author        :  CLAISSE
 *  
 *   Original      :  17 janv. 07 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: AnyAttribute.java,v 
 *
 */
/*
 * Created on 17 janv. 07
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevState;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.DeviceAttributeWrapper;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.TangoConstWrapper;

/**
 * A data transfer object that holds the data extracted from a read attribute.
 * 
 * @author CLAISSE
 */
public class AnyAttribute {
	final static Logger logger = LoggerFactory.getLogger(AnyAttribute.class);
	
    private static final String SPECTRUM_SEPARATOR = ",";

    /**
     * The attribute's complete name
     */
    private String completeName;

    /**
     * The attribute's X dimension (1 for scalars)
     */
    private int dimX;

    /**
     * The attribute's Y dimension (1 for scalars and spectrums)
     */
    private int dimY;

    /**
     * The attribute's type
     */
    private int type;

    /**
     * The attribute's format (scalar, spectrum, or image)
     */
    private int format;

    /**
     * The attribute's read/write property (read-only, write-only, or
     * read/write)
     */
    private int writable;

    /**
     * The attribute's double value
     */
    private double[] rawValueDouble;

    /**
     * The attribute's int value
     */
    private int[] rawValueInt;

    /**
     * The attribute's short value
     */
    private short[] rawValueShort;

    /**
     * The attribute's String value
     */
    private String[] rawValueString;

    /**
     * The attribute's long value
     */
    private long[] rawValueLong;

    /**
     * The attribute's float value
     */
    private float[] rawValueFloat;

    /**
     * The attribute's boolean value
     */
    private boolean[] rawValueBoolean;

    /**
     * The attribute's State value
     */
    private DevState[] rawValueState;

    private final int numberOfComponents;

    private double[] convertedNumericValuesTable;

    private String[] convertedStringValuesTable;

    private boolean hasBothReadAndWrite;

    private boolean hasBeenAggreggated;
    private String aggreggateAll;
    private String aggreggateRead;
    private String aggreggateWrite;

    public AnyAttribute(final String deviceName, final DeviceAttributeWrapper attribute) throws Exception {
        initParameters(deviceName, attribute);
        numberOfComponents = extractValue(attribute);
        initFormatAndWritable();
        buildPersistableValue();
    }

    private void initFormatAndWritable() {
        int format = TangoConstWrapper.AttrDataFormat_SCALAR;
        if (dimX > 1) {
            format = TangoConstWrapper.AttrDataFormat_SPECTRUM;
        }
        if (dimY > 1) {
            format = TangoConstWrapper.AttrDataFormat_IMAGE;
        }
        setFormat(format);

        hasBothReadAndWrite = numberOfComponents > dimX;
        // WARNING comment distinguer le cas WO???
        final int writable = hasBothReadAndWrite ? TangoConstWrapper.AttrWriteType_READ_WRITE
                : TangoConstWrapper.AttrWriteType_READ;
        setWritable(writable);
    }

    private int extractValue(final DeviceAttributeWrapper attribute) throws Exception {
        int _numberOfComponents = 0;
        switch (getType()) {
        case TangoConst.Tango_DEV_BOOLEAN:
            rawValueBoolean = attribute.extractBooleanArray();
            _numberOfComponents = rawValueBoolean == null ? 0 : rawValueBoolean.length;
            break;

        case TangoConst.Tango_DEV_DOUBLE:
            rawValueDouble = attribute.extractDoubleArray();
            _numberOfComponents = rawValueDouble == null ? 0 : rawValueDouble.length;
            break;

        case TangoConst.Tango_DEV_FLOAT:
            rawValueFloat = attribute.extractFloatArray();
            _numberOfComponents = rawValueFloat == null ? 0 : rawValueFloat.length;
            break;
        case TangoConst.Tango_DEV_LONG:
        case TangoConst.Tango_DEV_INT:
            rawValueInt = attribute.extractLongArray();
            _numberOfComponents = rawValueInt == null ? 0 : rawValueInt.length;
            break;
        case TangoConst.Tango_DEV_ULONG:
            rawValueLong = attribute.extractULongArray();
            _numberOfComponents = rawValueLong == null ? 0 : rawValueLong.length;
            break;
        case TangoConst.Tango_DEV_LONG64:
            rawValueLong = attribute.extractLong64Array();
            _numberOfComponents = rawValueLong == null ? 0 : rawValueLong.length;
            break;
        case TangoConst.Tango_DEV_ULONG64:
            rawValueLong = attribute.extractULong64Array();
            _numberOfComponents = rawValueLong == null ? 0 : rawValueLong.length;
            break;
        case TangoConst.Tango_DEV_USHORT:
            rawValueInt = attribute.extractUShortArray();
            _numberOfComponents = rawValueInt == null ? 0 : rawValueInt.length;
            break;
        case TangoConst.Tango_DEV_SHORT:
        case TangoConst.Tango_DEV_CHAR:
            rawValueShort = attribute.extractShortArray();
            _numberOfComponents = rawValueShort == null ? 0 : rawValueShort.length;
            break;
        case TangoConst.Tango_DEV_UCHAR:
            rawValueShort = attribute.extractUCharArray();
            _numberOfComponents = rawValueShort == null ? 0 : rawValueShort.length;
            break;

        case TangoConst.Tango_DEV_STATE:
            rawValueState = attribute.extractDevStateArray();
            _numberOfComponents = rawValueState == null ? 0 : rawValueState.length;
            break;

        case TangoConst.Tango_DEV_STRING:
            rawValueString = attribute.extractStringArray();
            _numberOfComponents = rawValueString == null ? 0 : rawValueString.length;
            break;
        default:
            logger.error("NO EXTRACTION " + attribute.getName());
        }
        return _numberOfComponents;
    }

    private void buildPersistableValue() {
        if (numberOfComponents == 0) {
            return;
        }
        convertedStringValuesTable = new String[numberOfComponents];
        convertedNumericValuesTable = new double[numberOfComponents];

        for (int i = 0; i < numberOfComponents; i++) {
            switch (getType()) {
            case TangoConst.Tango_DEV_BOOLEAN:
                convertedNumericValuesTable[i] = rawValueBoolean[i] ? 1 : 0; // for
                // scalars
                // which
                // are
                // stored
                // as
                // 0/1
                convertedStringValuesTable[i] = rawValueBoolean[i] + ""; // for
                // spectrums
                // which
                // are
                // stored
                // as
                // true,false,false,...
                break;

            case TangoConst.Tango_DEV_DOUBLE:
                convertedNumericValuesTable[i] = rawValueDouble[i];
                convertedStringValuesTable[i] = rawValueDouble[i] + "";
                break;

            case TangoConst.Tango_DEV_FLOAT:
                convertedNumericValuesTable[i] = rawValueFloat[i];
                convertedStringValuesTable[i] = rawValueFloat[i] + "";
                break;

            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_INT:
            case TangoConst.Tango_DEV_USHORT:
                convertedNumericValuesTable[i] = rawValueInt[i];
                convertedStringValuesTable[i] = rawValueInt[i] + "";
                break;
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_ULONG64:
            case TangoConst.Tango_DEV_LONG64:
                convertedNumericValuesTable[i] = rawValueLong[i];
                convertedStringValuesTable[i] = rawValueLong[i] + "";
                break;

            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                convertedNumericValuesTable[i] = rawValueShort[i];
                convertedStringValuesTable[i] = rawValueShort[i] + "";
                break;

            case TangoConst.Tango_DEV_STATE:
                convertedNumericValuesTable[i] = rawValueState[i].value();
                convertedStringValuesTable[i] = rawValueState[i].value()+"";
                break;

            case TangoConst.Tango_DEV_STRING:
                convertedStringValuesTable[i] = rawValueString[i];
                break;
            }
        }
    }

    private void initParameters(final String deviceName, final DeviceAttributeWrapper attribute) throws Exception {
        final String name = attribute.getName();
        final String completeName = deviceName + "/" + name;
        final int dimX = attribute.getDimX();
        final int dimY = attribute.getDimY();
        final int type = attribute.getType();

        setCompleteName(completeName);
        setDimX(dimX);
        setDimY(dimY);
        setType(type);
    }

    /**
     * @return the completeName
     */
    public String getCompleteName() {
        return completeName;
    }

    /**
     * @param completeName
     *            the completeName to set
     */
    public void setCompleteName(final String completeName) {
        this.completeName = completeName;
    }

    /**
     * @return the dimX
     */
    public int getDimX() {
        return dimX;
    }

    /**
     * @param dimX
     *            the dimX to set
     */
    public void setDimX(final int dimX) {
        this.dimX = dimX;
    }

    /**
     * @return the dimY
     */
    public int getDimY() {
        return dimY;
    }

    /**
     * @param dimY
     *            the dimY to set
     */
    public void setDimY(final int dimY) {
        this.dimY = dimY;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final int type) {
        this.type = type;
    }

    /**
     * @return the format
     */
    public int getFormat() {
        return format;
    }

    /**
     * @param format
     *            the format to set
     */
    public void setFormat(final int format) {
        this.format = format;
    }

    /**
     * @return the writable
     */
    public int getWritable() {
        return writable;
    }

    /**
     * @param writable
     *            the writable to set
     */
    public void setWritable(final int writable) {
        this.writable = writable;
    }

    public double[] getConvertedNumericValuesTable() {
        return convertedNumericValuesTable;
    }

    public String[] getConvertedStringValuesTable() {
        return convertedStringValuesTable;
    }

    public String getConvertedStringAggreggatedValues(final boolean wantsReadValue) {
        if (!hasBeenAggreggated) {
            aggreggate();
        }

        if (!hasBothReadAndWrite) {
            return aggreggateAll;
        } else {
            if (wantsReadValue) {
                return aggreggateRead;
            } else {
                return aggreggateWrite;
            }
        }
    }

    private void aggreggate() {
        if (!hasBothReadAndWrite) {
            aggreggateAll = aggreggateAll();
        } else {
            aggreggateRead = aggreggateRead();
            aggreggateWrite = aggreggateWrite();
        }
        hasBeenAggreggated = true;
    }

    private String aggreggateWrite() {
        try {
            if (convertedStringValuesTable == null || convertedStringValuesTable.length == 0) {
                return null;
            }
            final StringBuilder buff = new StringBuilder();
            for (int i = dimX; i < convertedStringValuesTable.length; i++) {
                buff.append(convertedStringValuesTable[i]);
                if (i < convertedStringValuesTable.length - 1) {
                    buff.append(SPECTRUM_SEPARATOR);
                }
            }
            return buff.toString();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String aggreggateRead() {
        if (convertedStringValuesTable == null || convertedStringValuesTable.length == 0) {
            return null;
        }
        final StringBuilder buff = new StringBuilder();
        for (int i = 0; i < dimX; i++) {
            buff.append(convertedStringValuesTable[i]);
            if (i < dimX - 1) {
                buff.append(SPECTRUM_SEPARATOR);
            }
        }
        return buff.toString();
    }

    private String aggreggateAll() {
        if (convertedStringValuesTable == null || convertedStringValuesTable.length == 0) {
            return null;
        }
        final StringBuilder buff = new StringBuilder();
        for (int i = 0; i < convertedStringValuesTable.length; i++) {
            buff.append(convertedStringValuesTable[i]);
            if (i < convertedStringValuesTable.length - 1) {
                buff.append(SPECTRUM_SEPARATOR);
            }
        }
        return buff.toString();
    }
}
