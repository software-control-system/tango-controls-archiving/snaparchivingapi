package fr.soleil.actiongroup.collectiveaction.onattributes;

import fr.soleil.actiongroup.collectiveaction.components.singleactioncompletelistener.extraction.qualities.Qualities;

/**
 * A group that reads the value and quality of numeric attributes from its members
 * @author CLAISSE 
 */
public interface ReadNumericAttributesAndQualities extends ReadNumericAttributes, Qualities
{
    
}