/*	Synchrotron Soleil 
 *  
 *   File          :  ddd.java
 *  
 *   Project       :  TangoParser
 *  
 *   Description   :  
 *  
 *   Author        :  CLAISSE
 *  
 *   Original      :  17 janv. 07 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: ddd.java,v 
 *
 */
 /*
 * Created on 17 janv. 07
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.context.PersistenceContext;



/**
 * A do-nothing implementation
 * @author CLAISSE 
 */
public class DoNothingPersistenceManager implements PersistenceManager {

    /**
     * 
     */
    public DoNothingPersistenceManager() 
    {
    
    }

    public void store(AnyAttribute attribute, PersistenceContext persistenceContext) throws Exception {
        // TODO Auto-generated method stub
        
    }
}
