package fr.soleil.archiving.snap.api;

import com.google.common.collect.Lists;
import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.Snapshot;
import fr.soleil.archiving.snap.api.tools.SnapshotLight;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.archiving.snap.api.tools.TangoJAVAUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.database.connection.DataBaseParameters.DataBaseType;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.statement.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

/**
 * <b>Description : </b> This class hides the loading of drivers, the
 * connection/disconnection with the database, and more generaly, this class
 * hides number of methods to insert, update and query <I>Snap</I>.
 *
 * @author Jean CHINKUMO
 * @version 1.0
 */
public class DataBaseAPI {

    private static final String ERROR_EXTRACTING_SNAPSHOT_DATA = "error extracting snapshot data";
    public static final int MAX_SELECT_SIZE = 1000;
    public static boolean useLog4JDBC = System.getProperty("log4jdbc.active") == null ? false : System.getProperty(
            "log4jdbc.active").equalsIgnoreCase("true");
    private final Logger logger = LoggerFactory.getLogger(DataBaseAPI.class);
    /**
     * Connection dbatabase type (<I>MySQL</I>, <I>Oracle</I>, ...)
     */

    /* JDBC driver used for the connection */
    /* Database Host adress */
    /* User's name for the connection */
    /* User's password for the connection */
    /* database name */
    /**
     * database'schema' used
     */
    private int currentSpectrumDimX;

    private DataBaseParameters params;
    private AbstractDataBaseConnector connector;

    /**
     * Constructor using host name, user name, password and database name.
     *
     * @param params Database connection parameters
     */
    public DataBaseAPI(DataBaseParameters params) throws ArchivingException {
        if (params.getHost().isEmpty()) {
            params.setDbType(DataBaseType.H2);
            connector = ConnectionFactory.connect(params);
        } else {
            try {
                params.setDbType(DataBaseType.ORACLE);
                connector = ConnectionFactory.connect(params);
            } catch (Exception e) {
                params.setDbType(DataBaseType.MYSQL);
                connector = ConnectionFactory.connect(params);
            }
        }
        this.params = params;
    }

    /**
     * <b>Description : </b> Gets the database name
     *
     * @return The database name
     */
    public String getDbName() {
        return params.getName();
    }

    /**
     * <b>Description : </b> Returns the connected database host identifier.
     *
     * @return The host where the connection is done
     */
    public String getHost() {
        return params.getHost();
    }

    /**
     * <b>Description : </b> Gets the current user's name for the connection
     *
     * @return The user's name for the connection
     */
    public String getUser() {
        return params.getUser();
    }

    /**
     * <b>Description : </b> Gets the current user's password for the connection
     *
     * @return The user's password for the connection
     */
    public String getPassword() {
        return params.getPassword();
    }

    /**
     * <b>Description : </b> Gets the type of database being used (<I>MySQL</I>,
     * <I>Oracle</I>, ...)
     *
     * @return The type of database being used
     */
    public DataBaseType getDb_type() {
        return params.getDbType();
    }

    /**
     * ************************************************************************
     * <b>Description</b> : Methods that counts the number of non null rows in
     * an array
     *
     * @param arr an array of Strings
     * @throws NullPointerException *************************************************************
     *                              * ***********
     */
    public int getArrayCount(final String[] arr) {
        int arrayCount = 0;
        try {
            for (int i = 0; i < arr.length; i++) {
                if (!arr[i].equals(null)) {
                    arrayCount++;
                }
            }
        } catch (final NullPointerException e) {
        }
        return arrayCount;
    }

    /**
     * ************************************************************************
     * <b>Description : </b> Closes the connection with the database
     *
     * @throws SnapshotingException *************************************************************
     *                              * *********
     */
    public void close(final Connection conn) {
        closeConnection(conn);
    }

    private void closeConnection(final Connection conn) {
        if (conn == null) {
            return;
        }

        try {
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();

            logger.error("ERROR !! " + "\r\n" + "\t Origin : \t " + "DataBaseAPI.closeConnection" + "\r\n"
                    + "\t Reason : \t " + getDbSchema().toUpperCase().trim() + "_FAILURE" + "\r\n"
                    + "\t Description : \t " + e.getMessage());
        }
    }


    /*****************************************************************************
     *
     *
     * Generals methods used to query SNAP (SELECT) (Extraction)
     *
     ****************************************************************************/

    /**
     * <b>Description : </b> Gets the database's schema name
     *
     * @return The database name
     */
    public String getDbSchema() {
        return params.getSchema();
    }

    /**
     * <b>Description : </b> Checks if the attribute of the given name, is
     * already registered in <I>SnapDb</I> (and more particularly in the table
     * of the definitions).
     *
     * @param att_name The name of the attribute to check.
     * @return boolean
     * @throws SnapshotingException
     */
    public boolean isRegistered(final String att_name) throws SnapshotingException {
        final int id = getAttID(att_name.trim());
        if (id != 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * ************************************************************************
     * <b>Description : </b> Gets for a specified attribute its ID as defined in
     * SnapDb
     *
     * @param att_name The attribute's name
     * @return The <I>SnapDb</I>'s ID that caracterize the given attribute
     * @throws SnapshotingException *************************************************************
     *                              * *********
     */
    public int getAttID(final String att_name) throws SnapshotingException {
        int attributesID = 0;
        Connection conn = null;
        ResultSet rset = null;

        PreparedStatement ps_get_att_id = null;
        final String table_name = getDbSchema() + "." + SnapConst.AST;
        StringBuilder selectStringBuilder = new StringBuilder();
        try {
            // Preparing statement...
            selectStringBuilder.append("SELECT " + SnapConst.ID + " FROM " + table_name + " WHERE ");
            if (params.getDbType() == DataBaseType.ORACLE) {
                selectStringBuilder.append("LOWER(" + SnapConst.fullName + ") like LOWER(?)");
            } else {
                selectStringBuilder.append(SnapConst.fullName + " like ?");
            }
            conn = connector.getConnection();
            ps_get_att_id = conn.prepareStatement(selectStringBuilder.toString());

            final String field1 = att_name.trim();
            ps_get_att_id.setString(1, field1);
            rset = ps_get_att_id.executeQuery();
            // Gets the result of the query
            if (rset.next()) {
                attributesID = rset.getInt(1);
                ps_get_att_id.close();
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getAttID() method...";
            final String desc = "";
            selectStringBuilder.append(" => ").append(att_name.trim());
            throw new SnapshotingException(message, reason, selectStringBuilder.toString(), ErrSeverity.PANIC, desc,
                    "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(ps_get_att_id);
            closeConnection(conn);
        }

        // Returns the total number of signals defined in Snap
        return attributesID;
    }

    public void closeResultSet(final ResultSet resultSet) {
        if (resultSet == null) {
            return;
        }

        try {
            resultSet.close();
        } catch (final SQLException e) {
            e.printStackTrace();

            logger.error("ERROR !! " + "\r\n" + "\t Origin : \t " + "DataBaseAPI.closeResultSet" + "\r\n"
                    + "\t Reason : \t " + getDbSchema().toUpperCase().trim() + "_FAILURE" + "\r\n"
                    + "\t Description : \t " + e.getMessage());
        }
    }

    public void closeStatement(final Statement preparedStatement) {
        if (preparedStatement == null) {
            return;
        }

        try {
            preparedStatement.close();
        } catch (final SQLException e) {
            e.printStackTrace();

            logger.error("ERROR !! " + "\r\n" + "\t Origin : \t " + "DataBaseAPI.closeStatement" + "\r\n"
                    + "\t Reason : \t " + getDbSchema().toUpperCase().trim() + "_FAILURE" + "\r\n"
                    + "\t Description : \t " + e.getMessage());
        }
    }

    /**
     * <b>Description : </b> Returns the new id for the new context being
     * referenced in database <I>snap</I>.<I>(mySQL only)</I>
     *
     * @return Returns the new id for the new context being referenced in
     * database <I>snap</I>.
     * @throws SnapshotingException
     */
    public int getMaxContextID() throws SnapshotingException {
        Statement stmt = null;
        ResultSet rset = null;
        String query;
        int res = 0;
        query = "SELECT MAX(" + SnapConst.ID_CONTEXT + ") FROM " + getDbSchema() + "." + SnapConst.CONTEXT;

        Connection conn = null;
        try {
            conn = connector.getConnection();
            stmt = conn.createStatement();
            rset = stmt.executeQuery(query);
            // Gets the result of the query
            if (rset.next()) {
                res = rset.getInt(1);
            }
            if (res < 0) {
                final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
                final String reason = "Failed while executing DataBaseAPI.getMaxID() method...";
                final String desc = "The table seem to be empty, please check it...";
                logger.error(new SnapshotingException(message, reason, ErrSeverity.WARN, desc, "").toString());
                res = 0;
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getMaxID() method...";
            final String desc = "";
            throw new SnapshotingException(message, reason, query, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(stmt);
            closeConnection(conn);
        }

        return res;
    }

    /**
     * This method retrives from the the database, the list of all registered
     * contexts (or 'snap-patterns) which subscribe to the clause and/or have
     * the identifier id_context. Author : Laure Garda
     *
     * @param clause
     * @param id_context
     * @return a list of registered contexts (or 'snap-patterns)
     * @throws SnapshotingException
     */
    public List<SnapContext> getContext(final String clause, final int id_context) throws SnapshotingException {
        String select_field = String.join(", ", SnapConst.TAB_CONTEXT);
        // Choix de la table dans laquelle effectuer la requete SQL.
        final String table_1 = getDbSchema() + "." + SnapConst.CONTEXT;
        // Affichage des resusltats par ordre croissant d'ID du context.
        final String orderBy = " ORDER BY " + SnapConst.ID_CONTEXT;

        String query = "SELECT " + select_field + " FROM " + table_1;

        try (Handle handle = connector.getJdbi().open()) {
            Query queryBinder = handle.createQuery(query + clause + orderBy);
            if (id_context != -1) {
                queryBinder.bind(0, id_context);
            }
            return queryBinder.map((rs, ctx) -> new SnapContext(rs.getString(SnapConst.author),
                    rs.getString(SnapConst.name), rs.getInt(SnapConst.ID_CONTEXT), rs.getDate(SnapConst.time),
                    rs.getString(SnapConst.reason), rs.getString(SnapConst.description))).list();
        }

    }

    /**
     * ************************************************************************
     * <b>Description : </b> Gets all snapshots from a given context Author :
     * Laure Garda : Renvoi les snapshots associes au contexte passe en
     * parametre qui verifient les conditions passees en parametre.
     *
     * @return array of strings
     * @throws SnapshotingException
     * @see SnapshotLight
     * ********************************************************************
     * * ****
     */
    public List<SnapshotLight> getContextAssociatedSnapshots(final String clause, final int id_context,
                                                             final int id_snap) throws SnapshotingException {
        // Create and execute the SQL query string
        String query;
        final String select_field = SnapConst.ID_SNAP + ", " + SnapConst.time + ", " + SnapConst.snap_comment;
        // recuperation de la table des snapshot.
        final String table_1 = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String orderBy = " ORDER BY " + SnapConst.ID_SNAP;

        query = "SELECT " + select_field + " FROM " + table_1 + clause + orderBy;

        try (Handle handle = connector.getJdbi().open()) {
            Query queryBinder = handle.createQuery(query);
            if (id_context != -1 && id_snap != -1) {
                queryBinder.bind(0, id_snap);
                queryBinder.bind(1, id_context);
            } else if (id_context != -1) {
                queryBinder.bind(0, id_context);
            } else if (id_snap != -1) {
                queryBinder.bind(0, id_snap);
            }
            return queryBinder.map((rs, ctx) -> new SnapshotLight(rs.getInt(SnapConst.ID_SNAP),
                    rs.getTimestamp(SnapConst.time), rs.getString(SnapConst.snap_comment))).list();
        }

    }

    /**
     * Retrieves the context identifier to wich the given snapshot identifier is
     * associated.
     *
     * @param snapID the snapshot identifier
     * @return the associated context identifier
     * @throws SnapshotingException
     */
    public int getContextID(final int snapID) throws SnapshotingException {
        int res = -1;
        ResultSet rset = null;
        PreparedStatement preparedStatement = null;
        Connection conn = null;
        String query;
        final String select_field = SnapConst.ID_CONTEXT;
        final String table_1 = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String clause_1 = SnapConst.ID_SNAP + " like ?";
        query = "SELECT " + select_field + " FROM " + table_1 + " WHERE " + clause_1;
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, snapID);
            rset = preparedStatement.executeQuery();
            // Gets the result of the query
            if (rset.next()) {
                res = rset.getInt(1);
                preparedStatement.close();
            }
        } catch (final SQLException e) {
            e.printStackTrace();

            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getContextID() method...";
            final String desc = "";
            final String queryDebug = query + " => " + snapID;
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
        return res;
    }

    private void selectScalarRO(final List<SnapAttributeExtract> attributes, int snapID, String table)
            throws SnapshotingException, SQLException {
        if (attributes.isEmpty()) {
            return;
        }
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        // SELECT id_att, value FROM t_sc_num_1val WHERE id_snap = ?
        final String query = "SELECT " + SnapConst.ID_ATT + "," + SnapConst.value + " FROM " + getDbSchema() + "."
                + table + " WHERE " + SnapConst.ID_SNAP + "=" + snapID + " AND " + SnapConst.ID_ATT
                + " IN (" + String.join(", ",
                attributes.stream().map(s -> String.valueOf(s.getAttId())).collect(Collectors.toList()))
                + ")";
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int attId = resultSet.getInt(1);
                final SnapAttributeExtract snapAttributeExtract = attributes.stream()
                        .filter(att -> att.getAttId() == attId).findFirst().get();
                snapAttributeExtract.setValue(
                        TangoJAVAUtils.castResultSetAsObject(snapAttributeExtract.getDataType(), resultSet, 2),
                        Boolean.valueOf(resultSet.wasNull()));
            }
        } finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    private void selectRO(final List<SnapAttributeExtract> attributes, int snapID,
                          final String tableName) throws SnapshotingException, SQLException {

        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<String> scpectrumROAttId = attributes.stream()
                .filter(s -> s.getDataFormat() == AttrDataFormat._SPECTRUM
                        && (s.getWritable() == AttrWriteType._READ || s.getWritable() == AttrWriteType._WRITE))
                .map(s -> String.valueOf(s.getAttId())).collect(Collectors.toList());

        if (scpectrumROAttId.isEmpty()) {
            return;
        }
        // SELECT id_att, value FROM t_sp_1val WHERE id_snap = ?
        final String query = "SELECT " + SnapConst.ID_ATT + "," + SnapConst.value + " FROM " + getDbSchema() + "."
                + tableName + " WHERE " + SnapConst.ID_SNAP + "=" + snapID + " AND " + SnapConst.ID_ATT + " IN ("
                + String.join(", ", scpectrumROAttId) + ")";
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int attId = resultSet.getInt(1);
                final SnapAttributeExtract snapAttributeExtract = attributes.stream()
                        .filter(att -> att.getAttId() == attId).findFirst().get();
                // fill data
                final Clob readClob = resultSet.getClob(2);
                if (readClob != null) {
                    final String value = readClob.getSubString(1, (int) readClob.length());
                    NullableData<?> data = getSpectrumValue(value, null, snapAttributeExtract.getDataType(),
                            false);
                    snapAttributeExtract.setValue(data.getValue(), data.getNullElements());
                }
            }
        } finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    private void selectRW(final List<SnapAttributeExtract> attributes, final int snapID,
                          final String tableName) throws SnapshotingException, SQLException {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        // SELECT id_att, read_value, write_value, dim_x FROM
        // t_sp_2val WHERE id_snap = ?

        List<String> scpectrumRWAttId = attributes.stream().filter(s -> s.getDataFormat() == AttrDataFormat._SPECTRUM
                        && (s.getWritable() == AttrWriteType._READ_WITH_WRITE || s.getWritable() == AttrWriteType._READ_WRITE))
                .map(s -> String.valueOf(s.getAttId())).collect(Collectors.toList());
        if (scpectrumRWAttId.isEmpty()) {
            return;
        }

        final String query = "SELECT " + SnapConst.ID_ATT + "," + SnapConst.read_value + "," + SnapConst.write_value
                + "," + SnapConst.dim_x + " FROM " + getDbSchema() + "." + tableName + " WHERE " + SnapConst.ID_SNAP
                + "=" + snapID + " AND " + SnapConst.ID_ATT + " IN (" + String.join(", ", scpectrumRWAttId) + ")";
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int attId = resultSet.getInt(1);
                final SnapAttributeExtract snapAttributeExtract = attributes.stream()
                        .filter(att -> att.getAttId() == attId).findFirst().get();
                // fill data
                String readValue = null;
                String writeValue = null;
                final Clob readClob = resultSet.getClob(2);
                if (readClob != null) {
                    readValue = readClob.getSubString(1, (int) readClob.length());
                }
                final Clob writeClob = resultSet.getClob(3);
                if (writeClob != null) {
                    writeValue = writeClob.getSubString(1, (int) writeClob.length());
                }
                NullableData<?> data = getSpectrumValue(readValue, writeValue,
                        snapAttributeExtract.getDataType(), true);
                snapAttributeExtract.setValue(data.getValue(), data.getNullElements());
                snapAttributeExtract.setDimX(resultSet.getInt(4));
            }
        } finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    private void selectScalarRW(final List<SnapAttributeExtract> attributes, int snapID, String table)
            throws SnapshotingException, SQLException {
        if (attributes.isEmpty()) {
            return;
        }
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        // SELECT id_att, read_value, write_value FROM t_sc_num_2val WHERE
        // id_snap = ?
        final String query = "SELECT " + SnapConst.ID_ATT + "," + SnapConst.read_value + "," + SnapConst.write_value
                + " FROM " + getDbSchema() + "." + table + " WHERE " + SnapConst.ID_SNAP + "=" + snapID + " AND "
                + SnapConst.ID_ATT + " IN (" + String.join(", ",
                attributes.stream().map(s -> String.valueOf(s.getAttId())).collect(Collectors.toList()))
                + ")";
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int attId = resultSet.getInt(1);
                final SnapAttributeExtract snapAttributeExtract = attributes.stream()
                        .filter(att -> att.getAttId() == attId).findFirst().get();
                // fill data
                final Object value = TangoJAVAUtils.initPrimitiveArray(snapAttributeExtract.getDataType(), 2);
                final boolean[] nullElements = new boolean[2];

                Array.set(value, 0, TangoJAVAUtils.castResultSetAsPrimitive(snapAttributeExtract.getDataType(),
                        resultSet, 2));
                nullElements[0] = resultSet.wasNull();
                Array.set(value, 1, TangoJAVAUtils.castResultSetAsPrimitive(snapAttributeExtract.getDataType(),
                        resultSet, 3));
                nullElements[1] = resultSet.wasNull();

                snapAttributeExtract.setValue(value, nullElements);
            }

        } finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    /**
     * Retrieve a snapshot results'
     *
     * @param attributes the attributes to retrieve. Will contain results after
     *                   execution
     * @param snapID     The snap ID
     * @throws SnapshotingException
     */
    public void getSnapResults(final List<SnapAttributeExtract> attributes, final int snapID)
            throws SnapshotingException {

        // one sql request per table
        List<SnapAttributeExtract> numericScalarRO = attributes.stream()
                .filter(s -> s.getDataFormat() == AttrDataFormat._SCALAR
                        && s.getDataType() != TangoConst.Tango_DEV_STRING
                        && (s.getWritable() == AttrWriteType._READ || s.getWritable() == AttrWriteType._WRITE))
                .collect(Collectors.toList());

        List<SnapAttributeExtract> stringScalarRO = attributes.stream()
                .filter(s -> s.getDataFormat() == AttrDataFormat._SCALAR
                        && s.getDataType() == TangoConst.Tango_DEV_STRING
                        && (s.getWritable() == AttrWriteType._READ || s.getWritable() == AttrWriteType._WRITE))
                .collect(Collectors.toList());

        List<SnapAttributeExtract> numericScalarRW = attributes.stream()
                .filter(s -> s.getDataFormat() == AttrDataFormat._SCALAR
                        && s.getDataType() != TangoConst.Tango_DEV_STRING
                        && (s.getWritable() == AttrWriteType._READ_WITH_WRITE || s.getWritable() == AttrWriteType._READ_WRITE))
                .collect(Collectors.toList());

        List<SnapAttributeExtract> stringScalarRW = attributes.stream()
                .filter(s -> s.getDataFormat() == AttrDataFormat._SCALAR
                        && s.getDataType() == TangoConst.Tango_DEV_STRING
                        && (s.getWritable() == AttrWriteType._READ_WITH_WRITE || s.getWritable() == AttrWriteType._READ_WRITE))
                .collect(Collectors.toList());

        try {
            // --- Get scalar numeric read only values

            // Avoid Oracle DB error ORA-01795: maximum number of expressions in a list is 1000
            final List<List<SnapAttributeExtract>> chunks = Lists.partition(numericScalarRO, MAX_SELECT_SIZE);
            for (List<SnapAttributeExtract> chunk : chunks) {
                selectScalarRO(chunk, snapID, SnapConst.T_SC_NUM_1VAL);
            }
        } catch (SQLException | SnapshotingException e) {
            logger.error(ERROR_EXTRACTING_SNAPSHOT_DATA, e);
        }
        try {
            // --- Get scalar numeric read write values
            final List<List<SnapAttributeExtract>> chunks = Lists.partition(numericScalarRW, MAX_SELECT_SIZE);
            for (List<SnapAttributeExtract> chunk : chunks) {
                selectScalarRW(chunk, snapID, SnapConst.T_SC_NUM_2VAL);
            }
        } catch (SQLException | SnapshotingException e) {
            logger.error(ERROR_EXTRACTING_SNAPSHOT_DATA, e);
        }

        try {
            // --- Get scalar string read only values
            final List<List<SnapAttributeExtract>> chunks = Lists.partition(stringScalarRO, MAX_SELECT_SIZE);
            for (List<SnapAttributeExtract> chunk : chunks) {
                selectScalarRO(chunk, snapID, SnapConst.T_SC_STR_1VAL);
            }
        } catch (SQLException | SnapshotingException e) {
            logger.error(ERROR_EXTRACTING_SNAPSHOT_DATA, e);
        }

        try {
            // --- Get scalar numeric read write values
            final List<List<SnapAttributeExtract>> chunks = Lists.partition(stringScalarRW, MAX_SELECT_SIZE);
            for (List<SnapAttributeExtract> chunk : chunks) {
                selectScalarRW(chunk, snapID, SnapConst.T_SC_STR_2VAL);
            }
        } catch (SQLException | SnapshotingException e) {
            logger.error(ERROR_EXTRACTING_SNAPSHOT_DATA, e);
        }
        try {
            // --- Get spectrum read values
            final List<List<SnapAttributeExtract>> chunks = Lists.partition(attributes, MAX_SELECT_SIZE);
            for (List<SnapAttributeExtract> chunk : chunks) {
                selectRO(chunk, snapID, SnapConst.T_SP_1VAL);
            }
        } catch (SQLException | SnapshotingException e) {
            logger.error(ERROR_EXTRACTING_SNAPSHOT_DATA, e);
        }
        try {
            // --- Get spectrum read write values
            final List<List<SnapAttributeExtract>> chunks = Lists.partition(attributes, MAX_SELECT_SIZE);
            for (List<SnapAttributeExtract> chunk : chunks) {
                selectRW(chunk, snapID, SnapConst.T_SP_2VAL);
            }
        } catch (SQLException | SnapshotingException e) {
            logger.error(ERROR_EXTRACTING_SNAPSHOT_DATA, e);
        }
    }

    private NullableData<?> getSpectrumValue(final String readStr, final String writeStr, final int dataType,
                                             final boolean returnAsReadWrite) {
        int readSize = 0, writeSize = 0;
        StringTokenizer readTokenizer;
        String readString = null, writeString = null;
        if (readStr == null) {
            readTokenizer = null;
        } else {
            readString = readStr.replaceAll("\\[", "").replaceAll("\\]", "");
            if ("".equals(readString) || "null".equals(readString) || "NaN".equalsIgnoreCase(readString)) {
                readTokenizer = null;
            } else {
                readTokenizer = new StringTokenizer(readString, GlobalConst.CLOB_SEPARATOR);
                readSize += readTokenizer.countTokens();
            }
        }

        StringTokenizer writeTokenizer;
        if (writeStr == null) {
            writeTokenizer = null;
        } else {
            writeString = writeStr.replaceAll("\\[", "").replaceAll("\\]", "");
            if ("".equals(writeString) || "null".equals(writeString) || "NaN".equalsIgnoreCase(writeString)) {
                writeTokenizer = null;
            } else {
                writeTokenizer = new StringTokenizer(writeString, GlobalConst.CLOB_SEPARATOR);
                writeSize += writeTokenizer.countTokens();
            }
        }

        boolean[] nullRead = null;
        boolean[] nullWrite = null;

        Object valueArr_read = TangoJAVAUtils.initPrimitiveArray(dataType, readSize);
        Object valueArr_write = null;


        if (returnAsReadWrite) {
            valueArr_write = TangoJAVAUtils.initPrimitiveArray(dataType, writeSize);
        }
        int i = 0;
        if (readTokenizer != null) {
            nullRead = new boolean[readSize];
            while (readTokenizer.hasMoreTokens()) {
                final String currentValRead = readTokenizer.nextToken();
                if (currentValRead == null || currentValRead.trim().equals("")) {
                    break;
                }
                if (TangoJAVAUtils.isNullOrNaN(currentValRead)) {
                    nullRead[i] = true;
                    Array.set(valueArr_read, i, TangoJAVAUtils.defaultValue(dataType));
                } else {
                    Array.set(valueArr_read, i, TangoJAVAUtils.cast(dataType, currentValRead));
                }
                i++;
            }
        }

        if (writeTokenizer != null) {
            nullWrite = new boolean[readSize];
            i = 0;
            while (writeTokenizer.hasMoreTokens()) {
                final String currentValWrite = writeTokenizer.nextToken();
                if (currentValWrite == null || currentValWrite.trim().equals("")) {
                    break;
                }
                if (TangoJAVAUtils.isNullOrNaN(currentValWrite)) {
                    nullWrite[i] = true;
                    Array.set(valueArr_write, i, TangoJAVAUtils.defaultValue(dataType));
                } else {
                    Array.set(valueArr_write, i, TangoJAVAUtils.cast(dataType, currentValWrite));
                }
                i++;
            }
        }

        if (returnAsReadWrite) {
            final Object[] result = new Object[]{valueArr_read, valueArr_write};
            final boolean[][] nullElements = new boolean[][]{nullRead, nullWrite};

            return new NullableData<boolean[][]>(result, nullElements);
        } else {
            return new NullableData<boolean[]>(valueArr_read, nullRead);
        }
    }

    /*****************************************************************************
     *
     *
     * Generals methods used to update SNAP (CREATE / ALTER) (Insert)
     *
     ****************************************************************************/

    /**
     * This method registers a given attribute into the database for snapshots
     * This methos does not take care of id parameter of the given attribute as
     * this parameter is managed in the database side (autoincrement).
     *
     * @param snapAttributeHeavy the attribute to register
     * @throws SnapshotingException
     */
    public void registerAttribute(final AttributeHeavy snapAttributeHeavy) throws SnapshotingException {
        Connection conn = null;
        Object[] listValues = new Object[]{snapAttributeHeavy.getAttributeCompleteName(),
                snapAttributeHeavy.getAttribute_device_name(), snapAttributeHeavy.getDomain(),
                snapAttributeHeavy.getFamily(), snapAttributeHeavy.getMember(), snapAttributeHeavy.getAttribute_name(),
                snapAttributeHeavy.getDataType(), snapAttributeHeavy.getDataFormat(), snapAttributeHeavy.getWritable(),
                snapAttributeHeavy.getMax_dim_x(), snapAttributeHeavy.getMax_dim_y(), snapAttributeHeavy.getLevel(),
                snapAttributeHeavy.getCtrl_sys(), snapAttributeHeavy.getArchivable(),
                snapAttributeHeavy.getSubstitute()};

        StringBuilder valueConcat = new StringBuilder();

        PreparedStatement preparedStatement = null;
        String query = "";
        if (params.getDbType() == DataBaseType.MYSQL) {

            final String tableName = getDbSchema() + "." + SnapConst.AST;

            // Create and execute the SQL query string
            // Build the query string
            String insert_fields = String.join(", ", SnapConst.TAB_DEF);

            String insert_values = "";
            for (int i = 1; i < SnapConst.TAB_DEF.length - 1; i++) {
                insert_values = insert_values + "?" + ", ";
            }
            insert_values = insert_values + "?";

            query = "INSERT INTO " + tableName + " (" + insert_fields + ") " + " VALUES(" + insert_values + " ) ";
        } else if (params.getDbType() == DataBaseType.ORACLE) {
            final String procName = "RSA";
            query = "{call " + getDbSchema() + "." + procName + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        }

        try {
            conn = connector.getConnection();
            preparedStatement = params.getDbType() == DataBaseType.MYSQL ? conn.prepareStatement(query)
                    : conn.prepareCall(query);
            if (params.getDbType() == DataBaseType.MYSQL) {
                preparedStatement.setDate(1, new java.sql.Date(snapAttributeHeavy.getRegistration_time().getTime()));
            } else {
                preparedStatement.setTimestamp(1, snapAttributeHeavy.getRegistration_time());
            }

            valueConcat.append("(");
            for (int i = 2; i < listValues.length + 2; ++i) {
                Object value = listValues[i - 2];
                if (value instanceof Integer) {
                    int intValue = ((Integer) value).intValue();
                    preparedStatement.setInt(i, intValue);
                    valueConcat.append(intValue);
                }
                if (value instanceof String) {
                    preparedStatement.setString(i, (String) value);
                    valueConcat.append((String) value);
                }
                valueConcat.append(",");
            }
            valueConcat.append(")");

            preparedStatement.executeUpdate();
            listValues = null;
            valueConcat = null;

            // preparedStatement.close();
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.registerAttribute() method...";
            final String desc = "";
            final String queryDebug = query + " => " + valueConcat.toString();
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(conn);
        }

    }

    /**
     * This method registers a new context in the database for snapshots.
     *
     * @param snapContext the new context to register into the database for snapshots.
     * @return the context identifier (int) associated to the new registered
     * context.
     * @throws SnapshotingException
     */
    public int create_context(final SnapContext snapContext) throws SnapshotingException {
        // TODO refact
        int contextID = snapContext.getId();
        final String tableName = getDbSchema() + "." + SnapConst.CONTEXT;
        final java.util.List<Integer> contextIDList = new ArrayList<Integer>();
        PreparedStatement prest = null;
        Connection conn = null;
        final String queryCurrentContext = "select " + SnapConst.ID_CONTEXT + " from " + tableName;
        try {
            conn = connector.getConnection();
            // check if context exists
            prest = conn.prepareStatement(queryCurrentContext);
            final ResultSet rs = prest.executeQuery();
            while (rs.next()) {
                final int contextIDDB = rs.getInt(1);
                contextIDList.add(contextIDDB);
            }
            PreparedStatement preparedStatement = null;
            // context already exists, update its values
            if (contextIDList.contains(contextID)) {
                // update snap.context SET name=?, author=?, reason=?,
                // description=? where id =?;
                final String updateQuery = "UPDATE " + tableName + " SET " + SnapConst.name + "= ?, "
                        + SnapConst.author + "= ?, " + SnapConst.reason + "= ?, " + SnapConst.description + "= ? WHERE "
                        + SnapConst.ID_CONTEXT + "= ?";
                try {

                    preparedStatement = conn.prepareStatement(updateQuery);
                    preparedStatement.setString(1, snapContext.getName());
                    preparedStatement.setString(2, snapContext.getAuthor_name());
                    preparedStatement.setString(3, snapContext.getReason());
                    preparedStatement.setString(4, snapContext.getDescription());
                    preparedStatement.setInt(5, contextID);

                    preparedStatement.executeUpdate();

                } catch (final SQLException e) {
                    final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
                    final String reason = "Failed while executing DataBaseAPI.create_context() method...";
                    final String desc = "";
                    final String queryDebug = updateQuery + " => (" + snapContext.getName() + ","
                            + snapContext.getAuthor_name() + "," + snapContext.getReason() + ","
                            + snapContext.getDescription() + "," + contextID + ")";
                    throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
                } finally {
                    closeStatement(preparedStatement);
                }

            } else { // context does not exists, create it
                if (params.getDbType() == DataBaseType.MYSQL) {
                    // Create and execute the SQL query string
                    // Build the query string
                    final String insert_fields = SnapConst.TAB_CONTEXT[1] + ", " + SnapConst.name + ", "
                            + SnapConst.TAB_CONTEXT[3] + ", " + SnapConst.TAB_CONTEXT[4] + ", "
                            + SnapConst.TAB_CONTEXT[5];
                    final String insert_values = "?,?,?,?,?";

                    final String query = "INSERT INTO " + tableName + " (" + insert_fields + ")" + "VALUES ("
                            + insert_values + ")";
                    try {

                        preparedStatement = conn.prepareStatement(query);
                        preparedStatement.setDate(1, new java.sql.Date(snapContext.getCreation_date().getTime()));
                        preparedStatement.setString(2, snapContext.getName());
                        preparedStatement.setString(3, snapContext.getAuthor_name());
                        preparedStatement.setString(4, snapContext.getReason());
                        preparedStatement.setString(5, snapContext.getDescription());
                        preparedStatement.executeUpdate();
                        contextID = getMaxContextID();// (snapContext);
                        snapContext.setId(contextID);
                    } catch (final SQLException e) {
                        final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
                        final String reason = "Failed while executing DataBaseAPI.create_context() method...";
                        final String desc = "";
                        final String queryDebug = query + " => (" + snapContext.getCreation_date() + ","
                                + snapContext.getName() + "," + snapContext.getAuthor_name() + ","
                                + snapContext.getReason() + "," + snapContext.getDescription() + ")";
                        throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
                    } finally {
                        closeStatement(preparedStatement);
                    }
                } else if (params.getDbType() == DataBaseType.ORACLE) {
                    CallableStatement callableStatement = null;
                    final String procName = "register_context";
                    final String query = "{? = call " + getDbSchema() + "." + procName + "(?, ?, ?, ?, ?)}";
                    try {
                        callableStatement = conn.prepareCall(query);
                        callableStatement.registerOutParameter(1, Types.INTEGER);
                        callableStatement.setInt(1, snapContext.getId());
                        callableStatement.setDate(2, new java.sql.Date(snapContext.getCreation_date().getTime()));
                        callableStatement.setString(3, snapContext.getName());
                        callableStatement.setString(4, snapContext.getAuthor_name());
                        callableStatement.setString(5, snapContext.getReason());
                        callableStatement.setString(6, snapContext.getDescription());
                        callableStatement.executeQuery();
                        // Gets the result of the query
                        contextID = callableStatement.getInt(1);
                        snapContext.setId(contextID);
                    } catch (final SQLException e) {
                        final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
                        final String reason = "Failed while executing DataBaseAPI.create_context() method...";
                        final String desc = "";
                        final String queryDebug = query + " => (" + snapContext.getId() + ","
                                + snapContext.getCreation_date() + "," + snapContext.getName() + ","
                                + snapContext.getAuthor_name() + "," + snapContext.getReason() + ","
                                + snapContext.getDescription() + ")";
                        throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
                    } finally {
                        closeStatement(callableStatement);
                    }
                }
            }
        } catch (final SQLException e1) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.create_context() method...";
            final String desc = "";
            throw new SnapshotingException(message, reason, queryCurrentContext, ErrSeverity.PANIC, desc, "", e1);
        } finally {
            closeStatement(prest);
            closeConnection(conn);
        }

        setContextAssociatedAttributes(snapContext);
        return contextID;
    }

    public List<AttributeHeavy> getContextAttributes(final int contextID, final String whereClause)
            throws SnapshotingException {
        final List<AttributeHeavy> result = new ArrayList<AttributeHeavy>();

        // Create and execute the SQL query string
        final String ast = getDbSchema() + "." + SnapConst.AST;
        final String context = getDbSchema() + "." + SnapConst.CONTEXT;
        final String list = getDbSchema() + "." + SnapConst.LIST;
        final String tables = ast + ", " + context + ", " + list;
        final String selectFields = SnapConst.ID + ", " + ast + "." + SnapConst.time + ", " + SnapConst.fullName + ", "
                + SnapConst.device + ", " + SnapConst.domain + ", " + SnapConst.family + ", " + SnapConst.member + ", "
                + SnapConst.attName + ", " + SnapConst.dataType + ", " + SnapConst.dataFormat + ", "
                + SnapConst.writable + ", " + SnapConst.max_dim_x + ", " + SnapConst.max_dim_y + ", " + SnapConst.levelg
                + ", " + SnapConst.facility + ", " + SnapConst.archivable + ", " + SnapConst.substitute;

        // final String selectFields = "*";
        final String clause1 = SnapConst.AST + "." + SnapConst.ID + " = " + SnapConst.LIST + "."
                + SnapConst.ID_ATT;
        final String clause2 = SnapConst.LIST + "." + SnapConst.ID_CONTEXT + " = " + SnapConst.CONTEXT + "."
                + SnapConst.ID_CONTEXT;
        final String clause3 = SnapConst.CONTEXT + "." + SnapConst.ID_CONTEXT + " = ?";
        // the SQL request is : select DISTINCT full_name from snap.ast where
        // ast.id = list.id_att and list.id_context = context.id_context and
        // context.name = 'nom';
        String query = "SELECT " + selectFields + " FROM " + tables
                + (whereClause.equals("") ? " WHERE " : whereClause + " AND ") + clause1 + " AND " + clause2 + " AND "
                + clause3;

        PreparedStatement statement = null;
        ResultSet rset = null;
        Connection conn = null;
        try {
            conn = connector.getConnection();
            statement = conn.prepareStatement(query);
            statement.setInt(1, contextID);
            rset = statement.executeQuery();
            // Gets the result of the query
            while (rset.next()) {
                final AttributeHeavy snapAttributeHeavy = new AttributeHeavy();
                snapAttributeHeavy.setAttributeId(rset.getInt(1));
                snapAttributeHeavy.setRegistration_time(rset.getTimestamp(2));
                snapAttributeHeavy.setAttributeCompleteName(rset.getString(3));
                snapAttributeHeavy.setAttribute_device_name(rset.getString(4));
                snapAttributeHeavy.setDomain(rset.getString(5));
                snapAttributeHeavy.setFamily(rset.getString(6));
                snapAttributeHeavy.setMember(rset.getString(7));
                snapAttributeHeavy.setAttribute_name(rset.getString(8));
                snapAttributeHeavy.setDataType(rset.getInt(9));
                snapAttributeHeavy.setDataFormat(rset.getInt(10));
                snapAttributeHeavy.setWritable(rset.getInt(11));
                snapAttributeHeavy.setMax_dim_x(rset.getInt(12));
                snapAttributeHeavy.setMax_dim_y(rset.getInt(13));
                snapAttributeHeavy.setLevel(rset.getInt(14));
                snapAttributeHeavy.setCtrl_sys(rset.getString(15)); // facility
                snapAttributeHeavy.setArchivable(rset.getInt(16));
                snapAttributeHeavy.setSubstitute(rset.getInt(17));
                result.add(snapAttributeHeavy);
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getAttDefinitionData() method...";
            final String desc = "";
            final String queryDebug = query + " => " + contextID;
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(statement);
            closeConnection(conn);
        }
        return result;
    }

    /**
     * Retrieve ID, data_type, data_format, writable of some attributes
     *
     * @param attributeNames The attribute names
     * @return
     * @throws SnapshotingException
     */
    public SnapAttributeExtract[] getAttributeConfig(final String... attributeNames) throws SnapshotingException {
        final SnapAttributeExtract[] result = new SnapAttributeExtract[attributeNames.length];
        final String selectFields = SnapConst.fullName + ", " + // full_name
                SnapConst.ID + ", " + // attribute_id
                SnapConst.dataType + ", " + // data_type
                SnapConst.dataFormat + ", " + // data_format
                SnapConst.writable; // writable
        final String astTableName = getDbSchema() + "." + SnapConst.AST;
        // SELECT ID, data_type, data_format, writable FROM snap.ast WHERE
        // full_name = 'attributeName'
        String query = "SELECT " + selectFields + " FROM " + astTableName + " WHERE ";
        for (int i = 0; i < attributeNames.length; i++) {
            if (i > 0 && i < attributeNames.length) {
                query = query + " OR ";
            }
            query = query + SnapConst.fullName + " = '" + attributeNames[i] + "'";
        }
        PreparedStatement preparedStatement = null;
        ResultSet rset = null;
        Connection conn = null;
        Map<String, AttributeLight> extractMap = new HashMap<String, AttributeLight>();
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            rset = preparedStatement.executeQuery();
            // Gets the result of the query
            while (rset.next()) {
                final AttributeLight snapAttributeLight = new AttributeLight();
                snapAttributeLight.setAttributeCompleteName(rset.getString(SnapConst.fullName));
                snapAttributeLight.setAttributeId(rset.getInt(SnapConst.ID));
                snapAttributeLight.setDataType(rset.getInt(SnapConst.dataType));
                snapAttributeLight.setDataFormat(rset.getInt(SnapConst.dataFormat));
                snapAttributeLight.setWritable(rset.getInt(SnapConst.writable));
                extractMap.put(snapAttributeLight.getAttributeCompleteName(), snapAttributeLight);
            }
            if (extractMap.size() != attributeNames.length) {
                throw new SnapshotingException(Arrays.toString(attributeNames) + " are not all in snapshot db");
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getContextAssociatedAttributes() method...";
            final String desc = "";
            throw new SnapshotingException(message, reason, query, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
        int index = 0;
        for (String attributeName : attributeNames) {
            AttributeLight snapAttributeLight = extractMap.remove(attributeName);
            if (snapAttributeLight == null) {
                // case sensitivity problem
                String nameToUse = null;
                for (Entry<String, AttributeLight> entry : extractMap.entrySet()) {
                    String tmpName = entry.getKey();
                    if ((tmpName != null) && tmpName.equalsIgnoreCase(attributeName)) {
                        nameToUse = tmpName;
                        snapAttributeLight = entry.getValue();
                    }
                }
                if (nameToUse != null) {
                    extractMap.remove(nameToUse);
                }
            }
            if (snapAttributeLight != null) {
                result[index++] = new SnapAttributeExtract(snapAttributeLight);
            }
        }
        extractMap.clear();
        return result;
    }

    /**
     * This method returns the list of attributes associated to a context
     *
     * @param id_context The given context's identifier
     * @return The list of attributes associated to the given context
     * @throws SnapshotingException
     */
    public List<SnapAttributeExtract> getContextAssociatedAttributes(final int id_context) throws SnapshotingException {
        final List<SnapAttributeExtract> attibutesList = new ArrayList<SnapAttributeExtract>();
        PreparedStatement preparedStatement = null;
        ResultSet rset = null;
        Connection conn = null;

        // Create and execute the SQL query string
        String query = "";
        final String table_name_1 = getDbSchema() + "." + SnapConst.AST;
        final String table_name_2 = getDbSchema() + "." + SnapConst.CONTEXT;
        final String table_name_3 = getDbSchema() + "." + SnapConst.LIST;
        final String tables = table_name_1 + ", " + table_name_2 + ", " + table_name_3;
        final String select_fields = SnapConst.fullName + ", " + // full_name
                SnapConst.ID + ", " + // attribute_id
                SnapConst.dataType + ", " + // data_type
                SnapConst.dataFormat + ", " + // data_format
                SnapConst.writable; // writable
        final String clause_1 = SnapConst.AST + "." + SnapConst.ID + " = " + SnapConst.LIST + "."
                + SnapConst.ID_ATT;
        final String clause_2 = SnapConst.LIST + "." + SnapConst.ID_CONTEXT + " = " + SnapConst.CONTEXT + "."
                + SnapConst.ID_CONTEXT;
        final String clause_3 = SnapConst.CONTEXT + "." + SnapConst.ID_CONTEXT + " = ?";
        // the SQL request is : select DISTINCT full_name from snap.ast where
        // ast.id = list.id_att and list.id_context = context.id_context and
        // context.name = 'nom';
        query = "SELECT DISTINCT " + select_fields + " FROM " + tables + " WHERE " + clause_1 + " AND " + clause_2
                + " AND " + clause_3;

        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);

            preparedStatement.setInt(1, id_context);
            rset = preparedStatement.executeQuery();
            // Gets the result of the query
            while (rset.next()) {
                final AttributeLight snapAttributeLight = new AttributeLight();
                snapAttributeLight.setAttributeCompleteName(rset.getString(SnapConst.fullName));
                snapAttributeLight.setAttributeId(rset.getInt(SnapConst.ID));
                snapAttributeLight.setDataType(rset.getInt(SnapConst.dataType));
                snapAttributeLight.setDataFormat(rset.getInt(SnapConst.dataFormat));
                snapAttributeLight.setWritable(rset.getInt(SnapConst.writable));
                final SnapAttributeExtract attr = new SnapAttributeExtract(snapAttributeLight);
                // set default value
                attr.setValue(null, null);
                attibutesList.add(new SnapAttributeExtract(snapAttributeLight));
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getContextAssociatedAttributes() method...";
            final String desc = "";
            final String queryDebug = query + " => " + id_context;
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
        return attibutesList;
    }

    /**
     * Registers the link between an attribute and a given context.
     *
     * @param snapContext
     * @throws SnapshotingException
     */
    public void setContextAssociatedAttributes(final SnapContext snapContext) throws SnapshotingException {
        final int id_context = snapContext.getId();
        final List<AttributeLight> attList = snapContext.getAttributeList();
        PreparedStatement preparedStatement = null;
        final String tableName = getDbSchema() + "." + SnapConst.LIST;

        String query;
        final String insert_fields = SnapConst.ID_CONTEXT + ", " + SnapConst.ID_ATT;
        query = "INSERT INTO " + tableName + " (" + insert_fields + ")" + " VALUES (?,?)";
        PreparedStatement prest = null;
        Connection conn = null;
        final String selectQuery = "select " + SnapConst.ID_ATT + " from " + tableName + " where "
                + SnapConst.ID_CONTEXT + " = " + id_context;
        try {
            conn = connector.getConnection();
            prest = conn.prepareStatement(selectQuery);
            final ResultSet rs = prest.executeQuery();
            final List<Integer> attributeIDList = new ArrayList<Integer>();
            while (rs.next()) {
                final int attributeID = rs.getInt(1);
                attributeIDList.add(attributeID);
            }
            for (int i = 0; i < attList.size(); i++) {
                final String att_full_name = attList.get(i).getAttributeCompleteName();
                final int id_att = getAttID(att_full_name);
                if (!attributeIDList.contains(id_att)) {
                    try {
                        preparedStatement = conn.prepareStatement(query);
                        preparedStatement.setInt(1, id_context);
                        preparedStatement.setInt(2, id_att);
                        preparedStatement.executeUpdate();
                    } catch (final SQLException e) {
                        final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
                        final String reason = "Failed while executing DataBaseAPI.setContextAssociatedAttributes() method...";
                        final String desc = "";
                        final String queryDebug = query + " => (" + id_context + "," + id_att + ")";
                        throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
                    } finally {
                        closeStatement(preparedStatement);
                    }
                }
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.setContextAssociatedAttributes() method...";
            final String desc = "";
            throw new SnapshotingException(message, reason, selectQuery, ErrSeverity.PANIC, desc, "", e);
        } catch (final SnapshotingException e) {
            throw e;
        } finally {
            closeConnection(conn);
            closeStatement(prest);
        }
    }

    /**
     * This method registers a new snapshots and retrieves its identifier
     *
     * @param contextID the context identifier to wich the new registered snapshots
     *                  belongs to.
     * @param timestamp the registration timestamp
     * @return the identifier for the new registered snapshot
     * @throws SnapshotingException
     * @see #getMaxContextID()
     * @see #registerSnap(int id_context)
     * @see #getSnapID(int id_context)
     * @see #updateSnapContextID(int id_snap, int initial_context_value)
     */
    public Snapshot createNewSnap(final int contextID, final Timestamp timestamp) throws SnapshotingException {
        Snapshot snapShot = new Snapshot();
        if (params.getDbType() == DataBaseType.MYSQL) {
            snapShot = createNewSnapMySQL(contextID, timestamp);
        } else if (params.getDbType() == DataBaseType.ORACLE) {
            snapShot = createNewSnapOracle(contextID, timestamp);
        }
        return snapShot;
    }

    /**
     * This method registers a new snapshots and retrieves its identifier
     *
     * @param contextID the context identifier to wich the new registered snapshots
     *                  belongs to.
     * @param timestamp the registration timestamp
     * @return the identifier for the new registered snapshot
     * @throws SnapshotingException
     * @see #getMaxContextID()
     * @see #registerSnap(int id_context)
     * @see #getSnapID(int id_context)
     * @see #updateSnapContextID(int id_snap, int initial_context_value)
     */
    private Snapshot createNewSnapMySQL(final int contextID, final Timestamp timestamp) throws SnapshotingException {
        final Snapshot snapShot = new Snapshot();
        // Here we use a special pattern to be sure of the unicity of the
        // snapID...
        int snapID = -1;

        final int maxContextID = getMaxContextID();
        // final int maxContextID = getMaxContextIDMySql();
        /*
         * -- M = max -- R = Nombre_aleatoire( compris entre 1 et 100000 ) + M
         * -- Insérons l'enregistrement, avec R à la place de id_context
         */
        final Random rand = new Random(); // constructeur
        final int alea = rand.nextInt(100000); // génération

        final int pseudoContextRecord = maxContextID + alea;
        registerSnap(pseudoContextRecord);

        /*-- Nous récupérons notre enregistrement --- > ID:*/
        snapID = getSnapID(pseudoContextRecord);

        // -- Plaçons la valeur de l'index dans une variable ID, et mettons
        // à jour la valeur de id_context :
        updateSnapContextID(snapID, contextID);
        snapShot.setId_context(contextID);
        snapShot.setId_snap(snapID);
        snapShot.setSnap_date(timestamp);
        snapShot.setAttribute_List(getContextAssociatedAttributes(contextID));
        return snapShot;
    }

    /**
     * This method registers a new snapshots and retrieves its identifier
     *
     * @param contextID the context identifier to wich the new registered snapshots
     *                  belongs to.
     * @param timestamp the registration timestamp
     * @return the identifier for the new registered snapshot
     * @throws SnapshotingException
     * @see #getMaxContextID()
     * @see #registerSnap(int id_context)
     * @see #getSnapID(int id_context)
     * @see #updateSnapContextID(int id_snap, int initial_context_value)
     */
    private Snapshot createNewSnapOracle(final int contextID, final Timestamp timestamp) throws SnapshotingException {
        final Snapshot snapShot = new Snapshot();
        int snapID = -1;
        if (contextID > getMaxContextID()) {
            return null;
        }
        CallableStatement cstmt_register_snapshot = null;
        final String query = "{? = call " + getDbSchema() + ".register_snapshot(?, ?)}";

        Connection conn = null;
        try {
            conn = connector.getConnection();
            cstmt_register_snapshot = conn.prepareCall(query);
            cstmt_register_snapshot.registerOutParameter(1, Types.INTEGER);
            cstmt_register_snapshot.setInt(2, contextID);
            cstmt_register_snapshot.setTimestamp(3, timestamp);
            cstmt_register_snapshot.executeQuery();
            // Gets the result of the query
            snapID = cstmt_register_snapshot.getInt(1);
            cstmt_register_snapshot.close();
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.createNewSnapOracle() method...";
            final String desc = "";
            final String queryDebug = query + " => (" + Types.INTEGER + "," + contextID + "," + timestamp + ")";
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeStatement(cstmt_register_snapshot);
            closeConnection(conn);
        }

        snapShot.setId_context(contextID);
        snapShot.setId_snap(snapID);
        snapShot.setSnap_date(timestamp);
        snapShot.setAttribute_List(getContextAssociatedAttributes(contextID));
        return snapShot;
    }

    /**
     * This method is used to update the ContextID for a snap record
     *
     * @param id_snap
     * @param initial_context_value
     * @throws SnapshotingException
     * @see #getMaxContextID()
     * @see #registerSnap(int id_context)
     * @see #getSnapID(int id_context)
     * @see #createNewSnap(int contextID, Timestamp timestamp)
     */
    private void updateSnapContextID(final int id_snap, final int initial_context_value) throws SnapshotingException {
        PreparedStatement preparedStatement = null;
        final String table = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String query = "UPDATE " + table + " SET " + SnapConst.ID_CONTEXT + "=? " + "WHERE " + SnapConst.ID_SNAP
                + "=?";
        Connection conn = null;

        try {

            conn = connector.getConnection();

            // preparedStatement = dbconn.prepareStatement(query);
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, initial_context_value);
            preparedStatement.setInt(2, id_snap);
            preparedStatement.executeUpdate();
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.updateSnapContextID() method...";
            final String desc = "";
            final String queryDebug = query + " => (" + initial_context_value + "," + id_snap + ")";
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    /**
     * Remplace le commentaire d'un snapshot donne par celui passe en argument.
     * Author : Laure Garda
     *
     * @param id_snap
     * @param new_comment
     * @throws SnapshotingException
     */
    public void updateSnapComment(final int id_snap, final String new_comment) throws SnapshotingException {
        PreparedStatement preparedStatement = null;
        Connection conn = null;
        final String table = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String query = "UPDATE " + table + " SET " + SnapConst.snap_comment + " = '" + new_comment + "' WHERE "
                + SnapConst.ID_SNAP + " = ?";
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, id_snap);
            preparedStatement.executeUpdate();
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.updateSnapComment() method...";
            final String desc = "";
            final String queryDebug = query + " => " + id_snap;
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    /**
     * Get the comment of a snapshot
     *
     * @param snapID the snapshot ID
     * @return the comment
     * @throws SnapshotingException
     */
    public String getSnapComment(final int snapID) throws SnapshotingException {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        final String table = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String query = "SELECT " + SnapConst.snap_comment + " FROM " + table + " WHERE " + SnapConst.ID_SNAP
                + " = " + snapID;
        String comment = "";
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                comment = resultSet.getString(1);
            }
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.updateSnapComment() method...";
            final String desc = "";
            throw new SnapshotingException(message, reason, query, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(conn);
            closeResultSet(resultSet);
        }
        return comment;
    }

    /**
     * This methos is used to retrieve the snapID when registered with a
     * temporary generated and unique contextID
     *
     * @param id_context
     * @return the newly created snap identifier
     * @throws SnapshotingException
     * @see #getMaxContextID()
     * @see #registerSnap(int id_context)
     * @see #updateSnapContextID(int id_snap, int initial_context_value)
     * @see #createNewSnap(int contextID, Timestamp timestamp)
     */
    private int getSnapID(final int id_context) throws SnapshotingException {
        int idSnap = -1;
        final String table = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String selectField = SnapConst.ID_SNAP;
        final String clause_1 = SnapConst.ID_CONTEXT + "=?";
        final String query = "SELECT " + selectField + " FROM " + table + " WHERE " + clause_1;
        PreparedStatement preparedStatement = null;
        ResultSet rset = null;
        Connection conn = null;
        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, id_context);
            rset = preparedStatement.executeQuery();
            // Gets the result of the query
            if (rset.next()) {
                idSnap = rset.getInt(1);
            }

            return idSnap;
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.getSnapID() method...";
            final String desc = "";
            final String queryDebug = query + " => " + id_context;
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeResultSet(rset);
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    /**
     * This methods registers a new snapShot in the database, but with a wrong
     * context identifier
     *
     * @param id_context
     * @throws SnapshotingException
     * @see #getMaxContextID()
     * @see #getSnapID(int id_context)
     * @see #updateSnapContextID(int id_snap, int initial_context_value)
     * @see #createNewSnap(int contextID, Timestamp timestamp)
     */
    private Timestamp registerSnap(final int id_context) throws SnapshotingException {
        final java.sql.Timestamp time = new java.sql.Timestamp(new java.util.Date().getTime());
        PreparedStatement preparedStatement = null;
        final String table = getDbSchema() + "." + SnapConst.SNAPSHOT;
        final String insert_fields = SnapConst.ID_CONTEXT + ", " + SnapConst.time;
        final String insert_values = "?, ?";
        final String query = "INSERT INTO " + table + " (" + insert_fields + ") VALUES(" + insert_values + ")";
        Connection conn = null;

        try {
            conn = connector.getConnection();
            preparedStatement = conn.prepareStatement(query);
            // preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, id_context);
            preparedStatement.setTimestamp(2, time);
            preparedStatement.executeUpdate();
            return time;
        } catch (final SQLException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.QUERY_FAILURE;
            final String reason = "Failed while executing DataBaseAPI.registerSnap() method...";
            final String desc = "" + "";
            final String queryDebug = query + " => (" + id_context + "," + time + ")";
            throw new SnapshotingException(message, reason, queryDebug, ErrSeverity.PANIC, desc, "", e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(conn);
        }
    }

    /**
     * @return Returns the currentSpectrumDimX.
     */
    public int getCurrentSpectrumDimX() {
        return currentSpectrumDimX;
    }

    /**
     * @param currentSpectrumDimX The currentSpectrumDimX to set.
     */
    public void setCurrentSpectrumDimX(final int currentSpectrumDimX) {
        this.currentSpectrumDimX = currentSpectrumDimX;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public static class NullableData<T> {
        private final Object value;
        private final T nullElements;

        public NullableData(Object value, T nullElements) {
            super();
            this.value = value;
            this.nullElements = nullElements;
        }

        public Object getValue() {
            return value;
        }

        public T getNullElements() {
            return nullElements;
        }
    }
}
