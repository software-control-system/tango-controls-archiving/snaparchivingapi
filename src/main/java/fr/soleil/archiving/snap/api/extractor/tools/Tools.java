package fr.soleil.archiving.snap.api.extractor.tools;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.snap.api.manager.SnapManagerApi;
import fr.soleil.database.connection.DataBaseParameters.DataBaseType;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A class with a few exception handling methods.
 * 
 * @author CLAISSE
 */
public class Tools {
    private static final Logger LOGGER = LoggerFactory.getLogger(Tools.class);
    public static final String FR_DATE_PATTERN = "dd-MM-yyyy HH:mm:ss.SSS";
    public static final String US_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final DateTimeFormatter FR_DATE_FORMAT = fr.soleil.lib.project.date.DateUtil
            .getDateTimeFormatter(FR_DATE_PATTERN);
    private static final DateTimeFormatter US_DATE_FORMAT = fr.soleil.lib.project.date.DateUtil
            .getDateTimeFormatter(FR_DATE_PATTERN);

    /**
     * Returns a date as a String formated for the database.
     * 
     * @param milliseconds the date in milliseconds
     * @return A date as a String formated for the database.
     */
    public static String formatDate(long milliseconds) {
        DataBaseType type = SnapManagerApi.getSnapDbType();
        String date = ObjectUtils.EMPTY_STRING;
        switch (type) {
            case ORACLE:
                date = DateUtil.milliToString(milliseconds, DateUtil.FR_DATE_PATTERN);
                break;
            case MYSQL:
            default:
                date = DateUtil.milliToString(milliseconds, DateUtil.US_DATE_PATTERN);
        }
        return date;
    }

    /**
     * Converts a Throwable to a DevFailed and throws it.
     * 
     * @param exception The exception to convert
     * @return The DevFailed representation
     * @throws DevFailed Always thrown by definition of the method
     */
    public static void throwDevFailed(Throwable exception) throws DevFailed {
        DevFailed devFailed = new DevFailed();
        devFailed.initCause(exception);
        throw devFailed;
    }

    /**
     * Extracts causes from a Throwable if it is an instance of DevFailed, and prints it.
     * 
     * @param t The exception to log
     */
    public static void printIfDevFailed(Throwable t) {
        t.printStackTrace();

        if (t instanceof DevFailed) {
            DevFailed df = (DevFailed) t;
            if (df.getCause() != null) {
                df.getCause().printStackTrace();
            }

            DevError[] errors = df.errors;
            if (errors != null && errors.length != 0) {
                for (int i = 0; i < errors.length; i++) {
                    DevError error = errors[i];
                    LOGGER.error("desc/" + error.desc + "/origin/" + error.origin + "/reason/" + error.reason);
                }
            }
        }
    }

    /**
     * Cast a string format date (dd-MM-yyyy HH:mm:ss or yyyy-MM-dd HH:mm:ss) into long (number of milliseconds since
     * January 1, 1970)
     * 
     * @param date
     * @return
     * @throws ArchivingException
     */
    public static long stringToMilli(String date) {
        long milli;
        boolean isFr = (date.indexOf("-") != 4);
        int currentLength = date.length();
        String toTheDay = "yyyy-MM-dd";
        if (!isFr) {
            if (currentLength == toTheDay.length()) {
                date += " 00:00:00";
            }
            if (currentLength == (toTheDay.length() + 1)) {
                date += "00:00:00";
            }
            if (currentLength == (toTheDay.length() + 2)) {
                date += "0:00:00";
            }
            if (currentLength == (toTheDay.length() + 3)) {
                date += ":00:00";
            }
            if (currentLength == (toTheDay.length() + 4)) {
                date += "00:00";
            }
            if (currentLength == (toTheDay.length() + 5)) {
                date += "0:00";
            }
            if (currentLength == (toTheDay.length() + 6)) {
                date += ":00";
            }
            if (currentLength == (toTheDay.length() + 7)) {
                date += "00";
            }
            if (currentLength == (toTheDay.length() + 8)) {
                date += "0";
            }
        }
        if (date.indexOf(".") == -1) {
            date = date + (".000");
        }
        try {
            if (isFr) {
                milli = fr.soleil.lib.project.date.DateUtil.stringToMilli(date, FR_DATE_FORMAT);
            } else {
                milli = fr.soleil.lib.project.date.DateUtil.stringToMilli(date, US_DATE_FORMAT);
            }
        } catch (Exception e1) {
            try {
                e1.printStackTrace();

                Timestamp ts = Timestamp.valueOf(date);
                milli = ts.getTime();
            } catch (Exception e) {
                e.printStackTrace();
                milli = -1;
            }
        }
        return milli;
    }

}
