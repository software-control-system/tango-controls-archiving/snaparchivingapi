package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.ScNum1Val;

@Service("scNum1ValDAO")
public class ScNum1ValDAOImpl extends AbstractValDAO<ScNum1Val> {
	public ScNum1ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<ScNum1Val> getValueClass() {
		return ScNum1Val.class;
	}
}
