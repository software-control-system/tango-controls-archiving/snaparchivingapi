package fr.soleil.archiving.snap.api.tools;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.lib.project.math.MathConst;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TangoJAVAUtils {

    public static Object initPrimitiveArray(int dataType, int length) {
        switch (dataType) {
            case TangoConst.Tango_DEV_BOOLEAN:
                return new boolean[length];
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                return new byte[length];
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                return new int[length];
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                return new short[length];
            case TangoConst.Tango_DEV_FLOAT:
                return new float[length];
            case TangoConst.Tango_DEV_STRING:
                return new String[length];
            case TangoConst.Tango_DEV_DOUBLE:
            default:
                return new double[length];
        }

    }

    public static Object castResultSetAsPrimitive(int dataType, ResultSet resultSet, int index) throws SQLException {
        switch (dataType) {
            case TangoConst.Tango_DEV_STRING:
                return resultSet.getString(index);
            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                return resultSet.getByte(index);
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_STATE:
                return resultSet.getInt(index);
            case TangoConst.Tango_DEV_BOOLEAN:
                return resultSet.getInt(index) != 0;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                return resultSet.getShort(index);
            case TangoConst.Tango_DEV_FLOAT:
                return resultSet.getFloat(index);
            case TangoConst.Tango_DEV_DOUBLE:
            default:
                return resultSet.getDouble(index);
        }
    }

    public static Object cast(final int dataType, String currentValRead) {
        try {
            switch (dataType) {
                case TangoConst.Tango_DEV_BOOLEAN:
                    return (int) Double.parseDouble(currentValRead) != 0;
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    return Integer.parseInt(currentValRead);
                case TangoConst.Tango_DEV_LONG64:
                case TangoConst.Tango_DEV_ULONG64:
                    return Long.parseLong(currentValRead);
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    return Short.parseShort(currentValRead);
                case TangoConst.Tango_DEV_CHAR:
                case TangoConst.Tango_DEV_UCHAR:
                    return Byte.parseByte(currentValRead);
                case TangoConst.Tango_DEV_FLOAT:
                    return Float.parseFloat(currentValRead);
                case TangoConst.Tango_DEV_DOUBLE:
                    return Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_STRING:
                    return StringFormater.formatStringToRead(currentValRead);
                default:
                    return null;
            }
        } catch (NumberFormatException e) {
            switch (dataType) {
                case TangoConst.Tango_DEV_BOOLEAN:
                    return "true".equalsIgnoreCase(currentValRead.trim());
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    return (int) Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_LONG64:
                case TangoConst.Tango_DEV_ULONG64:
                    return (long) Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    return (short) Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_CHAR:
                case TangoConst.Tango_DEV_UCHAR:
                    return (byte) Double.parseDouble(currentValRead);
                default:
                    return null;
            }
        }
    }

    public static Object castResultSetAsObject(int dataType, ResultSet resultSet, int index) throws SQLException {
        switch (dataType) {
            case TangoConst.Tango_DEV_STRING:
                return resultSet.getString(index);
            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_CHAR:
                return resultSet.getShort(index);
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_STATE:
			case TangoConst.Tango_DEV_SHORT:
			case TangoConst.Tango_DEV_USHORT:
                return resultSet.getInt(index);
            case TangoConst.Tango_DEV_BOOLEAN:
                return resultSet.getInt(index) != 0;
            case TangoConst.Tango_DEV_FLOAT:
                return resultSet.getFloat(index);
            case TangoConst.Tango_DEV_DOUBLE:
            default:
                return resultSet.getDouble(index);
        }
    }

    public static Object defaultValue(final int dataType) {
        switch (dataType) {
            case TangoConst.Tango_DEV_BOOLEAN:
                return false;
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                return 0;
            case TangoConst.Tango_DEV_LONG64:
            case TangoConst.Tango_DEV_ULONG64:
                return 0l;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                return (short) 0;
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                return (byte) 0;
            case TangoConst.Tango_DEV_FLOAT:
                return Float.NaN;
            case TangoConst.Tango_DEV_DOUBLE:
                return MathConst.NAN_FOR_NULL;
            default:
                return null;
        }
    }

    public static boolean isNullOrNaN(final String value) {
        return ((value == null) || value.isEmpty() || "null".equals(value) || "NaN".equalsIgnoreCase(value));
    }
}
