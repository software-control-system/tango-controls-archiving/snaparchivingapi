package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.Sp1Val;

@Service("sp1ValDAO")
public class Sp1ValDAOImpl extends AbstractValDAO<Sp1Val> {
	public Sp1ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<Sp1Val> getValueClass() {
		return Sp1Val.class;
	}
}
