package fr.soleil.archiving.snap.api.persistence.spring.dto;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CompositeId
 * 
 * @author CLAISSE
 */
public class CompositeId implements Serializable {

    private static final long serialVersionUID = -7083548602060429550L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompositeId.class);

    private int idSnap;
    private int idAtt;

    public CompositeId() {

    }

    @Override
    public int hashCode() {
        // using prime numbers to reduce odds of common product for different
        // key fields (the odds don't have to be 0, but performance benefits
        // from low odds)
        return 7 * idAtt + 11 * idSnap;
    }

    @Override
    public boolean equals(Object otherObject) {
        boolean result;
        if (otherObject == null) {
            result = false;
        } else if (this == otherObject) {
            result = true;
        } else if (this.getClass() != otherObject.getClass()) {
            result = false;
        } else {
            CompositeId other = (CompositeId) otherObject;
            if (other.idAtt != this.idAtt) {
                result = false;
            } else if (other.idSnap != this.idSnap) {
                result = false;
            } else {
                result = true;
            }
        }
        return result;
    }

    /**
     * @return the idAtt
     */
    public int getIdAtt() {
        return idAtt;
    }

    /**
     * @param idAtt
     *            the idAtt to set
     */
    public void setIdAtt(int idAtt) {
        this.idAtt = idAtt;
    }

    /**
     * @return the idSnap
     */
    public int getIdSnap() {
        return idSnap;
    }

    /**
     * @param idSnap
     *            the idSnap to set
     */
    public void setIdSnap(int idSnap) {
        this.idSnap = idSnap;
    }

    public void trace() {
        LOGGER.debug(this + "/idSnap/" + idSnap + "/idAtt/" + idAtt);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[idSnap=").append(idSnap);
        sb.append(",idAtt=").append(idAtt).append("]");
        return sb.toString();
    }
}
