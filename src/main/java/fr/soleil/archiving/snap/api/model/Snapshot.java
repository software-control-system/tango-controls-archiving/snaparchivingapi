package fr.soleil.archiving.snap.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SNAPSHOT")
public class Snapshot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5816631000141431971L;

	private int idContext = -1; // Identifier for the related context
	private int idSnap = -1; // Identifier for this snapshot
	private java.sql.Timestamp time = null; // Timestamp asociated to this
	// snapshot

	@Id
	@Column(name = "ID_SNAP", unique = true, nullable = false, precision = 18, scale = 0)
	public int getIdSnap() {
		return idSnap;
	}

	public void setIdSnap(int id_snap) {
		this.idSnap = id_snap;
	}

	@Column(name = "ID_CONTEXT", unique = true, nullable = false, precision = 18, scale = 0)
	public int getIdContext() {
		return idContext;
	}

	public void setIdContext(int id_context) {
		this.idContext = id_context;
	}

	@Column(name = "TIME", nullable = false)
	public java.sql.Timestamp getTime() {
		return time;
	}

	public void setTime(java.sql.Timestamp time) {
		this.time = time;
	}
}
