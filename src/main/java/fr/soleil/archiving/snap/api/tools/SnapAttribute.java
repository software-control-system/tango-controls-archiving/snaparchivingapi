package fr.soleil.archiving.snap.api.tools;

import java.sql.Timestamp;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A snapshot attribute.
 * 
 * @author ounsy
 */
public class SnapAttribute {
    private String attributeCompleteName = ObjectUtils.EMPTY_STRING;
    protected int dataFormat;
    protected int dataType;
    protected int writable;
    // Identifier for this snapshot
    private int snapId = -1;
    // Identifier for the associated attribute
    private int attId = -1;
    // Timestamp associated to this snapshot
    private Timestamp snapDate = null;
    private Object value;
    private Object nullElements;

    public SnapAttribute() {

    }

    public String getAttributeCompleteName() {
        return attributeCompleteName;
    }

    public void setAttributeCompleteName(String attributeCompleteName) {
        this.attributeCompleteName = attributeCompleteName;
    }

    public int getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(int data_format) {
        this.dataFormat = data_format;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int data_type) {
        this.dataType = data_type;
    }

    public int getWritable() {
        return writable;
    }

    public void setWritable(int writable) {
        this.writable = writable;
    }

    public int getAttId() {
        return attId;
    }

    public void setAttId(int attId) {
        this.attId = attId;
    }

    public int getSnapId() {
        return snapId;
    }

    public void setSnapId(int snapId) {
        this.snapId = snapId;
    }

    public Timestamp getSnapDate() {
        return snapDate;
    }

    public void setSnapDate(Timestamp snap_date) {
        this.snapDate = snap_date;
    }

    public Object getNullElements() {
        return nullElements;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value, Object nullElements) {
        this.value = value;
        this.nullElements = nullElements;
    }
}
