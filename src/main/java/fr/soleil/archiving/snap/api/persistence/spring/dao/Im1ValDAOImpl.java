package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.Im1Val;

@Service("im1ValDAO")
public class Im1ValDAOImpl extends AbstractValDAO<Im1Val> {
	public Im1ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<Im1Val> getValueClass() {
		return Im1Val.class;
	}
}
