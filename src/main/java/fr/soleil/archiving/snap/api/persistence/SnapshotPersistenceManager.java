package fr.soleil.archiving.snap.api.persistence;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance.PersistenceManager;

public interface SnapshotPersistenceManager extends PersistenceManager {
	public String getResourceName();

	void autowired(Object object);
}
