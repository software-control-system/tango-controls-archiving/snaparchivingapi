package fr.soleil.archiving.snap.api;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.soleil.archiving.common.api.utils.GetConf;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Snapshot configuration
 * 
 * @author chinkumo
 */
public class GetSnapshotingConf {

    private static final String TYPE_PROPERTY = "dbType";
    private static final String FACILITY_PROPERTY = "facility";

    /**
     * return the host property define for the given class
     * 
     * @param className the name of the class
     */
    public static String getHost(String className) throws SnapshotingException {
        try {
            return GetConf.getHost(className);
        } catch (DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    /**
     * return the name property define for the given class
     * 
     * @param className the name of the class
     */
    public static String getName(String className) throws SnapshotingException {
        try {
            return GetConf.getName(className);
        } catch (DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    /**
     * return the name property define for the given class
     * 
     * @param className the name of the class
     */
    public static String getSchema(String className) throws SnapshotingException {
        try {
            return GetConf.getSchema(className);
        } catch (DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    public static String getUser(String className) throws SnapshotingException {
        try {
            return GetConf.getUser(className);
        } catch (DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    public static String getPwd(String className) throws SnapshotingException {
        try {
            return GetConf.getPwd(className);
        } catch (DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    /**
     * return the name property define for the given class
     * 
     * @param className the name of the class
     */
    public static String isRAC(String className) throws SnapshotingException {
        try {
            return GetConf.isRAC(className);
        } catch (DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    /**
     * return the name property define for the given class
     * 
     * @param className the name of the class
     */
    public static int getType(String className) throws SnapshotingException {
        String propname = TYPE_PROPERTY;
        try {
            Database dbase = ApiUtil.get_db_obj();
            int property = ConfigConst.BD_MYSQL;
            DbDatum dbdatum = dbase.get_class_property(className, propname);
            if (!dbdatum.is_empty()) {
                property = dbdatum.extractLong();
            }
            return property;
        } catch (DevFailed devFailed) {
            String message = SnapConst.SNAPSHOTING_ERROR_PREFIX;
            String reason = SnapConst.TANGO_COMM_EXCEPTION + " or " + propname + " property missing...";
            String desc = "Failed while executing GetConf.getType() method...";
            throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc, ObjectUtils.EMPTY_STRING,
                    devFailed);
        }

    }

    /**
     * return the facility property define for the given class
     * 
     * @param className the name of the class
     * @throws SnapshotingException
     */
    public static boolean getFacility(String className) throws SnapshotingException {
        String propname = FACILITY_PROPERTY;
        try {
            Database dbase = ApiUtil.get_db_obj();
            boolean property = false;
            propname = FACILITY_PROPERTY;
            DbDatum dbdatum = dbase.get_class_property(className, propname);
            if (!dbdatum.is_empty()) {
                property = dbdatum.extractBoolean();
            }
            return property;
        } catch (DevFailed devFailed) {
            String message = SnapConst.SNAPSHOTING_ERROR_PREFIX;
            String reason = SnapConst.TANGO_COMM_EXCEPTION + " or " + propname + " property missing...";
            String desc = "Failed while executing GetConf.getFacility() method...";
            throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc, ObjectUtils.EMPTY_STRING,
                    devFailed);
        }
    }
}
