package fr.soleil.archiving.snap.api.persistence.spring.dto;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance.AnyAttribute;
import fr.soleil.archiving.snap.api.persistence.context.SnapshotPersistenceContext;

/**
 * 1 string value
 * 
 * @author CLAISSE
 */
public class ScStr1Val extends Val {
    private String value;

    public ScStr1Val() {

    }

    public ScStr1Val(AnyAttribute attribute, SnapshotPersistenceContext context) {
        super(attribute, context);
        String[] val = attribute.getConvertedStringValuesTable();
        if (val == null || val.length < 1) {
            value = null;
        } else {
            value = val[0];
        }
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
