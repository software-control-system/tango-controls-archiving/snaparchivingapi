package fr.soleil.archiving.snap.api.manager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.CommandInfo;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.snap.api.DataBaseAPI;
import fr.soleil.archiving.snap.api.SnapConnectionParameters;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.SnapTool;
import fr.soleil.archiving.snap.api.tools.Snapshot;
import fr.soleil.archiving.snap.api.tools.SnapshotLight;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.database.connection.DataBaseParameters.DataBaseType;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * Java source code for the class SnapManagerApi
 * 
 * @author chinkumo
 */
public class SnapManagerApi {

    // Device class
    public static final String SNAP_ARCHIVER_DEVICE_CLASS = "SnapArchiver";
    public static final String SNAP_MANAGER_DEVICE_CLASS = "SnapManager";
    public static final String SNAP_EXTRACTOR_DEVICE_CLASS = "SnapExtractor";
    private static final Logger LOGGER = LoggerFactory.getLogger(SnapManagerApi.class);
    private static final String NAN_ARRAY = "[NaN]";
    private static final String NAN = "NaN";
    private static final String DATA_BASE_API_NOT_INIT = "DataBaseAPI not initialized";
    // Command name
    private static final String LAUNCH_SNAPHOT = "LaunchSnapShot";
    private static final String CREATE_CONTEXT = "CreateNewContext";
    private static final String TRIGGER_LAUNCH_SNAPHOT = "TriggerLaunchSnapShot";
    // Snap Data Base
    private static DataBaseAPI snapDataBase = null;

    // Devices
    private static String[] snapArchiverList;
    private static String[] snapManagerList;
    private static String[] snapExtractorList;

    // private static boolean sFacility = false;
    // private static boolean is_snap_connected = false;
    // ============================================================
    // Accessors

    /**
     * @return the Host of the snap database
     */
    public static String getSnapHost() {
        if (snapDataBase != null) {
            return snapDataBase.getHost();
        } else {
            return ObjectUtils.EMPTY_STRING;
        }
    }

    /**
     * @return the name of the snap database
     */
    public static String getSnapDatabaseName() {
        if (snapDataBase != null) {
            return snapDataBase.getDbName();
        } else {
            return ObjectUtils.EMPTY_STRING;
        }
    }

    /**
     * @return the user of the snap database
     */
    public static String getSnapUser() {
        if (snapDataBase != null) {
            return snapDataBase.getUser();
        } else {
            return ObjectUtils.EMPTY_STRING;
        }
    }

    /**
     * @return the password of the snap database
     */
    public static String getSnapPassword() {
        if (snapDataBase != null) {
            return snapDataBase.getPassword();
        } else {
            return ObjectUtils.EMPTY_STRING;
        }
    }

    /**
     * @return the snap database type (MySQL, ORACLE)...
     */
    public static DataBaseType getSnapDbType() {
        if (snapDataBase != null) {
            return snapDataBase.getDb_type();
        }
        return DataBaseType.UNKNOWN;
    }

    // ============================================================
    // Methodes

    /**
     * This method returns the name of one of the running devices, according to
     * the given class
     *
     * @param deviceClass The device's class
     * @return The device's name
     * @throws DevFailed
     */
    private static String chooseDevice(final String deviceClass) throws DevFailed {
        String device_name = ObjectUtils.EMPTY_STRING;
        String[] devicesList = null;
        if (SNAP_ARCHIVER_DEVICE_CLASS.equals(deviceClass)) {
            initDeviceList(SNAP_ARCHIVER_DEVICE_CLASS);
            devicesList = snapArchiverList;
        } else if (SNAP_MANAGER_DEVICE_CLASS.equals(deviceClass)) {
            initDeviceList(SNAP_MANAGER_DEVICE_CLASS);
            devicesList = snapManagerList;
        } else if (SNAP_EXTRACTOR_DEVICE_CLASS.equals(deviceClass)) {
            initDeviceList(SNAP_EXTRACTOR_DEVICE_CLASS);
            devicesList = snapExtractorList;
        }
        final Random hasard = new Random(System.currentTimeMillis());
        if (devicesList.length > 0) {
            final int choosed_index = hasard.nextInt(devicesList.length);
            device_name = devicesList[choosed_index];
        }
        return device_name;
    }

    /**
     * This method gets all the running SnapArchivers and stores the name in the
     * m_snapArchiverList
     *
     * @throws DevFailed
     */
    private static void initDeviceList(final String deviceClass) throws DevFailed {
        final Database dbase = ApiUtil.get_db_obj();
        final String[] runningDeviceList = dbase.get_device_exported_for_class("*" + deviceClass + "*");

        List<String> myRunningDeviceList = new ArrayList<String>(runningDeviceList.length);

        for (final String element : runningDeviceList) {
            if (deviceLivingTest(element, deviceClass)) {
                myRunningDeviceList.add(element);
            }
        }
        if (SNAP_ARCHIVER_DEVICE_CLASS.equals(deviceClass)) { // SnapArchiverList
            // building snapArchiverList
            snapArchiverList = myRunningDeviceList.toArray(new String[myRunningDeviceList.size()]);
        } else if (SNAP_MANAGER_DEVICE_CLASS.equals(deviceClass)) { // SnapManagerList
            // building snapManagerList
            snapManagerList = myRunningDeviceList.toArray(new String[myRunningDeviceList.size()]);
        } else if (SNAP_EXTRACTOR_DEVICE_CLASS.equals(deviceClass)) { // SnapBrowserList
            // building snapBrowserList
            snapExtractorList = myRunningDeviceList.toArray(new String[myRunningDeviceList.size()]);
        }

    }

    /**
     * Tests if the given device is alive
     *
     * @param deviceName
     * @return true if the device is running
     */
    private static boolean deviceLivingTest(final String deviceName, final String deviceClass) {
        try {
            final DeviceProxy deviceProxy = new DeviceProxy(deviceName);
            deviceProxy.ping();

            // verification de l existance d une commande
            final CommandInfo[] commandList = deviceProxy.command_list_query();
            if (SNAP_ARCHIVER_DEVICE_CLASS.equals(deviceClass)) {
                for (final CommandInfo element : commandList) {
                    if (TRIGGER_LAUNCH_SNAPHOT.equals(element.cmd_name)) {
                        return true;
                    }
                }
            } else if (SNAP_MANAGER_DEVICE_CLASS.equals(deviceClass)) {
                for (final CommandInfo element : commandList) {
                    if (CREATE_CONTEXT.equals(element.cmd_name)) {
                        return true;
                    }
                    if (LAUNCH_SNAPHOT.equals(element.cmd_name)) {
                        return true;
                    }
                }
            }
            return false;
        } catch (final DevFailed devFailed) {
            LOGGER.error("ERROR !! " + "\r\n" + "\t Origin : \t " + "SnapManagerApi.deviceLivingTest" + "\r\n"
                    + "\t Reason : \t " + "SNAP_FAILURE" + "\r\n" + "\t Description : \t " + devFailed.getMessage()
                    + "\r\n" + "\t Additional information : \t " + "The device " + deviceName + " does not answer..."
                    + "\r\n");
            return false;
        }
    }

    private static String[] split_att_name_3_fields(String att_name) {
        String host = ObjectUtils.EMPTY_STRING;
        String domain = ObjectUtils.EMPTY_STRING;
        String family = ObjectUtils.EMPTY_STRING;
        String member = ObjectUtils.EMPTY_STRING;
        String attribut = ObjectUtils.EMPTY_STRING;
        final String[] argout = new String[5];// = {"HOST", "DOMAIN", "FAMILY",
        // "MEMBER", "ATTRIBUT"};
        String[] decoupe; // découpage en 5 partie : host, domain, family,
        // member, attribut

        // Host name management
        if (att_name.startsWith("//")) {
            att_name = att_name.substring(2, att_name.length());
        } else {
            att_name = "HOST:port/" + att_name;
        }

        // Spliting
        decoupe = att_name.split("/"); // spliting the name in 3 fields
        host = decoupe[0];
        domain = decoupe[1];
        family = decoupe[2];
        member = decoupe[3];
        attribut = decoupe[4];

        argout[0] = host;
        argout[1] = domain;
        argout[2] = family;
        argout[3] = member;
        argout[4] = attribut;
        return argout;
    }

    public static synchronized void initSnapConnection(DataBaseParameters parameters) throws ArchivingException {
        snapDataBase = new DataBaseAPI(parameters);
    }

    public static synchronized void initFullSnapConnection(final String host, final String name, final String schema,
            final String user, final String pass, final String isRac) throws SnapshotingException {

        SnapConnectionParameters.initFromArgumentsProperties(host, name, schema, user, pass, isRac);
        SnapConnectionParameters.initFromSystemProperties();
        SnapConnectionParameters.initFromClassProperties();
        SnapConnectionParameters.initFromDefaultProperties();

        try {
            DataBaseParameters parameters = new DataBaseParameters();
            parameters.setHost(SnapConnectionParameters.getSnapHost());
            parameters.setName(SnapConnectionParameters.getSnapName());
            parameters.setSchema(SnapConnectionParameters.getSnapSchema());
            parameters.setPassword(SnapConnectionParameters.getSnapPassword());
            parameters.setUser(SnapConnectionParameters.getSnapUser());
            snapDataBase = new DataBaseAPI(parameters);
        } catch (ArchivingException e) {
            throw new SnapshotingException(e.toTangoException());
        }

        LOGGER.info(SnapConnectionParameters.appendSnapConnectionInfoLog(new StringBuilder("Connection info:\n"))
                .toString());

    }

    /**
     * This method gets informations on a given attribute and registers the
     * attribute into the database "Snap"
     *
     * @param att_complete_name the given attribute
     * @throws SnapshotingException exception throwned in case of communications problems with
     *             the device or database
     */
    private static void register(String att_complete_name) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        try {
            // /srubio@cells.es
            // This code has been modified to ensure that the device_name and
            // the attribute_name match
            // exactly those defined in the TangoDatabase (to avoid problems of
            // case in hibernate modules)

            final int index = att_complete_name.lastIndexOf("/");
            String device_name = att_complete_name.substring(0, index);
            final DeviceProxy deviceProxy = new DeviceProxy(device_name);
            final Database dbase = ApiUtil.get_db_obj();
            final String[] devslist = dbase.get_device_class_list(deviceProxy.info().server_id);
            for (int i = 0; i < devslist.length; i++) {
                if (i > 1 && 0 == i % 2 && devslist[i].equalsIgnoreCase(device_name)) {
                    device_name = devslist[i];
                }
            }
            String attribute_name = att_complete_name.substring(index + 1);
            final AttributeInfo att_info = deviceProxy.get_attribute_info(attribute_name);
            attribute_name = att_info.name;
            att_complete_name = device_name + "/" + attribute_name;
            // / srubio@cells.es: end of my code

            final String[] att_splitted_name = split_att_name_3_fields(att_complete_name);

            final Timestamp time = new Timestamp(new java.util.Date().getTime());

            final AttributeHeavy snapAttribute = new AttributeHeavy(att_complete_name);
            snapAttribute.setRegistration_time(time);
            snapAttribute.setAttributeCompleteName(att_complete_name);
            snapAttribute.setAttribute_device_name(device_name);
            snapAttribute.setDomain(att_splitted_name[1]);
            snapAttribute.setFamily(att_splitted_name[2]);
            snapAttribute.setMember(att_splitted_name[3]);
            snapAttribute.setAttribute_name(att_splitted_name[4]);
            snapAttribute.setDataType(att_info.data_type);
            snapAttribute.setDataFormat(att_info.data_format.value());
            snapAttribute.setWritable(att_info.writable.value());
            snapAttribute.setMax_dim_x(att_info.max_dim_x);
            snapAttribute.setMax_dim_y(att_info.max_dim_y);
            snapAttribute.setLevel(att_info.level.value());
            snapAttribute.setCtrl_sys(att_splitted_name[0]);
            snapAttribute.setArchivable(0);
            snapAttribute.setSubstitute(0);

            snapDataBase.registerAttribute(snapAttribute);
        } catch (final DevFailed devFailed) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.ATT_UNREACH_EXCEPTION;
            final String reason = "Failed while executing SnapManagerApi.register() method...";
            final String desc = ObjectUtils.EMPTY_STRING;
            throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING,
                    devFailed);
        } catch (final SnapshotingException e) {
            throw e;
        }
    }

    /**
     * This method insure that a given attribute was registered into Snap DB
     *
     * @param attributeName the attribute name.
     */
    public static void insureRegitration(final String attributeName) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        if (!snapDataBase.isRegistered(attributeName)) {
            register(attributeName);
        }
    }

    /**
     * TODO LG
     *
     * @param att_name
     * @return
     * @throws SnapshotingException
     */
    public static int getAttId(final String att_name) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        return snapDataBase.getAttID(att_name);
    }

    public static int createContext(final SnapContext snapContext) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        // Gets the attributes associated to the context
        final List<AttributeLight> theoricAttList = snapContext.getAttributeList();

        // Registers the attributes into the Snap database
        for (int i = 0; i < theoricAttList.size(); i++) {
            final AttributeLight snapAtt = theoricAttList.get(i);
            final String attributeName = snapAtt.getAttributeCompleteName();

            // Verify that the choosen attribute is registered into the Snap
            // database
            SnapManagerApi.insureRegitration(attributeName);

            // Gets the attribute identifier
            final int att_id = SnapManagerApi.getAttId(attributeName.trim());
            snapAtt.setAttributeId(att_id);

        }
        return snapDataBase.create_context(snapContext);
    }

    /**
     * @param snapContext
     */
    public static int createContext2Manager(final SnapContext snapContext) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        final int timeout = 3000;
        int newContextID = -1;
        try {

            final String device = chooseDevice(SNAP_MANAGER_DEVICE_CLASS);
            if (!device.isEmpty()) {
                final DeviceProxy deviceProxy = new DeviceProxy(device);
                deviceProxy.set_timeout_millis(snapContext.getAttributeList().size() * timeout);
                deviceProxy.ping();
                DeviceData device_data_in = null;
                device_data_in = new DeviceData();
                device_data_in.insert(snapContext.toArray());
                final DeviceData device_data_out = deviceProxy.command_inout("CreateNewContext", device_data_in);
                newContextID = device_data_out.extractLong();
            } else {
                final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : "
                        + SnapConst.ERROR_SNAPPATTERN_CREATION;
                final String reason = "Failed while executing SnapManagerApi.createContext2Manager() method...";
                final String desc = ObjectUtils.EMPTY_STRING;
                throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING);
            }
        } catch (final DevFailed devFailed) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.DEV_UNREACH_EXCEPTION;
            final String reason = "Failed while executing SnapManagerApi.createContext2Manager() method...";
            final String desc = ObjectUtils.EMPTY_STRING;
            throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING,
                    devFailed);
        }
        return newContextID;

    }

    /**
     * TODO LG Description : Extracts the clause SQL from the given criterions
     * and gets the contexts which subscribe to thoses conditions
     *
     * @param criterions Conditions related to the fields of the context table
     * @return a list of contexts which subscribe to the given conditions
     *         (Criterions)
     * @throws SnapshotingException
     */
    public static List<SnapContext> getContext(final Criterions criterions) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        // Gets the clause that corresponds to the given criterions
        final String clause = SnapTool.getContextClause(criterions);
        // Gets the conditions related to the context identifier
        final int context_id = SnapTool.getIdContextContextTable(criterions);
        // Gets the list of the contexts which subscribe to all thoses
        // conditions
        return snapDataBase.getContext(clause, context_id);
    }

    /**
     * This method - registers the new Sanpshot in the database SnapDB, - gets
     * the ID of the snapshot being built and - return a 'SnapShot' object with
     * the ID field filled.
     *
     * @return a 'SnapShot' object with the snapID field filled.
     * @throws SnapshotingException
     */
    public static Snapshot registerSnapShot(final int contextID) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        final Timestamp time = new Timestamp(System.currentTimeMillis());
        final Snapshot snapShot = snapDataBase.createNewSnap(contextID, time);
        return snapShot;
    }

    /**
     * This method triggers the snapshoting oh attributes that belong to the
     * context identified by the the given contextID
     *
     * @param contextID The context identifier
     * @return The 'SnapManagerResult.OK_SNAPLAUNCH' if success,
     *         'SnapManagerResult.ERROR_SNAPLAUNCH' otherwise
     */
    public static int launchSnap(final int contextID) throws DevFailed {

        final String snapArchiverName = chooseDevice(SNAP_ARCHIVER_DEVICE_CLASS);
        if (snapArchiverName.isEmpty()) {
            throw new DevFailed();
        }
        final DeviceProxy snapArchiverProxy = new DeviceProxy(snapArchiverName);

        DeviceData device_data = null;
        device_data = new DeviceData();
        device_data.insert(contextID);
        // launch snapshot
        snapArchiverProxy.command_inout("TriggerLaunchSnapShot", device_data);
        // wait for its completion
        while (snapArchiverProxy.state().equals(DevState.RUNNING)) {
            try {
                Thread.sleep(100);
            } catch (final InterruptedException e) {
            }
        }
        // get the result
        final DeviceData device_data_out = snapArchiverProxy.command_inout("GetSnapShotResult", device_data);

        final int snapID = device_data_out.extractLong();
        return snapID;
    }

    public static List<SnapAttributeExtract> getContextAssociatedAttributes(final int id_context)
            throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        return snapDataBase.getContextAssociatedAttributes(id_context);
    }

    /**
     * TODO LG This method returns a list of attributes that belong to the
     * context identified by the given id_context and subscribe to the given
     * conditions (criterions)
     *
     * @param id_context The context identifier
     * @param criterions Conditions related to fields of the context table
     * @return a list of attributes that belong to the context identified by the
     *         given id_context and subscribe to the given conditions
     *         (criterions)
     * @throws SnapshotingException
     */
    public static List<AttributeHeavy> getContextAssociatedAttributes(final int id_context, final Criterions criterions)
            throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        final List<AttributeHeavy> res = snapDataBase.getContextAttributes(id_context,
                SnapTool.getAttributeClause(criterions));
        return res;
    }

    /**
     * TODO LG
     *
     * @param snapshot
     * @return
     * @throws SnapshotingException
     */
    public static List<SnapAttributeExtract> getSnapshotAssociatedAttributes(final SnapshotLight snapshot)
            throws SnapshotingException {
        return getSnapshotAssociatedAttributes(snapshot, -1);
    }

    /**
     * @param snapshot
     * @param contextID
     * @return
     * @throws SnapshotingException
     */
    public static List<SnapAttributeExtract> getSnapshotAssociatedAttributes(final SnapshotLight snapshot,
            int contextID) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }

        if (contextID < 0) {
            contextID = getContextID(snapshot.getId_snap());
        }
        // Gets the attributes list
        final List<SnapAttributeExtract> theoricList = getContextAssociatedAttributes(contextID);
        final List<SnapAttributeExtract> result = new ArrayList<SnapAttributeExtract>(theoricList);
        snapDataBase.getSnapResults(new ArrayList<SnapAttributeExtract>(theoricList), snapshot.getId_snap());

        return result;
    }

    public static int getMaxID() throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        return snapDataBase.getMaxContextID();
    }

    /**
     * TODO LG Description : Extracts the clause SQL from the given criterions
     * and gets the snapshots which subscribe to thoses conditions
     *
     * @param criterions Conditions related to the fields of the snapshot table
     * @return a list of snapshots which subscribe to the given conditions
     *         (Criterions)
     * @throws SnapshotingException
     */
    public static List<SnapshotLight> getContextAssociatedSnapshots(final Criterions criterions)
            throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        // Gets the clause that corresponds to the given criterions
        final String clause = SnapTool.getSnapshotClause(criterions);
        // Gets the conditions related to the context identifier and the
        // snapshot identifier
        final int id_context = SnapTool.getIdContextSnapTable(criterions);
        final int id_snap = SnapTool.getIdSnap(criterions);
        // Gets the list of the snapshots which subscribe to all thoses
        // conditions
        return snapDataBase.getContextAssociatedSnapshots(clause, id_context, id_snap);
    }

    public static int getContextID(final int idSnapshot) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        return snapDataBase.getContextID(idSnapshot);
    }

    public static List<SnapAttributeExtract> getSnapResult(final int id_snap) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        final int contextID = snapDataBase.getContextID(id_snap);
        final List<SnapAttributeExtract> theoricList = getContextAssociatedAttributes(contextID);
        final List<SnapAttributeExtract> result = new ArrayList<SnapAttributeExtract>(theoricList);
        snapDataBase.getSnapResults(result, id_snap);

        return result;
    }

    public static SnapAttributeExtract[] getSnapValues(final int idSnap, final String... attributeNames)
            throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        final SnapAttributeExtract[] conf = snapDataBase.getAttributeConfig(attributeNames);
        snapDataBase.getSnapResults(Arrays.asList(conf), idSnap);
        return conf;
    }

    /**
     * TODO LG
     *
     * @param id_snap
     * @param new_comment
     * @throws SnapshotingException
     */
    public static void updateSnapComment(final int id_snap, final String new_comment) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        snapDataBase.updateSnapComment(id_snap, new_comment);
    }

    /**
     * Get the comment of a snapshot
     *
     * @param snapID the snapshot ID
     * @return the comment
     * @throws SnapshotingException
     */
    public static String getSnapComment(final int snapID) throws SnapshotingException {
        if (snapDataBase == null) {
            throw new SnapshotingException(DATA_BASE_API_NOT_INIT);
        }
        return snapDataBase.getSnapComment(snapID);
    }

    /**
     * This method is used by a client (GUI) to trigger an equipment setting.
     *
     * @param snapShot
     * @throws SnapshotingException
     */
    public static void setEquipmentsWithSnapshot(final Snapshot snapShot) throws SnapshotingException {
        try {
            final int timeout = 3000;
            final String device = chooseDevice(SNAP_MANAGER_DEVICE_CLASS);
            if (!device.isEmpty()) {
                final DeviceProxy deviceProxy = new DeviceProxy(device);
                deviceProxy.set_timeout_millis(snapShot.getAttribute_List().size() * timeout);
                deviceProxy.ping();
                DeviceData device_data = null;
                device_data = new DeviceData();
                device_data.insert(snapShot.toArray());
                deviceProxy.command_inout("SetEquipmentsWithSnapshot", device_data);
            } else {
                final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.ERROR_SNAP_SET_EQUIPMENT;
                final String reason = "Failed while executing SnapManagerApi.setEquipmentsWithSnapshot() method...";
                final String desc = "No SnapManager available";
                throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING);
            }
        } catch (final DevFailed devFailed) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.DEV_UNREACH_EXCEPTION;
            final String reason = "Failed while executing SnapManagerApi.setEquipmentsWithSnapshot() method...";
            final String desc = ObjectUtils.EMPTY_STRING;
            throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING,
                    devFailed);
        }
    }

    /**
     * This method is used by a snapshoting device to actually set equipements
     * to the their values on the given snapshot.
     *
     * @param snapShot
     * @throws SnapshotingException
     */
    public static void TriggerSetEquipments(final Snapshot snapShot) throws SnapshotingException {
        final List<SnapAttributeExtract> attribute_List = snapShot.getAttribute_List();
        SnapshotingException snapFinalEx = null;

        for (int i = 0; i < attribute_List.size(); i++) {
            final SnapAttributeExtract snapAttributeExtract = attribute_List.get(i);
            if (snapAttributeExtract.getWritable() != AttrWriteType._READ
                    && snapAttributeExtract.getWritable() != AttrWriteType._READ_WITH_WRITE) {
                try {
                    final AttributeProxy attributeProxy = new AttributeProxy(
                            snapAttributeExtract.getAttributeCompleteName());
                    final DeviceAttribute deviceAttribute = new DeviceAttribute(attributeProxy.name());
                    final Object value = snapAttributeExtract.getWriteValue();
                    switch (snapAttributeExtract.getDataFormat()) {
                        case AttrDataFormat._SCALAR:
                            if (value == null || NAN.equals(value)) {
                                break;
                            }
                            switch (snapAttributeExtract.getDataType()) {
                                case TangoConst.Tango_DEV_STRING:
                                    deviceAttribute.insert(((String) value).toString());
                                    break;
                                case TangoConst.Tango_DEV_STATE:
                                    deviceAttribute.insert(((Number) value).intValue());
                                    break;
                                case TangoConst.Tango_DEV_UCHAR:
                                    deviceAttribute.insert_uc(((Number) value).byteValue());
                                    break;
                                case TangoConst.Tango_DEV_LONG:
                                    deviceAttribute.insert(((Number) value).intValue());
                                    break;
                                case TangoConst.Tango_DEV_ULONG:
                                    deviceAttribute.insert_us(((Number) value).intValue());
                                    break;
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    deviceAttribute.insert(((Boolean) value).booleanValue());
                                    break;
                                case TangoConst.Tango_DEV_USHORT:
                                    deviceAttribute.insert_us(((Number) value).shortValue());
                                    break;
                                case TangoConst.Tango_DEV_SHORT:
                                    deviceAttribute.insert(((Number) value).shortValue());
                                    break;
                                case TangoConst.Tango_DEV_FLOAT:
                                    deviceAttribute.insert(((Number) value).floatValue());
                                    break;
                                case TangoConst.Tango_DEV_DOUBLE:
                                default:
                                    deviceAttribute.insert(((Number) value).doubleValue());
                                    break;
                            }
                            break;
                        case AttrDataFormat._SPECTRUM:
                            if ((value == null) || NAN_ARRAY.equals(value) || NAN.equals(value)) {
                                break;
                            }
                            switch (snapAttributeExtract.getDataType()) {
                                case TangoConst.Tango_DEV_UCHAR:
                                    final byte[] byteVal = NumberArrayUtils.extractByteArray(value);
                                    deviceAttribute.insert_uc(byteVal, byteVal.length, 0);
                                    break;
                                case TangoConst.Tango_DEV_LONG:
                                    final int[] longVal = NumberArrayUtils.extractIntArray(value);
                                    deviceAttribute.insert(longVal);
                                    break;
                                case TangoConst.Tango_DEV_ULONG:
                                    final int[] longValu = NumberArrayUtils.extractIntArray(value);
                                    deviceAttribute.insert_us(longValu);
                                    break;
                                case TangoConst.Tango_DEV_SHORT:
                                    final short[] shortVal = NumberArrayUtils.extractShortArray(value);
                                    deviceAttribute.insert(shortVal);
                                    break;
                                case TangoConst.Tango_DEV_USHORT:
                                    final short[] shortValu = NumberArrayUtils.extractShortArray(value);
                                    deviceAttribute.insert_us(shortValu);
                                    break;
                                case TangoConst.Tango_DEV_FLOAT:
                                    final float[] floatVal = NumberArrayUtils.extractFloatArray(value);
                                    deviceAttribute.insert(floatVal);
                                    break;
                                case TangoConst.Tango_DEV_DOUBLE:
                                    final double[] doubleVal = NumberArrayUtils.extractDoubleArray(value);
                                    deviceAttribute.insert(doubleVal);
                                    break;
                                case TangoConst.Tango_DEV_STRING:
                                    final String[] stringVal = (String[]) value;
                                    deviceAttribute.insert(stringVal);
                                    break;
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    final boolean[] boolVal = (boolean[]) value;
                                    deviceAttribute.insert(boolVal);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            // nothing to do
                    }
                    attributeProxy.write(deviceAttribute);
                } catch (final DevFailed devFailed) {
                    final String nameOfFailure = snapAttributeExtract.getAttributeCompleteName();

                    final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : "
                            + SnapConst.ERROR_SNAP_SET_EQUIPMENT + " on attribute: " + nameOfFailure;
                    final String reason = "Failed while executing SnapManagerApi.TriggerSetEquipments() method on attribute:"
                            + nameOfFailure;
                    final String desc = reason;
                    if (snapFinalEx == null) {
                        snapFinalEx = new SnapshotingException(message, reason, ErrSeverity.PANIC, desc,
                                ObjectUtils.EMPTY_STRING, devFailed);
                    } else {
                        final SnapshotingException snapEx = new SnapshotingException(devFailed);
                        snapFinalEx.addStack(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING,
                                snapEx);
                    }
                }
            }
        }
        if (snapFinalEx != null) {
            throw snapFinalEx;
        }
    }

    /**
     * @param cmd_name
     * @param option
     * @param id_snap
     * @throws SnapshotingException
     */
    public static String setEquipmentWithCommand(final String cmd_name, final String option, final Snapshot snapShot)
            throws SnapshotingException {
        try {
            final int timeout = 3000;
            final String device = chooseDevice(SNAP_MANAGER_DEVICE_CLASS);
            if (!device.equals(ObjectUtils.EMPTY_STRING)) {
                final DeviceProxy deviceProxy = new DeviceProxy(device);
                deviceProxy.set_timeout_millis(snapShot.getAttribute_List().size() * timeout);
                deviceProxy.ping();
                DeviceData device_data = null;
                device_data = new DeviceData();

                String[] first_argin = new String[] { cmd_name, option, String.valueOf(snapShot.getId_snap()) };
                String[] argin = first_argin;
                if (snapShot.isFiltered()) {
                    List<SnapAttributeExtract> attributes = snapShot.getAttribute_List();
                    if (attributes != null) {
                        int argin_size = attributes.size() + 3;
                        argin = new String[argin_size];
                        for (int i = 0; i < first_argin.length; ++i) {
                            argin[i] = first_argin[i];
                        }
                        for (int i = 0; i < attributes.size(); ++i) {
                            SnapAttributeExtract attr = attributes.get(i);
                            argin[i + 3] = (attr != null) ? attr.getAttributeCompleteName() : ObjectUtils.EMPTY_STRING;
                        }
                    }
                    attributes = null;
                }
                device_data.insert(argin);

                device_data = deviceProxy.command_inout("SetEquipmentsWithCommand", device_data);
                return device_data.extractString();
            } else {
                final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.ERROR_SNAP_SET_EQUIPMENT;
                final String reason = "Failed while executing SnapManagerApi.setEquipmentWithCommand() method...";
                final String desc = ObjectUtils.EMPTY_STRING;
                throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING);
            }
        } catch (final DevFailed devFailed) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.DEV_UNREACH_EXCEPTION;
            final String reason = "Failed while executing SnapManagerApi.setEquipmentWithCommand() method...";
            final String desc = devFailed.getMessage();
            throw new SnapshotingException(message, reason, ErrSeverity.PANIC, desc, ObjectUtils.EMPTY_STRING,
                    devFailed);
        }

    }
}
