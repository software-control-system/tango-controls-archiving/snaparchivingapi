/*  Synchrotron Soleil 
 *  
 *   File          :  SnapshotPersistenceManagerFactory.java
 *  
 *   Project       :  javaapi
 *  
 *   Description   :  
 *  
 *   Author        :  CLAISSE
 *  
 *   Original      :  9 mars 07 
 *  
 *   Revision:                      Author:  
 *   Date:                          State:  
 *  
 *   Log: SnapshotPersistenceManagerFactory.java,v 
 *
 */
/*
 * Created on 9 mars 07
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.snap.api.persistence;

public class SnapshotPersistenceManagerFactory {
    private static SnapshotPersistenceManagerFactory instance = new SnapshotPersistenceManagerFactory();

    private SnapshotPersistenceManagerFactory() {
    }

    public static SnapshotPersistenceManagerFactory getInstance() {
	return instance;
    }

    public SnapshotPersistenceManager getManager(String beansFileName, String userName, String password) {
		return new SpringSnapshotPersistenceManagerImpl(beansFileName, userName, password);
    }

}
