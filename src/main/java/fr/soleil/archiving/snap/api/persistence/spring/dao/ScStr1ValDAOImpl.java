package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr1Val;

@Service("scStr1ValDAO")
public class ScStr1ValDAOImpl extends AbstractValDAO<ScStr1Val> {
	public ScStr1ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<ScStr1Val> getValueClass() {
		return ScStr1Val.class;
	}
}
