package fr.soleil.archiving.snap.api.persistence.spring.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance.AnyAttribute;
import fr.soleil.archiving.snap.api.persistence.context.SnapshotPersistenceContext;

/**
 * 2 numerical values
 * 
 * @author CLAISSE
 */
public class ScNum2Val extends Val {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScNum2Val.class);

    private double readValue;
    private double writeValue;

    public ScNum2Val() {

    }

    public ScNum2Val(AnyAttribute attribute, SnapshotPersistenceContext context) {
        super(attribute, context);

        double[] val = attribute.getConvertedNumericValuesTable();
        if (val == null) {
            readValue = Double.NaN;
            writeValue = Double.NaN;
        } else {
            readValue = val.length < 1 ? Double.NaN : val[0];
            writeValue = val.length < 2 ? Double.NaN : val[1];
        }
    }

    /**
     * @return the readValue
     */
    public double getReadValue() {
        return readValue;
    }

    /**
     * @param readValue
     *            the readValue to set
     */
    public void setReadValue(double readValue) {
        this.readValue = readValue;
    }

    /**
     * @return the writeValue
     */
    public double getWriteValue() {
        return writeValue;
    }

    /**
     * @param writeValue
     *            the writeValue to set
     */
    public void setWriteValue(double writeValue) {
        this.writeValue = writeValue;
    }

    public void trace() {
        super.getCompositeId().trace();
        LOGGER.debug("ScNum2Val/readValue/" + readValue);
        LOGGER.debug("ScNum2Val/writeValue/" + writeValue);
    }
}
