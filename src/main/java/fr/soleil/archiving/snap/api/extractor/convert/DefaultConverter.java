/*
 * Synchrotron Soleil
 * 
 * File : DefaultConverter.java
 * 
 * Project : snapExtractorAPI
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 24 janv. 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: DefaultConverter.java,v
 */
/*
 * Created on 24 janv. 2006
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.snap.api.extractor.convert;

import java.lang.reflect.Array;
import java.util.Arrays;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * 
 * @author CLAISSE
 */
public class DefaultConverter implements IConverter {

    public DefaultConverter() {
        super();
    }

    protected void fillDbData(DbData dbData, NullableTimedData[] timedAttrDatas) {
        if ((dbData != null) && (timedAttrDatas != null)) {
            dbData.setTimedData(timedAttrDatas);
            NullableTimedData theTimedAttrData = timedAttrDatas[0];
            dbData.setMaxX(theTimedAttrData.getX());
            dbData.setMaxY(theTimedAttrData.getY());
        }
    }

    // TODO maybe return DbData[] (TANGOARCH-715)
    @Override
    public synchronized DbData[] convert(SnapAttributeExtract currentExtract) throws DevFailed {
        DbData[] dbData = DbData.initExtractionResult(currentExtract.getAttributeCompleteName(),
                currentExtract.getDataType(), currentExtract.getDataFormat(), currentExtract.getWritable());

        NullableTimedData[][] timedAttrDatas = null;
        try {
            timedAttrDatas = convertData(currentExtract);

            if (timedAttrDatas == null) {
                dbData = null;
            }
        } catch (IllegalArgumentException iae) {
            DevFailed devFailed = new DevFailed();
            devFailed.initCause(iae);
            throw devFailed;
        }
        if (dbData != null) {
            fillDbData(dbData[READ_INDEX], timedAttrDatas[READ_INDEX]);
            fillDbData(dbData[WRITE_INDEX], timedAttrDatas[WRITE_INDEX]);
            Arrays.fill(timedAttrDatas, null);
        }
        return dbData;
    }

    private NullableTimedData[][] convertData(SnapAttributeExtract currentExtract) throws IllegalArgumentException {
        Object value = currentExtract.getValue();
        int dataType = currentExtract.getDataType();
        int dataFormat = currentExtract.getDataFormat();
        boolean isScalar = (dataFormat == AttrDataFormat._SCALAR);
        boolean hasBothReadAndWriteValues = ((currentExtract.getWritable() == AttrWriteType._READ_WITH_WRITE)
                || (currentExtract.getWritable() == AttrWriteType._READ_WRITE));

        long when;
        if (currentExtract.getSnapDate() == null) {
            when = System.currentTimeMillis();
        } else {
            when = currentExtract.getSnapDate().getTime();
        }

        NullableTimedData[][] ret = new NullableTimedData[2][1];
        NullableTimedData[] theTimedAttrData;
        try {
            if (isScalar) {
                if (hasBothReadAndWriteValues) {
                    theTimedAttrData = buildDataScalarRW(value, dataType, when);
                } else {
                    theTimedAttrData = buildDataScalarR(value, dataType,
                            currentExtract.getWritable() == AttrWriteType._WRITE, when);
                }
            } else {
                if (hasBothReadAndWriteValues) {
                    theTimedAttrData = buildDataSpectrumRW(value, dataType, when);
                    for (NullableTimedData data : theTimedAttrData) {
                        if (data != null) {
                            data.setX(currentExtract.getDimX());
                        }
                    }
                } else {
                    theTimedAttrData = buildDataSpectrumR(value, dataType,
                            currentExtract.getWritable() == AttrWriteType._WRITE, when);
                }
            }
        } catch (IllegalArgumentException iae) {
            throw iae;
        }

        if (theTimedAttrData == null) {
            ret = null;
        } else {
            ret[READ_INDEX][0] = theTimedAttrData[READ_INDEX];
            ret[WRITE_INDEX][0] = theTimedAttrData[WRITE_INDEX];
            Arrays.fill(theTimedAttrData, null);
        }

        return ret;
    }

    private boolean isNullOrNaN(String value) {
        return (value == null || "null".equals(value) || "NaN".equalsIgnoreCase(value));
    }

    private boolean isNullOrNaNArray(Object value) {
        boolean result;
        if (value == null) {
            result = true;
        } else {
            String strValue = String.valueOf(value);
            result = ("NaN".equalsIgnoreCase(strValue) || "[NaN]".equalsIgnoreCase(strValue));
        }
        return result;
    }

    protected NullableTimedData[] treatScalarBooleanCase(String[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        boolean[] data = new boolean[1];
        boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(valueTab[index])) {
            data[0] = false;
            nullElements[0] = true;
        } else {
            data[0] = Boolean.parseBoolean(valueTab[index]);
        }
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, nullElements);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatScalarShortCase(String[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        short[] data = new short[1];
        boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(valueTab[index])) {
            data[0] = 0;
            nullElements[0] = true;
        } else {
            double theDataL_W_D = Double.parseDouble(valueTab[index]);
            data[0] = (short) Math.round(theDataL_W_D);
        }
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, nullElements);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatScalarIntCase(String[] valueTab, NullableTimedData[] timedAttrDatas, int index) {
        NullableTimedData[] result = timedAttrDatas;
        int[] data = new int[1];
        boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(valueTab[index])) {
            data[0] = 0;
            nullElements[0] = true;
        } else {
            double theDataL_W_D = Double.parseDouble(valueTab[index]);
            data[0] = (int) Math.round(theDataL_W_D);
        }
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, nullElements);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatScalarFloatCase(String[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        float[] data = new float[1];
        boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(valueTab[index])) {
            data[0] = Float.NaN;
            nullElements[0] = true;
        } else {
            data[0] = Float.parseFloat(valueTab[index]);
        }
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, nullElements);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatScalarDoubleCase(String[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        double[] data = new double[1];
        boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(valueTab[index])) {
            data[0] = MathConst.NAN_FOR_NULL;
            nullElements[0] = true;
        } else {
            data[0] = Double.parseDouble(valueTab[index]);
        }
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, nullElements);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatScalarStringCase(String[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        String[] data = new String[1];
        boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(valueTab[index])) {
            data[0] = null;
            nullElements[0] = true;
        } else {
            data[0] = valueTab[index];
        }
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, nullElements);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatSpectrumBooleanCase(Object[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        // TODO maybe clone
        boolean[] data = (boolean[]) valueTab[index];
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, null);
        timedAttrData.setX(data == null ? 0 : data.length);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatSpectrumShortCase(Object[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        // TODO maybe use NumberArrayUtils.extractShortArray(valueTab[index]), true);
        short[] data = NumberArrayUtils.extractShortArray(valueTab[index]);

        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, null);
        timedAttrData.setX(data == null ? 0 : data.length);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatSpectrumIntCase(Object[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        // TODO maybe use NumberArrayUtils.extractIntArray(valueTab[index]), true);
        int[] data = NumberArrayUtils.extractIntArray(valueTab[index]);

        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, null);
        timedAttrData.setX(data == null ? 0 : data.length);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatSpectrumFloatCase(Object[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        // TODO maybe use NumberArrayUtils.extractFloatArray(valueTab[index]), true);
        float[] data = NumberArrayUtils.extractFloatArray(valueTab[index]);

        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, null);
        timedAttrData.setX(data == null ? 0 : data.length);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatSpectrumDoubleCase(Object[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        // TODO maybe use NumberArrayUtils.extractDoubleArray(valueTab[index]), true);
        double[] data = NumberArrayUtils.extractDoubleArray(valueTab[index]);

        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, null);
        timedAttrData.setX(data == null ? 0 : data.length);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    protected NullableTimedData[] treatSpectrumStringCase(Object[] valueTab, NullableTimedData[] timedAttrDatas,
            int index) {
        NullableTimedData[] result = timedAttrDatas;
        // TODO maybe clone
        String[] data = (String[]) valueTab[index];
        NullableTimedData timedAttrData = new NullableTimedData();
        timedAttrData.setValue(data, null);
        timedAttrData.setX(data == null ? 0 : data.length);
        if (result == null) {
            result = new NullableTimedData[2];
        }
        result[index] = timedAttrData;
        return result;
    }

    /**
     * @param value
     * @param dataType
     * @param when
     * @return
     */
    private NullableTimedData[] buildDataScalarRW(Object value, int dataType, long when)
            throws IllegalArgumentException {
        NullableTimedData[] result = null;
        // String[] valueTab = this.castObjectTableToString ( (Object []) value
        // , true );
        String[] valueTab = this.castObjectTableToString(value, false);
        if (valueTab == null) {
            result = null;
        } else {
            switch (dataType) {
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    result = treatScalarIntCase(valueTab, result, READ_INDEX);
                    result = treatScalarIntCase(valueTab, result, WRITE_INDEX);
                    break;

                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    result = treatScalarShortCase(valueTab, result, READ_INDEX);
                    result = treatScalarShortCase(valueTab, result, WRITE_INDEX);
                    break;

                case TangoConst.Tango_DEV_DOUBLE:
                    result = treatScalarDoubleCase(valueTab, result, READ_INDEX);
                    result = treatScalarDoubleCase(valueTab, result, WRITE_INDEX);
                    break;

                case TangoConst.Tango_DEV_BOOLEAN:
                    result = treatScalarBooleanCase(valueTab, result, READ_INDEX);
                    result = treatScalarBooleanCase(valueTab, result, WRITE_INDEX);
                    break;

                case TangoConst.Tango_DEV_FLOAT:
                    result = treatScalarFloatCase(valueTab, result, READ_INDEX);
                    result = treatScalarFloatCase(valueTab, result, WRITE_INDEX);
                    break;

                case TangoConst.Tango_DEV_STRING:
                    result = treatScalarStringCase(valueTab, result, READ_INDEX);
                    result = treatScalarStringCase(valueTab, result, WRITE_INDEX);
                    break;

                case TangoConst.Tango_DEV_STATE:
                    result = treatScalarIntCase(valueTab, result, READ_INDEX);
                    result = treatScalarIntCase(valueTab, result, WRITE_INDEX);
                    break;

                default:
                    throw new IllegalArgumentException("DefaultConverter/Data type not supported: " + dataType);
            }
            if (result != null) {
                for (NullableTimedData theTimedAttrData : result) {
                    if (theTimedAttrData != null) {
                        theTimedAttrData.setDataType(dataType);
                        theTimedAttrData.setTime(when);
                        theTimedAttrData.setX(1);
                        theTimedAttrData.setY(0);
                    }
                }
            }
        }
        return result;
    }

    private NullableTimedData[] buildDataSpectrumRW(Object value, int dataType, long when)
            throws IllegalArgumentException {
        NullableTimedData[] result = null;
        if (!isNullOrNaNArray(value)) {
            Object[] valueTab = (Object[]) value;
            if ((valueTab.length == 2) && (!isNullOrNaNArray(valueTab[0])) && (!isNullOrNaNArray(valueTab[1]))) {

                switch (dataType) {
                    case TangoConst.Tango_DEV_LONG:
                    case TangoConst.Tango_DEV_ULONG:
                    case TangoConst.Tango_DEV_STATE:
                        result = treatSpectrumIntCase(valueTab, result, READ_INDEX);
                        result = treatSpectrumIntCase(valueTab, result, WRITE_INDEX);
                        break;

                    case TangoConst.Tango_DEV_SHORT:
                    case TangoConst.Tango_DEV_USHORT:
                        result = treatSpectrumShortCase(valueTab, result, READ_INDEX);
                        result = treatSpectrumShortCase(valueTab, result, WRITE_INDEX);
                        break;

                    case TangoConst.Tango_DEV_DOUBLE:
                        result = treatSpectrumDoubleCase(valueTab, result, READ_INDEX);
                        result = treatSpectrumDoubleCase(valueTab, result, WRITE_INDEX);
                        break;

                    case TangoConst.Tango_DEV_BOOLEAN:
                        result = treatSpectrumBooleanCase(valueTab, result, READ_INDEX);
                        result = treatSpectrumBooleanCase(valueTab, result, WRITE_INDEX);
                        break;

                    case TangoConst.Tango_DEV_FLOAT:
                        result = treatSpectrumFloatCase(valueTab, result, READ_INDEX);
                        result = treatSpectrumFloatCase(valueTab, result, WRITE_INDEX);
                        break;

                    case TangoConst.Tango_DEV_STRING:
                        result = treatSpectrumStringCase(valueTab, result, READ_INDEX);
                        result = treatSpectrumStringCase(valueTab, result, WRITE_INDEX);
                        break;

                    default:
                        throw new IllegalArgumentException("DefaultConverter/Data type not supported: " + dataType);
                }
                if (result != null) {
                    for (NullableTimedData theTimedAttrData : result) {
                        if (theTimedAttrData != null) {
                            theTimedAttrData.setDataType(dataType);
                            theTimedAttrData.setTime(when);
                        }
                    }
                }
            }
        }
        return result;
    }

    private NullableTimedData[] buildDataScalarR(Object value, int dataType, boolean write, long when)
            throws IllegalArgumentException {
        NullableTimedData[] result = null;
        NullableTimedData theTimedAttrData = null;

        final boolean[] nullElements = new boolean[1];
        String valS = String.valueOf(value);
        switch (dataType) {
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                int[] dataL = new int[1];
                if (isNullOrNaN(valS)) {
                    dataL[0] = 0;
                    nullElements[0] = true;
                } else {
                    double valL_D = Double.parseDouble(valS);
                    dataL[0] = (int) Math.round(valL_D);
                }
                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataL, nullElements);
                break;

            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                short[] dataS = new short[1];
                if (isNullOrNaN(valS)) {
                    dataS[0] = 0;
                    nullElements[0] = true;
                } else {
                    double valS_D = Double.parseDouble(valS);
                    dataS[0] = (short) Math.round(valS_D);
                }

                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataS, nullElements);
                break;

            case TangoConst.Tango_DEV_DOUBLE:
                Double valD = (Double) (value);

                double[] dataD = new double[1];
                if (valD == null) {
                    dataD[0] = MathConst.NAN_FOR_NULL;
                    nullElements[0] = true;
                } else {
                    dataD[0] = valD.doubleValue();
                }
                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataD, nullElements);
                break;

            case TangoConst.Tango_DEV_BOOLEAN:
                Boolean valB = (Boolean) (value);

                boolean[] dataB = new boolean[1];
                if (valB == null) {
                    dataB[0] = false;
                    nullElements[0] = true;
                } else {
                    dataB[0] = valB.booleanValue();
                }
                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataB, nullElements);
                break;

            case TangoConst.Tango_DEV_FLOAT:
                Float valF = (Float) (value);

                float[] dataF = new float[1];
                if (valF == null) {
                    dataF[0] = Float.NaN;
                    nullElements[0] = true;
                } else {
                    dataF[0] = valF.floatValue();
                }
                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataF, nullElements);
                break;

            case TangoConst.Tango_DEV_STRING:
                String valStr = (String) (value);

                String[] dataStr = new String[1];
                if (valStr == null) {
                    nullElements[0] = true;
                }
                dataStr[0] = valStr;
                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataStr, nullElements);
                break;

            case TangoConst.Tango_DEV_STATE:
                Integer valI = (Integer) (value);

                int[] dataI = new int[1];
                if (valI == null) {
                    dataI[0] = 0;
                    nullElements[0] = true;
                } else {
                    dataI[0] = valI.intValue();
                }
                theTimedAttrData = new NullableTimedData();
                theTimedAttrData.setValue(dataI, nullElements);
                break;

            default:
                throw new IllegalArgumentException("DefaultConverter/Data type not supported: " + dataType);
        }
        if (theTimedAttrData != null) {
            theTimedAttrData.setDataType(dataType);
            theTimedAttrData.setTime(when);
            theTimedAttrData.setX(1);
            theTimedAttrData.setY(0);
            result = new NullableTimedData[2];
            result[write ? WRITE_INDEX : READ_INDEX] = theTimedAttrData;
        }
        return result;
    }

    private NullableTimedData[] buildDataSpectrumR(Object value, int dataType, boolean write, long when)
            throws IllegalArgumentException {
        NullableTimedData[] result = null;
        if (!isNullOrNaNArray(value)) {
            int x;
            NullableTimedData theTimedAttrData = null;

            switch (dataType) {
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    int[] dataL = (int[]) value;
                    x = dataL.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataL, null);
                    break;

                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    short[] dataS = (short[]) value;
                    x = dataS.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataS, null);
                    break;

                case TangoConst.Tango_DEV_DOUBLE:
                    double[] dataD = (double[]) value;
                    x = dataD.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataD, null);
                    break;

                case TangoConst.Tango_DEV_BOOLEAN:
                    boolean[] dataB = (boolean[]) value;
                    x = dataB.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataB, null);
                    break;

                case TangoConst.Tango_DEV_FLOAT:
                    float[] dataF = (float[]) value;
                    x = dataF.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataF, null);
                    break;

                case TangoConst.Tango_DEV_STRING:
                    String[] dataStr = (String[]) value;
                    x = dataStr.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataStr, null);
                    break;

                case TangoConst.Tango_DEV_STATE:
                    int[] dataI = (int[]) value;
                    x = dataI.length;

                    theTimedAttrData = new NullableTimedData();
                    theTimedAttrData.setValue(dataI, null);
                    break;

                default:
                    throw new IllegalArgumentException("DefaultConverter/Data type not supported: " + dataType);
            }
            if (theTimedAttrData != null) {
                theTimedAttrData.setDataType(dataType);
                theTimedAttrData.setTime(when);
                theTimedAttrData.setX(x);
                result = new NullableTimedData[2];
                result[write ? WRITE_INDEX : READ_INDEX] = theTimedAttrData;
            }
        }
        return result;
    }

    @Override
    public String[] castObjectTableToString(Object objects, boolean returnNullIfOneElementIsNull) {
        String[] ret = null;
        if (objects != null && objects.getClass().isArray()) {
            int length = Array.getLength(objects);
            if (length > 0) {
                ret = new String[length];
                for (int i = 0; i < length; i++) {
                    Object tmp = Array.get(objects, i);

                    if (tmp == null && returnNullIfOneElementIsNull) {
                        ret = null;
                        break;
                    } else {
                        ret[i] = String.valueOf(tmp);
                    }
                }
            }
        }
        return ret;
    }
}
