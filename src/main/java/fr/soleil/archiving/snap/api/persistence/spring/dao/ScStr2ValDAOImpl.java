package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr2Val;

@Service("scStr2ValDAO")
public class ScStr2ValDAOImpl extends AbstractValDAO<ScStr2Val> {
	public ScStr2ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<ScStr2Val> getValueClass() {
		return ScStr2Val.class;
	}
}
