package fr.soleil.archiving.snap.api.persistence.spring.dao;

import java.io.Serializable;
import java.util.List;

public interface AbstractHibernateDAO<T extends Serializable> {
	public T findOne(int id);

	public List<T> findAll();

	public void save(T entity);

	public T update(T entity);

	public void delete(T entity);

	public void deleteById(int id);
}