/*	Synchrotron Soleil 
 *  
 *   File          :  Sp1Val.java
 *  
 *   Project       :  javaapi
 *  
 *   Description   :  
 *  
 *   Author        :  CLAISSE
 *  
 *   Original      :  7 mars 07 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: Sp1Val.java,v 
 *
 */
/*
 * Created on 7 mars 07
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.snap.api.persistence.spring.dto;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance.AnyAttribute;
import fr.soleil.archiving.snap.api.persistence.context.SnapshotPersistenceContext;

public class Sp1Val extends SpVal {
	private String value;

	public Sp1Val() {

	}

	public Sp1Val(AnyAttribute attribute, SnapshotPersistenceContext context) {
		super(attribute, context);
		this.value = attribute.getConvertedStringAggreggatedValues(false);
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
