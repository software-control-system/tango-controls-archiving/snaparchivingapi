package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.Im2Val;

@Service("im2ValDAO")
public class Im2ValDAOImpl extends AbstractValDAO<Im2Val> {
	public Im2ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<Im2Val> getValueClass() {
		return Im2Val.class;
	}
}
