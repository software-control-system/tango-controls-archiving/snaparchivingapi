package fr.soleil.archiving.snap.api.persistence.spring.dto;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance.AnyAttribute;
import fr.soleil.archiving.snap.api.persistence.context.SnapshotPersistenceContext;

/**
 * 1 numerical value
 * 
 * @author CLAISSE
 */
public class ScNum1Val extends Val {
    private double value;

    public ScNum1Val() {

    }

    public ScNum1Val(AnyAttribute attribute, SnapshotPersistenceContext context) {
        super(attribute, context);
        double[] val = attribute.getConvertedNumericValuesTable();
        if (val == null || val.length < 1) {
            value = Double.NaN;
        } else {
            value = val[0];
        }
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }
}
