package fr.soleil.archiving.snap.api;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDevice;
import fr.soleil.archiving.common.api.utils.DbConnectionInfo;
import fr.soleil.archiving.snap.api.manager.SnapManagerApi;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;

public class SnapConnectionParameters {

    private static final DbConnectionInfo SNAP_INFO_CONNECTOR = new DbConnectionInfo(
            SnapManagerApi.SNAP_MANAGER_DEVICE_CLASS);

    public static String getSnapUser() {
        return SNAP_INFO_CONNECTOR.getDbUser();
    }

    public static void setSnapUser(String hDBUser, String origin) {
        SNAP_INFO_CONNECTOR.setDbUser(hDBUser, origin);
    }

    public static String getSnapPassword() {
        return SNAP_INFO_CONNECTOR.getDbPassword();
    }

    public static void setSnapPassword(String hDBPassword, String origin) {
        SNAP_INFO_CONNECTOR.setDbPassword(hDBPassword, origin);
    }

    public static String getSnapHost() {
        return SNAP_INFO_CONNECTOR.getDbHost();
    }

    public static void setSnapHost(String hDbHost, String origin) {
        SNAP_INFO_CONNECTOR.setDbHost(hDbHost, origin);
    }

    public static String getSnapName() {
        return SNAP_INFO_CONNECTOR.getDbName();
    }

    public static void setSnapName(String hDbName, String origin) {
        SNAP_INFO_CONNECTOR.setDbName(hDbName, origin);
    }

    public static String getSnapSchema() {
        return SNAP_INFO_CONNECTOR.getDbSchema();
    }

    public static void setSnapSchema(String hDbSchema, String origin) {
        SNAP_INFO_CONNECTOR.setDbSchema(hDbSchema, origin);
    }

    public static boolean isSnapRac() {
        return Boolean.parseBoolean(SNAP_INFO_CONNECTOR.isDbRac());
    }

    public static String getSnapRac() {
        return SNAP_INFO_CONNECTOR.isDbRac();
    }

    public static void setSnapRac(String hDbRac, String origin) {
        SNAP_INFO_CONNECTOR.setDbRac(hDbRac, origin);
    }

    // Init with properties

    public static void initFromDeviceProperties(final DbDevice device) throws SnapshotingException {
        try {
            SNAP_INFO_CONNECTOR.initFromDeviceProperties(device);
        } catch (final DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    public static void initFromClassProperties() throws SnapshotingException {
        try {
            SNAP_INFO_CONNECTOR.initFromClassProperties();
        } catch (final DevFailed devFailed) {
            throw new SnapshotingException(devFailed);
        }
    }

    public static void initFromDefaultProperties() {
        SNAP_INFO_CONNECTOR.initFromDefaultProperties(ConfigConst.DEFAULT_S_HOST, ConfigConst.DEFAULT_S_BD,
                ConfigConst.DEFAULT_S_SCHEMA, ConfigConst.DEFAULT_SB_USER, ConfigConst.DEFAULT_SB_PASSWD, "false");
    }

    public static void initFromSystemProperties() {
        SNAP_INFO_CONNECTOR.initFromSystemProperties("SNAP_HOST", "SNAP_NAME", "SNAP_SCHEMA", "SNAP_USER", "SNAP_PASS",
                "SNAP_RAC");
    }

    public static void initFromArgumentsProperties(final String host, final String name, final String schema,
            final String user, final String pass, final String isRac) {
        SNAP_INFO_CONNECTOR.initFromArgumentsProperties(host, name, schema, user, pass, isRac);
    }

    /**
     * Try to initialize information with all known methods. The search is
     * interrupted when a value is find for an information. <br>
     * Order of search :
     * <ul>
     * <li>Given arguments</li>
     * <li>System properties</li>
     * <li>Device Properties</li>
     * <li>Class Properties From SnapManager</li>
     * <li>Default registered properties (only for test)</li>
     * </ul>
     */
    public static void performAllInit(final DbDevice device, final String host, final String name, final String schema,
            final String user, final String pass, final String isRac) throws SnapshotingException {

        initFromArgumentsProperties(host, name, schema, user, pass, isRac);
        initFromSystemProperties();
        initFromDeviceProperties(device);
        initFromClassProperties();
        initFromDefaultProperties();
    }

    public static void performSnapInit(DbDevice device, boolean searchSystemProperties, boolean searchClassProperties)
            throws SnapshotingException {

        if (searchSystemProperties) {
            initFromSystemProperties();
        }
        if (device != null) {
            initFromDeviceProperties(device);
        }
        if (searchClassProperties) {
            initFromClassProperties();
        }
        initFromDefaultProperties();
    }

    public static StringBuilder appendSnapConnectionInfoLog(StringBuilder builder) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        result.append("################\n");
        result.append("### Snapshot ###\n");
        result.append("################\n");
        SNAP_INFO_CONNECTOR.appendLoggerTrace(result);
        result.append("\n############################");
        return result;
    }

}
