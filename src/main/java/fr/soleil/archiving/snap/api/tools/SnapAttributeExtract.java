package fr.soleil.archiving.snap.api.tools;

import java.lang.reflect.Array;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An extracted snapshot attribute
 * 
 * @author chinkumo
 */
public class SnapAttributeExtract extends SnapAttribute {

    // Everything here is already available in SnapAttribute
    private int dimX;

    protected static Integer parseInteger(String arg) {
        Integer value;
        try {
            value = Integer.valueOf(arg);
        } catch (NumberFormatException nfe) {
            value = Integer.valueOf((int) Double.parseDouble(arg));
        }
        return value;
    }

    protected static Short parseShort(String arg) {
        Short value;
        try {
            value = Short.valueOf(arg);
        } catch (NumberFormatException nfe) {
            value = Short.valueOf((short) Double.parseDouble(arg));
        }
        return value;
    }

    protected static Byte parseByte(String arg) {
        Byte value;
        try {
            value = Byte.valueOf(arg);
        } catch (NumberFormatException nfe) {
            value = Byte.valueOf((byte) Double.parseDouble(arg));
        }
        return value;
    }

    protected static int toInt(String arg) {
        int value;
        try {
            value = Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            value = (int) Double.parseDouble(arg);
        }
        return value;
    }

    protected static short toShort(String arg) {
        short value;
        try {
            value = Short.parseShort(arg);
        } catch (NumberFormatException e) {
            value = (short) Double.parseDouble(arg);
        }
        return value;
    }

    protected static byte toByte(String arg) {
        byte value;
        try {
            value = Byte.parseByte(arg);
        } catch (NumberFormatException e) {
            value = (byte) Double.parseDouble(arg);
        }
        return value;
    }

    /**
     * 
     * @param argin
     *            0= name, 1=id, 2=data type, 3= data format, 4= writable
     */
    public SnapAttributeExtract(final String[] argin) {
        setAttributeCompleteName(argin[0]);
        setAttId(Integer.parseInt(argin[1]));
        dataType = Integer.parseInt(argin[2]);
        dataFormat = Integer.parseInt(argin[3]);
        writable = Integer.parseInt(argin[4]);
        Object value = "NaN";

        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (writable) {
                    case AttrWriteType._READ:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                value = new String(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                value = parseInteger(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                value = Byte.valueOf(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                value = parseInteger(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                value = parseInteger(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                value = Boolean.valueOf(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                value = Short.valueOf(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                value = Float.valueOf(argin[5]);
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                value = Double.valueOf(argin[5]);
                                break;
                            default:
                                value = Double.valueOf(argin[5]);
                                break;
                        }
                        break;

                    case AttrWriteType._READ_WITH_WRITE:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                final String[] valueString = { new String(argin[5]), new String(argin[6]) };
                                value = valueString;
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                final int[] valueInteger = { toInt(argin[5]), toInt(argin[6]) };
                                value = valueInteger;
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                final byte[] valueByte = { toByte(argin[5]), toByte(argin[6]) };
                                value = valueByte;
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                final int[] valueLong = { toInt(argin[5]), toInt(argin[6]) };
                                value = valueLong;
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                final int[] valueULong = { toInt(argin[5]), toInt(argin[6]) };
                                value = valueULong;
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                final boolean[] valueBoolean = { new Boolean(argin[5]), new Boolean(argin[6]) };
                                value = valueBoolean;
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                final short[] valueShort = { toShort(argin[5]), toShort(argin[6]) };
                                value = valueShort;
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                final float[] valueFloat = { new Float(argin[5]), new Float(argin[6]) };
                                value = valueFloat;
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                final double[] valueDouble = { new Double(argin[5]), new Double(argin[6]) };
                                value = valueDouble;
                                break;
                            default:
                                final double[] valueDouble2 = { new Double(argin[5]), new Double(argin[6]) };
                                value = valueDouble2;
                                break;
                        }
                        break;

                    case AttrWriteType._WRITE:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                value = new String(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                value = parseInteger(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                value = Byte.valueOf(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                value = parseInteger(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                value = parseInteger(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                value = Boolean.valueOf(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                value = Short.valueOf(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                value = Float.valueOf(argin[6]);
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                value = Double.valueOf(argin[6]);
                                break;
                            default:
                                value = Double.valueOf(argin[6]);
                                break;
                        }
                        break;

                    case AttrWriteType._READ_WRITE:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                final String[] valueString = { new String(argin[5]), new String(argin[6]) };
                                value = valueString;
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                final int[] valueInteger = { toInt(argin[5]), toInt(argin[6]) };
                                value = valueInteger;
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                final byte[] valueByte = { toByte(argin[5]), toByte(argin[6]) };
                                value = valueByte;
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                final int[] valueLong = { toInt(argin[5]), toInt(argin[6]) };
                                value = valueLong;
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                final int[] valueULong = { toInt(argin[5]), toInt(argin[6]) };
                                value = valueULong;
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                final boolean[] valueBoolean = { new Boolean(argin[5]), new Boolean(argin[6]) };
                                value = valueBoolean;
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                final short[] valueShort = { toShort(argin[5]), toShort(argin[6]) };
                                value = valueShort;
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                final float[] valueFloat = { new Float(argin[5]), new Float(argin[6]) };
                                value = valueFloat;
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                final double[] valueDouble = { new Double(argin[5]), new Double(argin[6]) };
                                value = valueDouble;
                                break;
                            default:
                                final double[] valueDouble2 = { new Double(argin[5]), new Double(argin[6]) };
                                value = valueDouble2;
                                break;
                        }
                        break;
                }
                break;
            case AttrDataFormat._SPECTRUM:
                final String toSplitRead = argin[5];
                final String toSplitWrite = argin[6];
                String[] stringArrayRead;
                String[] stringArrayWrite;
                switch (writable) {
                    case AttrWriteType._WRITE:
                        if (toSplitWrite == null || "NaN".equalsIgnoreCase(toSplitWrite.trim())
                                || "[]".equalsIgnoreCase(toSplitWrite.trim())) {
                            value = "NaN";
                            break;
                        }
                        stringArrayWrite = toSplitWrite.substring(1, toSplitWrite.length() - 1)
                                .split(GlobalConst.CLOB_SEPARATOR);
                        switch (dataType) {
                            case TangoConst.Tango_DEV_BOOLEAN:
                                value = new boolean[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    try {
                                        ((boolean[]) value)[i] = (((byte) Double
                                                .parseDouble(stringArrayWrite[i])) != 0);
                                    } catch (final NumberFormatException n) {
                                        ((boolean[]) value)[i] = "true".equalsIgnoreCase(stringArrayWrite[i]);
                                    }
                                }
                                break;
                            case TangoConst.Tango_DEV_STRING:
                                value = new String[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((String[]) value)[i] = new String(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                                value = new byte[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((byte[]) value)[i] = toByte(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                                value = new int[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((int[]) value)[i] = toInt(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_SHORT:
                                value = new short[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((short[]) value)[i] = toShort(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                value = new float[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((float[]) value)[i] = Float.parseFloat(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                value = new double[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((double[]) value)[i] = Double.parseDouble(stringArrayWrite[i]);
                                }
                                break;
                            default:
                                value = "NaN";
                        }
                        break;
                    case AttrWriteType._READ:
                        if (toSplitRead == null || "NaN".equalsIgnoreCase(toSplitRead.trim())
                                || "[]".equalsIgnoreCase(toSplitRead.trim())) {
                            value = "NaN";
                            break;
                        }
                        stringArrayRead = toSplitRead.substring(1, toSplitRead.length() - 1)
                                .split(GlobalConst.CLOB_SEPARATOR);
                        switch (dataType) {
                            case TangoConst.Tango_DEV_BOOLEAN:
                                value = new boolean[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    try {
                                        ((boolean[]) value)[i] = ((byte) Double.parseDouble(stringArrayRead[i])) != 0;
                                    } catch (final NumberFormatException n) {
                                        ((boolean[]) value)[i] = "true".equalsIgnoreCase(stringArrayRead[i]);
                                    }
                                }
                                break;
                            case TangoConst.Tango_DEV_STRING:
                                value = new String[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((String[]) value)[i] = new String(stringArrayRead[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                                value = new byte[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((byte[]) value)[i] = toByte(stringArrayRead[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_STATE:
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                                value = new int[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((int[]) value)[i] = toInt(stringArrayRead[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_SHORT:
                                value = new short[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((short[]) value)[i] = toShort(stringArrayRead[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                value = new float[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((float[]) value)[i] = Float.parseFloat(stringArrayRead[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                value = new double[stringArrayRead.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((double[]) value)[i] = Double.parseDouble(stringArrayRead[i]);
                                }
                                break;
                            default:
                                value = "NaN";
                        }
                        break;

                    case AttrWriteType._READ_WRITE:
                    case AttrWriteType._READ_WITH_WRITE:
                        if (toSplitWrite == null || "NaN".equalsIgnoreCase(toSplitWrite.trim())
                                || "[]".equalsIgnoreCase(toSplitWrite.trim())) {
                            value = "NaN";
                            break;
                        }
                        if (toSplitRead == null || "NaN".equalsIgnoreCase(toSplitRead.trim())
                                || "[]".equalsIgnoreCase(toSplitRead.trim())) {
                            value = "NaN";
                            break;
                        }
                        stringArrayRead = toSplitRead.substring(1, toSplitRead.length() - 1)
                                .split(GlobalConst.CLOB_SEPARATOR);
                        stringArrayWrite = toSplitWrite.substring(1, toSplitWrite.length() - 1)
                                .split(GlobalConst.CLOB_SEPARATOR);
                        value = new Object[2];
                        switch (dataType) {
                            case TangoConst.Tango_DEV_BOOLEAN:
                                ((Object[]) value)[0] = new boolean[stringArrayRead.length];
                                ((Object[]) value)[1] = new boolean[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    try {
                                        ((boolean[]) ((Object[]) value)[0])[i] = (((byte) Double
                                                .parseDouble(stringArrayRead[i])) != 0);
                                    } catch (final NumberFormatException n) {
                                        ((boolean[]) ((Object[]) value)[0])[i] = "true"
                                                .equalsIgnoreCase(stringArrayRead[i]);
                                    }
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    try {
                                        ((boolean[]) ((Object[]) value)[1])[i] = (((byte) Double
                                                .parseDouble(stringArrayWrite[i])) != 0);
                                    } catch (final NumberFormatException n) {
                                        ((boolean[]) ((Object[]) value)[1])[i] = "true"
                                                .equalsIgnoreCase(stringArrayWrite[i]);
                                    }
                                }
                                break;
                            case TangoConst.Tango_DEV_STRING:
                                ((Object[]) value)[0] = new String[stringArrayRead.length];
                                ((Object[]) value)[1] = new String[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((String[]) ((Object[]) value)[0])[i] = new String(stringArrayRead[i]);
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((String[]) ((Object[]) value)[1])[i] = new String(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                                ((Object[]) value)[0] = new byte[stringArrayRead.length];
                                ((Object[]) value)[1] = new byte[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((byte[]) ((Object[]) value)[0])[i] = toByte(stringArrayRead[i]);
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((byte[]) ((Object[]) value)[1])[i] = toByte(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                                ((Object[]) value)[0] = new int[stringArrayRead.length];
                                ((Object[]) value)[1] = new int[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((int[]) ((Object[]) value)[0])[i] = toInt(stringArrayRead[i]);
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((int[]) ((Object[]) value)[1])[i] = toInt(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_SHORT:
                                ((Object[]) value)[0] = new short[stringArrayRead.length];
                                ((Object[]) value)[1] = new short[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((short[]) ((Object[]) value)[0])[i] = toShort(stringArrayRead[i]);
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((short[]) ((Object[]) value)[1])[i] = toShort(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                ((Object[]) value)[0] = new float[stringArrayRead.length];
                                ((Object[]) value)[1] = new float[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((float[]) ((Object[]) value)[0])[i] = Float.parseFloat(stringArrayRead[i]);
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((float[]) ((Object[]) value)[1])[i] = Float.parseFloat(stringArrayWrite[i]);
                                }
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                ((Object[]) value)[0] = new double[stringArrayRead.length];
                                ((Object[]) value)[1] = new double[stringArrayWrite.length];
                                for (int i = 0; i < stringArrayRead.length; i++) {
                                    ((double[]) ((Object[]) value)[0])[i] = Double.parseDouble(stringArrayRead[i]);
                                }
                                for (int i = 0; i < stringArrayWrite.length; i++) {
                                    ((double[]) ((Object[]) value)[1])[i] = Double.parseDouble(stringArrayWrite[i]);
                                }
                                break;
                            default:
                                value = "NaN";
                        }
                        break;

                }
                break;
            default:
                value = "NaN";
        }

        setValue(value, null);

    }

    public SnapAttributeExtract(final AttributeLight snapAttributeLight) {
        super.setAttributeCompleteName(snapAttributeLight.getAttributeCompleteName());
        super.setAttId(snapAttributeLight.getAttributeId());
        dataFormat = snapAttributeLight.getDataFormat();
        dataType = snapAttributeLight.getDataType();
        writable = snapAttributeLight.getWritable();
    }

    // Everything here is already available in SnapAttribute
    public String valueToString(final int pos) {
        final String nullvalue = "NULL";
        String value = nullvalue;
        if (getValue() == null) {
            return nullvalue;
        }
        if (getValue() instanceof Object[]) {
            final Object[] valTab = (Object[]) getValue();
            if ((writable == AttrWriteType._READ_WITH_WRITE || writable == AttrWriteType._READ_WRITE)
                    && valTab[pos] == null) {
                return nullvalue;
            }
        }

        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (writable) {
                    case AttrWriteType._READ:
                        if (pos == 0) {
                            value = String.valueOf(getValue());
                        }
                        break;
                    case AttrWriteType._WRITE:
                        if (pos == 1) {
                            value = String.valueOf(getValue());
                        }
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        value = String.valueOf(Array.get(getValue(), pos));
                        break;
                }
                break;
            case AttrDataFormat._SPECTRUM:
                if (getValue() == null) {
                    return value;
                }
                value = "[";
                if (pos == 0) {
                    switch (writable) {
                        case AttrWriteType._READ:
                            switch (dataType) {
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    final boolean[] valb = (boolean[]) getValue();
                                    if (valb != null && valb.length > 0) {
                                        for (int i = 0; i < valb.length - 1; i++) {
                                            value += valb[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valb[valb.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_STRING:
                                    final String[] valstr = (String[]) getValue();
                                    if (valstr != null && valstr.length > 0) {
                                        for (int i = 0; i < valstr.length - 1; i++) {
                                            value += valstr[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valstr[valstr.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_CHAR:
                                case TangoConst.Tango_DEV_UCHAR:
                                    final byte[] valc = (byte[]) getValue();
                                    if (valc != null && valc.length > 0) {
                                        for (int i = 0; i < valc.length - 1; i++) {
                                            value += valc[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valc[valc.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_STATE:
                                case TangoConst.Tango_DEV_LONG:
                                case TangoConst.Tango_DEV_ULONG:
                                    final int[] vall = (int[]) getValue();
                                    if (vall != null && vall.length > 0) {
                                        for (int i = 0; i < vall.length - 1; i++) {
                                            value += vall[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vall[vall.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_USHORT:
                                case TangoConst.Tango_DEV_SHORT:
                                    final short[] vals = (short[]) getValue();
                                    if (vals != null && vals.length > 0) {
                                        for (int i = 0; i < vals.length - 1; i++) {
                                            value += vals[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vals[vals.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_FLOAT:
                                    final float[] valf = (float[]) getValue();
                                    if (valf != null && valf.length > 0) {
                                        for (int i = 0; i < valf.length - 1; i++) {
                                            value += valf[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valf[valf.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_DOUBLE:
                                    final double[] vald = (double[]) getValue();
                                    if (vald != null && vald.length > 0) {
                                        for (int i = 0; i < vald.length - 1; i++) {
                                            value += vald[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vald[vald.length - 1];
                                    }
                                    break;
                                default:
                                    value += "NaN";
                            }
                            break;
                        case AttrWriteType._READ_WITH_WRITE:
                        case AttrWriteType._READ_WRITE:
                            final Object[] temp = (Object[]) getValue();
                            switch (dataType) {
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    final boolean[] valb = (boolean[]) temp[pos];
                                    if (valb != null && valb.length > 0) {
                                        for (int i = 0; i < valb.length - 1; i++) {
                                            value += valb[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valb[valb.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_STRING:
                                    final String[] valstr = (String[]) temp[pos];
                                    if (valstr != null && valstr.length > 0) {
                                        for (int i = 0; i < valstr.length - 1; i++) {
                                            value += valstr[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valstr[valstr.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_CHAR:
                                case TangoConst.Tango_DEV_UCHAR:
                                    final byte[] valc = (byte[]) temp[pos];
                                    if (valc != null && valc.length > 0) {
                                        for (int i = 0; i < valc.length - 1; i++) {
                                            value += valc[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valc[valc.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_STATE:
                                case TangoConst.Tango_DEV_LONG:
                                case TangoConst.Tango_DEV_ULONG:
                                    final int[] vall = (int[]) temp[pos];
                                    if (vall != null && vall.length > 0) {
                                        for (int i = 0; i < vall.length - 1; i++) {
                                            value += vall[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vall[vall.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_USHORT:
                                case TangoConst.Tango_DEV_SHORT:
                                    final short[] vals = (short[]) temp[pos];
                                    if (vals != null && vals.length > 0) {
                                        for (int i = 0; i < vals.length - 1; i++) {
                                            value += vals[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vals[vals.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_FLOAT:
                                    final float[] valf = (float[]) temp[pos];
                                    if (valf != null && valf.length > 0) {
                                        for (int i = 0; i < valf.length - 1; i++) {
                                            value += valf[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valf[valf.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_DOUBLE:
                                    final double[] vald = (double[]) temp[pos];
                                    if (vald != null && vald.length > 0) {
                                        for (int i = 0; i < vald.length - 1; i++) {
                                            value += vald[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vald[vald.length - 1];
                                    }
                                    break;
                                default:
                                    value += "NaN";
                            }
                            break;
                    }
                } else if (pos == 1) {
                    switch (writable) {
                        case AttrWriteType._WRITE:
                            switch (dataType) {
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    final boolean[] valb = (boolean[]) getValue();
                                    if (valb != null && valb.length > 0) {
                                        for (int i = 0; i < valb.length - 1; i++) {
                                            value += valb[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valb[valb.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_STRING:
                                    final String[] valstr = (String[]) getValue();
                                    if (valstr != null && valstr.length > 0) {
                                        for (int i = 0; i < valstr.length - 1; i++) {
                                            value += valstr[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valstr[valstr.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_CHAR:
                                case TangoConst.Tango_DEV_UCHAR:
                                    final byte[] valc = (byte[]) getValue();
                                    if (valc != null) {
                                        for (int i = 0; i < valc.length - 1; i++) {
                                            value += valc[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valc[valc.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_LONG:
                                case TangoConst.Tango_DEV_ULONG:
                                    final int[] vall = (int[]) getValue();
                                    if (vall != null && vall.length > 0) {
                                        for (int i = 0; i < vall.length - 1; i++) {
                                            value += vall[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vall[vall.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_USHORT:
                                case TangoConst.Tango_DEV_SHORT:
                                    final short[] vals = (short[]) getValue();
                                    if (vals != null && vals.length > 0) {
                                        for (int i = 0; i < vals.length - 1; i++) {
                                            value += vals[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vals[vals.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_FLOAT:
                                    final float[] valf = (float[]) getValue();
                                    if (valf != null && valf.length > 0) {
                                        for (int i = 0; i < valf.length - 1; i++) {
                                            value += valf[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valf[valf.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_DOUBLE:
                                    final double[] vald = (double[]) getValue();
                                    if (vald != null && vald.length > 0) {
                                        for (int i = 0; i < vald.length - 1; i++) {
                                            value += vald[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vald[vald.length - 1];
                                    }
                                    break;
                                default:
                                    value += "NaN";
                            }
                            break;
                        case AttrWriteType._READ_WITH_WRITE:
                        case AttrWriteType._READ_WRITE:
                            final Object[] temp = (Object[]) getValue();
                            switch (dataType) {
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    final boolean[] valb = (boolean[]) temp[pos];
                                    if (valb != null && valb.length > 0) {
                                        for (int i = 0; i < valb.length - 1; i++) {
                                            value += valb[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valb[valb.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_STRING:
                                    final String[] valstr = (String[]) temp[pos];
                                    if (valstr != null && valstr.length > 0) {
                                        for (int i = 0; i < valstr.length - 1; i++) {
                                            value += valstr[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valstr[valstr.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_CHAR:
                                case TangoConst.Tango_DEV_UCHAR:
                                    final byte[] valc = (byte[]) temp[pos];
                                    if (valc != null && valc.length > 0) {
                                        for (int i = 0; i < valc.length - 1; i++) {
                                            value += valc[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valc[valc.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_LONG:
                                case TangoConst.Tango_DEV_ULONG:
                                    final int[] vall = (int[]) temp[pos];
                                    if (vall != null && vall.length > 0) {
                                        for (int i = 0; i < vall.length - 1; i++) {
                                            value += vall[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vall[vall.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_USHORT:
                                case TangoConst.Tango_DEV_SHORT:
                                    final short[] vals = (short[]) temp[pos];
                                    if (vals != null && vals.length > 0) {
                                        for (int i = 0; i < vals.length - 1; i++) {
                                            value += vals[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vals[vals.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_FLOAT:
                                    final float[] valf = (float[]) temp[pos];
                                    if (valf != null && valf.length > 0) {
                                        for (int i = 0; i < valf.length - 1; i++) {
                                            value += valf[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += valf[valf.length - 1];
                                    }
                                    break;
                                case TangoConst.Tango_DEV_DOUBLE:
                                    final double[] vald = (double[]) temp[pos];
                                    if (vald != null && vald.length > 0) {
                                        for (int i = 0; i < vald.length - 1; i++) {
                                            value += vald[i] + GlobalConst.CLOB_SEPARATOR;
                                        }
                                        value += vald[vald.length - 1];
                                    }
                                    break;
                                default:
                                    value += "NaN";
                            }
                            break;
                    }
                }
                value += "]";
                break;
            case AttrDataFormat._IMAGE:
                value = ((String) getValue()).toString();
                break;
        }
        return value;
    }

    /**
     * 
     * @param device_data
     */
    public void insertsnapAttributeValue(final DeviceData device_data) {
        // TODO Auto-generated method stub
        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (writable) {
                    case AttrWriteType._READ:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                device_data.insert((String) getValue());
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                device_data.insert((Integer) getValue());
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                device_data.insert((Byte) getValue());
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                device_data.insert((Integer) getValue());
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                device_data.insert((Integer) getValue());
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                device_data.insert((Boolean) getValue());
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                device_data.insert((Short) getValue());
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                device_data.insert((Float) getValue());
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                device_data.insert((Double) getValue());
                                break;
                            default:
                                device_data.insert((Double) getValue());
                                break;
                        }
                        break;
                    case AttrWriteType._WRITE:
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                device_data.insert((String) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                device_data.insert((Integer) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                device_data.insert((Byte) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                device_data.insert((Integer) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                device_data.insert((Integer) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                device_data.insert((Boolean) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                device_data.insert((Short) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                device_data.insert((Float) Array.get(getValue(), 0));
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                            default:
                                device_data.insert((Double) Array.get(getValue(), 0));
                                break;
                        }
                        break;
                }
                break;

            case AttrDataFormat._SPECTRUM:

                switch (writable) {
                    case AttrWriteType._READ:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                device_data.insert((String[]) getValue());
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                final int[] val_St = (int[]) getValue();
                                device_data.insert(val_St.clone());
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                final byte[] val_UChar = (byte[]) getValue();
                                device_data.insert(val_UChar.clone());
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                final int[] val_Lg = (int[]) getValue();
                                device_data.insert(val_Lg.clone());
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                final int[] val_ULg = (int[]) getValue();
                                device_data.insert(val_ULg.clone());
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                final short[] val_Short = (short[]) getValue();
                                device_data.insert(val_Short.clone());
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                final float[] val_Float = (float[]) getValue();
                                device_data.insert(val_Float.clone());
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                double[] val_Double = (double[]) getValue();
                                device_data.insert(val_Double.clone());
                                break;
                            default:
                                val_Double = (double[]) getValue();
                                device_data.insert(val_Double.clone());
                                break;
                        }
                        break;
                    case AttrWriteType._WRITE:
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        switch (dataType) {
                            case TangoConst.Tango_DEV_STRING:
                                device_data.insert((String[]) ((Object[]) getValue())[0]);
                                break;
                            case TangoConst.Tango_DEV_STATE:
                                final int[] val_St = (int[]) ((Object[]) getValue())[0];
                                device_data.insert(val_St.clone());
                                break;
                            case TangoConst.Tango_DEV_UCHAR:
                                final byte[] val_UChar = (byte[]) ((Object[]) getValue())[0];
                                device_data.insert(val_UChar.clone());
                                break;
                            case TangoConst.Tango_DEV_LONG:
                                final int[] val_Lg = (int[]) ((Object[]) getValue())[0];
                                device_data.insert(val_Lg.clone());
                                break;
                            case TangoConst.Tango_DEV_ULONG:
                                final int[] val_ULg = (int[]) ((Object[]) getValue())[0];
                                device_data.insert(val_ULg.clone());
                                break;
                            case TangoConst.Tango_DEV_BOOLEAN:
                                break;
                            case TangoConst.Tango_DEV_SHORT:
                                final short[] val_Short = (short[]) ((Object[]) getValue())[0];
                                device_data.insert(val_Short.clone());
                                break;
                            case TangoConst.Tango_DEV_FLOAT:
                                final float[] val_Float = (float[]) getValue();
                                device_data.insert(val_Float.clone());
                                break;
                            case TangoConst.Tango_DEV_DOUBLE:
                                double[] val_Double = (double[]) ((Object[]) getValue())[0];
                                device_data.insert(val_Double.clone());
                                break;
                            default:
                                val_Double = (double[]) ((Object[]) getValue())[0];
                                device_data.insert(val_Double.clone());
                                break;
                        }
                        break;
                }
                break;

            case AttrDataFormat._IMAGE:
                break;
        }
    }

    public Object getWriteValue() {
        Object write_value = null;
        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (writable) {
                    case AttrWriteType._READ:
                        break;
                    case AttrWriteType._READ_WRITE:
                    case AttrWriteType._READ_WITH_WRITE:
                        if (getValue() == null) {
                            write_value = null;
                        } else {
                            write_value = Array.get(getValue(), 1);
                        }
                        break;
                    case AttrWriteType._WRITE:
                        write_value = getValue();
                        break;
                }
                break;
            case AttrDataFormat._SPECTRUM:
            case AttrDataFormat._IMAGE:
                switch (writable) {
                    case AttrWriteType._READ:
                        break;
                    case AttrWriteType._WRITE:
                        if (getValue() == null || "NaN".equals(getValue())) {
                            return "NaN";
                        }
                        write_value = getValue();
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        final Object temp = getValue();
                        if (temp == null || "NaN".equals(temp)) {
                            return "NaN";
                        }
                        Object secondVal = Array.get(temp, 1);
                        if (secondVal == null || "NaN".equals(secondVal)) {
                            return "NaN";
                        }
                        write_value = secondVal;
                        break;
                }
                break;
        }
        return write_value;
    }

    public Object getNullElementsWrite() {
        Object nullWrite = null;
        Object nullElements = getNullElements();
        if (nullElements != null) {
            switch (writable) {
                case AttrWriteType._READ:
                    break;
                case AttrWriteType._READ_WITH_WRITE:
                case AttrWriteType._READ_WRITE:
                    nullWrite = Array.get(nullElements, 1);
                    break;
                case AttrWriteType._WRITE:
                    nullWrite = nullElements;
                    break;
            }
        }
        return nullWrite;
    }

    /**
     * 
     * @return
     */
    public Object getReadValue() {
        Object read_value = "NaN";
        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (writable) {
                    case AttrWriteType._READ:
                        read_value = getValue();
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        if (getValue() == null) {
                            read_value = null;
                        } else {
                            read_value = Array.get(getValue(), 0);
                        }
                        break;
                    case AttrWriteType._WRITE:
                        break;
                }
                break;
            case AttrDataFormat._SPECTRUM:
            case AttrDataFormat._IMAGE:
                switch (writable) {
                    case AttrWriteType._READ:
                        if (getValue() == null || "NaN".equals(getValue())) {
                            return "NaN";
                        }
                        read_value = getValue();
                        break;
                    case AttrWriteType._WRITE:
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        if (getValue() == null || "NaN".equals(getValue())) {
                            return "NaN";
                        }
                        final Object temp = getValue();
                        Object firstVal = Array.get(temp, 0);
                        if (firstVal == null || "NaN".equals(firstVal)) {
                            return "NaN";
                        }
                        read_value = firstVal;
                        break;
                }
                break;
        }
        return read_value;
    }

    public Object getNullElementsRead() {
        Object nullRead = null;
        Object nullElements = getNullElements();
        if (nullElements != null) {
            switch (writable) {
                case AttrWriteType._READ:
                    nullRead = nullElements;
                    break;
                case AttrWriteType._READ_WITH_WRITE:
                    nullRead = Array.get(nullElements, 0);
                    break;
                case AttrWriteType._WRITE:
                    break;
                case AttrWriteType._READ_WRITE:
                    Array.get(nullElements, 0);
                    break;
            }
        }
        return nullRead;
    }

    public Object getNewValue(final String stringValue) {
        Object newValue = new Object();

        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                switch (dataType) {
                    case TangoConst.Tango_DEV_BOOLEAN: {
                        newValue = Boolean.parseBoolean(stringValue);
                        break;
                    }
                    case TangoConst.Tango_DEV_DOUBLE: {
                        newValue = Double.parseDouble(stringValue);
                        break;
                    }
                    case TangoConst.Tango_DEV_FLOAT: {
                        newValue = Float.parseFloat(stringValue);
                        break;
                    }
                    case TangoConst.Tango_DEV_INT:
                    case TangoConst.Tango_DEV_LONG:
                    case TangoConst.Tango_DEV_STATE:
                    case TangoConst.Tango_DEV_ULONG: {
                        newValue = parseInteger(stringValue);
                        break;
                    }
                    case TangoConst.Tango_DEV_SHORT: {
                        newValue = parseShort(stringValue);
                        break;
                    }
                    case TangoConst.Tango_DEV_UCHAR: {
                        newValue = parseByte(stringValue);
                        break;
                    }
                    case TangoConst.Tango_DEV_STRING:
                    default:
                        newValue = stringValue;
                        break;
                } // End of switch ( data_type )
                break; // End of case AttrDataFormat._SCALAR

            case AttrDataFormat._SPECTRUM: {
                final String[] stringTable = stringValue.split(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                Object newTable = null;
                switch (dataType) {
                    case TangoConst.Tango_DEV_BOOLEAN: {
                        boolean[] bool = new boolean[dimX];
                        for (int i = 0; i < stringTable.length; i++) {
                            bool[i] = Boolean.parseBoolean(stringTable[i]);
                        }
                        newTable = bool;
                        break;
                    }
                    case TangoConst.Tango_DEV_DOUBLE: {
                        double[] db = new double[dimX];
                        for (int i = 0; i < stringTable.length; i++) {
                            db[i] = Double.parseDouble(stringTable[i]);
                        }
                        newTable = db;
                        break;
                    }
                    case TangoConst.Tango_DEV_FLOAT: {
                        float[] fl = new float[dimX];
                        for (int i = 0; i < stringTable.length; i++) {
                            fl[i] = Float.parseFloat(stringTable[i]);
                        }
                        newTable = fl;
                        break;
                    }
                    case TangoConst.Tango_DEV_INT:
                    case TangoConst.Tango_DEV_LONG:
                    case TangoConst.Tango_DEV_STATE:
                    case TangoConst.Tango_DEV_ULONG: {
                        int[] in = new int[dimX];
                        for (int i = 0; i < stringTable.length; i++) {
                            in[i] = toInt(stringTable[i]);
                        }
                        newTable = in;
                        break;
                    }
                    case TangoConst.Tango_DEV_SHORT: {
                        short[] sh = new short[dimX];
                        for (int i = 0; i < stringTable.length; i++) {
                            sh[i] = toShort(stringTable[i]);
                        }
                        newTable = sh;
                        break;
                    }
                    case TangoConst.Tango_DEV_UCHAR: {
                        byte[] by = new byte[dimX];
                        for (int i = 0; i < stringTable.length; i++) {
                            by[i] = toByte(stringTable[i]);
                        }
                        newTable = by;
                        break;
                    }
                    case TangoConst.Tango_DEV_STRING: {
                        newTable = stringTable;
                        break;
                    }
                    default:
                        newTable = stringTable;
                        break;
                } // End of switch ( data_type )

                newValue = newTable;
                break;
            } // End of case AttrDataFormat._SPECTRUM

            case AttrDataFormat._IMAGE: {
                final String[][] stringMatrix = new String[dimX][dimX];
                final String[] stringTable = stringValue.split(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                for (int i = 0; i < stringTable.length; i++) {
                    stringMatrix[i] = stringTable[i].split(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                }

                Object[] newMatrix = null;

                switch (dataType) {
                    case TangoConst.Tango_DEV_BOOLEAN:
                        boolean[][] bool = new boolean[dimX][dimX];
                        for (int i = 0; i < dimX; i++) {
                            for (int j = 0; j < dimX; j++) {
                                bool[i][j] = Boolean.parseBoolean(stringMatrix[i][j]);
                            }
                        }
                        newMatrix = bool;
                        break;

                    case TangoConst.Tango_DEV_DOUBLE:
                        double[][] db = new double[dimX][dimX];
                        for (int i = 0; i < dimX; i++) {
                            for (int j = 0; j < dimX; j++) {
                                db[i][j] = Double.parseDouble(stringMatrix[i][j]);
                            }
                        }
                        newMatrix = db;
                        break;

                    case TangoConst.Tango_DEV_FLOAT:
                        float[][] fl = new float[dimX][dimX];
                        for (int i = 0; i < dimX; i++) {
                            for (int j = 0; j < dimX; j++) {
                                fl[i][j] = Float.parseFloat(stringMatrix[i][j]);
                            }
                        }
                        newMatrix = fl;
                        break;

                    case TangoConst.Tango_DEV_INT:
                    case TangoConst.Tango_DEV_LONG:
                    case TangoConst.Tango_DEV_STATE:
                    case TangoConst.Tango_DEV_ULONG:
                        int[][] in = new int[dimX][dimX];
                        for (int i = 0; i < dimX; i++) {
                            for (int j = 0; j < dimX; j++) {
                                in[i][j] = toInt(stringMatrix[i][j]);
                            }
                        }
                        newMatrix = in;
                        break;

                    case TangoConst.Tango_DEV_SHORT:
                        short[][] sh = new short[dimX][dimX];
                        for (int i = 0; i < dimX; i++) {
                            for (int j = 0; j < dimX; j++) {
                                sh[i][j] = toShort(stringMatrix[i][j]);
                            }
                        }
                        newMatrix = sh;
                        break;

                    case TangoConst.Tango_DEV_UCHAR:
                        byte[][] by = new byte[dimX][dimX];
                        for (int i = 0; i < dimX; i++) {
                            for (int j = 0; j < dimX; j++) {
                                by[i][j] = toByte(stringMatrix[i][j]);
                            }
                        }
                        newMatrix = by;
                        break;

                    case TangoConst.Tango_DEV_STRING:
                        newMatrix = stringMatrix;
                        break;

                    default:
                        newMatrix = stringMatrix;
                        break;
                } // End of switch ( data_type )

                newValue = newMatrix;
                break;
            } // End of case AttrDataFormat._IMAGE

            default:
                break;
        } // End of switch ( data_format )

        return newValue;
    }

    public void setWriteValue(final Object writeValue, final Object nullElements) {
        switch (getDataFormat()) {
            case AttrDataFormat._SCALAR:
            case AttrDataFormat._SPECTRUM:
                switch (writable) {
                    case AttrWriteType._READ:
                        break;

                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        final Object newValue = getValue();
                        Array.set(newValue, 1, writeValue);
                        Object newNullElements = getNullElements();
                        if (newNullElements == null) {
                            if (nullElements == null) {
                                newNullElements = null;
                            } else if (nullElements instanceof Boolean) {
                                newNullElements = new boolean[2];
                            } else {
                                newNullElements = Array.newInstance(nullElements.getClass(), 2);
                            }
                        }
                        Array.set(newNullElements, 1, nullElements);
                        setValue(newValue, newNullElements);
                        break;

                    case AttrWriteType._WRITE:
                        setValue(writeValue, nullElements);
                        break;
                }
                break;

            case AttrDataFormat._IMAGE:
                setValue(writeValue, nullElements);
                break;
        }
    }

    @Override
    public String toString() {
        String snapStr = ObjectUtils.EMPTY_STRING;
        final String value = (writable == AttrWriteType._READ || writable == AttrWriteType._READ_WITH_WRITE
                || writable == AttrWriteType._READ_WRITE ? "read value :  " + valueToString(0)
                        : ObjectUtils.EMPTY_STRING)
                + (writable == AttrWriteType._WRITE || writable == AttrWriteType._READ_WITH_WRITE
                        || writable == AttrWriteType._READ_WRITE ? "\t " + "write value : " + valueToString(1)
                                : ObjectUtils.EMPTY_STRING);
        snapStr = "attribute ID   : \t" + getAttId() + "\r\n" + "attribute Name : \t" + getAttributeCompleteName()
                + "\r\n" + "attribute value : \t" + value + "\r\n";
        return snapStr;
    }

    public String[] toArray() {
        String[] snapAttExt;
        snapAttExt = new String[7];
        snapAttExt[0] = getAttributeCompleteName();
        snapAttExt[1] = Integer.toString(getAttId());
        snapAttExt[2] = Integer.toString(dataType);
        snapAttExt[3] = Integer.toString(dataFormat);
        snapAttExt[4] = Integer.toString(writable);
        snapAttExt[5] = valueToString(0);
        snapAttExt[6] = valueToString(1);
        return snapAttExt;
    }

    /**
     * @return Returns the dimX.
     */
    public int getDimX() {
        return dimX;
    }

    /**
     * @param dimX
     *            The dimX to set.
     */
    public void setDimX(final int dimX) {
        this.dimX = dimX;
    }

    public String getdeviceName() {
        final String name = getAttributeCompleteName();
        return name.substring(0, name.lastIndexOf("/"));
    }
}
