package fr.soleil.archiving.snap.api.extractor.datasources.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.snap.api.extractor.tools.Tools;
import fr.soleil.archiving.snap.api.manager.ISnapManager;
import fr.soleil.archiving.snap.api.manager.SnapManagerImpl;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.SnapshotLight;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;

/**
 * An implementation that loads data from the Snap Database
 * 
 * @author chinkumo, CLAISSE
 */
public class RealSnapReader implements ISnapReader {
    private ISnapManager manager;
    private boolean isReady = false;
    private static final String SPLIT_CHAR = "|";

    RealSnapReader() {
        super();
    }

    @Override
    public synchronized void openConnection() throws DevFailed {
        try {
            if (!isReady) {
                manager = new SnapManagerImpl();
                isReady = true;
            }
        } catch (Exception e) {
            Tools.throwDevFailed(e);
        }
    }

    @Override
    public void closeConnection() {
        isReady = false;
    }

    @Override
    public SnapAttributeExtract[] getSnap(final int id) throws DevFailed {
        try {
            return manager.getSnap(id);
        } catch (SnapshotingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public SnapAttributeExtract[] getSnapValues(final int id, final String... attributeNames) throws DevFailed {
        try {
            return manager.getSnapValues(id, attributeNames);
        } catch (SnapshotingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public DevVarLongStringArray getSnapshotsForContext(final int contextId) throws DevFailed {
        Criterions searchCriterions = new Criterions();
        searchCriterions
                .addCondition(new Condition(SnapConst.TAB_SNAP[1], SnapConst.OP_EQUALS, Integer.toString(contextId)));
        SnapshotLight[] snapshots = null;
        try {
            snapshots = manager.findSnapshots(searchCriterions);
        } catch (Exception e) {
            Tools.throwDevFailed(e);
        }
        DevVarLongStringArray ret;
        if (snapshots == null || snapshots.length == 0) {
            ret = null;
        } else {
            int numberOfSnapshots = snapshots.length;
            ret = new DevVarLongStringArray();
            int[] lvalue = new int[numberOfSnapshots];
            String[] svalue = new String[numberOfSnapshots];
            for (int i = 0; i < numberOfSnapshots; i++) {
                SnapshotLight currentSnapshot = snapshots[i];
                lvalue[i] = currentSnapshot.getId_snap();
                svalue[i] = currentSnapshot.getSnap_date() + " , " + currentSnapshot.getComment();
            }
            ret.lvalue = lvalue;
            ret.svalue = svalue;
        }
        return ret;
    }

    @Override
    public int[] getSnapshotsID(final int ctxID, final String[] criterions) {
        int[] argout = null;
        Criterions ret = getInputCriterion(criterions);
        if (ret != null) {
            ret.addCondition(new Condition(SnapConst.TAB_SNAP[1], SnapConst.OP_EQUALS, Integer.toString(ctxID)));

            SnapshotLight[] newList;
            try {
                newList = manager.findSnapshots(ret);
                if (ret.getConditions("first") != null) {
                    argout = new int[1];
                    argout[0] = newList[0].getId_snap();
                } else if (ret.getConditions("last") != null) {
                    argout = new int[1];
                    argout[0] = newList[newList.length - 1].getId_snap();
                } else {
                    argout = new int[newList.length];
                    for (int i = 0; i < newList.length; i++) {
                        argout[i] = newList[i].getId_snap();
                    }
                }
            } catch (SnapshotingException e) {
                argout = null;
            }
        }
        return argout;
    }

    private Criterions getInputCriterion(final String[] criterions) {
        Criterions ret = new Criterions();
        try {
            Condition cond;
            String criterion_type = null;
            String criterion_op = null;
            String criterion_value = null;
            for (String criterion : criterions) {
                if (criterion.equals("first") || criterion.equals("last")) {
                    cond = getCondition(criterion, criterion, criterion);
                    ret.addCondition(cond);
                } else if (criterion.equals("id_snap") || criterion.equals("comment") || criterion.equals("time")) {
                    criterion_type = criterion.substring(0, criterion.indexOf(" "));
                    criterion = criterion.substring(criterion.indexOf(criterion_type) + criterion_type.length() + 1);
                    criterion_op = criterion.substring(0, criterion.indexOf(" "));
                    criterion_value = criterion.substring(criterion.indexOf(criterion_op) + criterion_op.length())
                            .trim();
                    cond = getCondition(criterion_op, criterion_value, criterion_type);
                    ret.addCondition(cond);
                } else {
                    ret = null;
                    break;
                }
            } // end for (String criterion : criterions)
        } catch (Exception e) {
            ret = null;
        }
        return ret;
    }

    // ------------------------------------------------
    /**
     * If both operator and threshold value are filled, builds a Condition from them. Otherwise returns null.
     * 
     * @param selectedItem The operator
     * @param text The value
     * @param id_field_key2 The field's id
     * @return The resulting Condition or null
     */
    public Condition getCondition(String selectedItem, String text, String id_field_key2) {
        Condition condition;
        // Date Formating
        if (id_field_key2.equals("time")) {
            text = Tools.formatDate(Tools.stringToMilli(text));
        } else if (id_field_key2.contains("comment")) {
            id_field_key2 = "snap_comment";
            selectedItem = getCommentCondition(selectedItem);
        }

        boolean isACriterion = true;

        if (selectedItem == null || selectedItem.isEmpty()) {
            isACriterion = false;
        }

        if (text == null || text.trim().isEmpty()) {
            isACriterion = false;
        }

        if (isACriterion || selectedItem.equals("first") || selectedItem.equals("last")) {
            condition = new Condition(id_field_key2, selectedItem, text.trim());
        } else {
            condition = null;
        }
        return condition;
    }

    private String getCommentCondition(final String selectedItem) {
        String res;
        if (selectedItem.equalsIgnoreCase("starts")) {
            res = "Starts with";
        } else if (selectedItem.equalsIgnoreCase("ends")) {
            res = "Ends with";
        } else {
            res = "Contains";
        }
        return res;
    }

    private Map<Integer, SnapContext> getAllSnapContext() throws DevFailed {
        Map<Integer, SnapContext> contextMap = new HashMap<>();
        Criterions searchCriterions = new Criterions();
        SnapContext[] contexts = null;
        int contextId = -1;
        try {
            contexts = manager.findContexts(searchCriterions);
        } catch (Exception e) {
            Tools.throwDevFailed(e);
        }

        if (contexts != null) {
            for (SnapContext context : contexts) {
                contextId = context.getId();
                contextMap.put(contextId, context);
            }
        }
        return contextMap;
    }

    @Override
    public DevVarLongStringArray getAllContexts() throws DevFailed {
        return getAllContexts(false);
    }

    // return with attributeValue
    private DevVarLongStringArray getAllContexts(boolean withAttribute) throws DevFailed {
        DevVarLongStringArray ret;
        Criterions searchCriterions = new Criterions();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SnapContext[] contextTab = null;

        try {
            contextTab = manager.findContexts(searchCriterions);
        } catch (Exception e) {
            Tools.throwDevFailed(e);
        }
        if (contextTab == null || contextTab.length == 0) {
            ret = null;
        } else {
            int numberOfContexts = contextTab.length;

            ret = new DevVarLongStringArray();
            int[] lvalue = new int[numberOfContexts];
            String[] svalue = new String[numberOfContexts];

            List<String> attributesList = null;
            int contextId = -1;
            for (int i = 0; i < numberOfContexts; i++) {
                SnapContext currentContext = contextTab[i];
                contextId = currentContext.getId();
                lvalue[i] = contextId;

                if (withAttribute) {
                    attributesList = getAttributeForContext(currentContext, searchCriterions);
                }

                svalue[i] = currentContext.getName() + SPLIT_CHAR + currentContext.getAuthor_name() + SPLIT_CHAR
                        + sdf.format(currentContext.getCreation_date()) + SPLIT_CHAR + currentContext.getReason()
                        + SPLIT_CHAR + currentContext.getDescription();
                if (attributesList != null && !attributesList.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    for (String att : attributesList) {
                        sb.append(SPLIT_CHAR);
                        sb.append(att);
                    }
                    svalue[i] = svalue[i] + sb.toString();
                }
            }

            ret.lvalue = lvalue;
            ret.svalue = svalue;
        }
        return ret;
    }

    private List<String> getAttributeForContext(SnapContext context, Criterions searchCriterions) {
        List<String> attributesList = null;
        if (context != null && manager != null) {
            try {
                AttributeHeavy[] findContextAttributes = manager.findContextAttributes(context, searchCriterions);
                if (findContextAttributes != null && findContextAttributes.length != 0) {
                    attributesList = new ArrayList<>();
                    for (AttributeHeavy att : findContextAttributes) {
                        attributesList.add(att.getAttributeCompleteName());
                    }
                }
            } catch (Exception e) {
                // Leave the attributeList empty
                // TODO trace the error
            }
        }
        return attributesList;
    }

    @Override
    public List<String> getAttributeForContext(int contextId) throws DevFailed {
        List<String> attributeForContextList = null;
        Criterions criterions = new Criterions();
        Map<Integer, SnapContext> allSnapContext = getAllSnapContext();
        Set<Integer> keySet = allSnapContext.keySet();
        for (int id : keySet) {
            if (id == contextId) {
                SnapContext snapContexts = allSnapContext.get(contextId);
                attributeForContextList = getAttributeForContext(snapContexts, criterions);
            }
        }
        return attributeForContextList;
    }

    private List<SnapContext> getSnapContextListForAttribute(String argin) throws DevFailed {
        Criterions criterions = new Criterions();
        List<String> attributeForContext = null;
        List<SnapContext> contextList = new ArrayList<>();
        Map<Integer, SnapContext> mapContextList = new HashMap<>();

        Map<Integer, SnapContext> allSnapContext = getAllSnapContext();
        for (Entry<Integer, SnapContext> entry : allSnapContext.entrySet()) {
            Integer id = entry.getKey();
            SnapContext snapContext = entry.getValue();
            if (snapContext != null) {
                attributeForContext = getAttributeForContext(snapContext, criterions);
                if (attributeForContext != null) {
                    for (String attributeName : attributeForContext) {
                        if (attributeName.contains(argin)) {
                            mapContextList.put(id, snapContext);
                            Collection<SnapContext> values = mapContextList.values();
                            for (SnapContext snap : values) {
                                if (!contextList.contains(snap)) {
                                    contextList.add(snap);
                                }
                            }
                        }
                    }
                }
            }
        }
        return contextList;
    }

    @Override
    public DevVarLongStringArray getContextListForAttribute(String argin) throws DevFailed {
        List<SnapContext> snapContextListForAttribute = getSnapContextListForAttribute(argin);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        DevVarLongStringArray ret = new DevVarLongStringArray();
        SnapContext[] arrayList = new SnapContext[] {};

        int contextId = 0;

        if (snapContextListForAttribute != null && !snapContextListForAttribute.isEmpty()) {
            arrayList = snapContextListForAttribute.toArray(new SnapContext[snapContextListForAttribute.size()]);

            if (arrayList != null) {
                int numberOfContexts = arrayList.length;
                int[] lvalue = new int[numberOfContexts];
                String[] svalue = new String[numberOfContexts];

                for (int i = 0; i < numberOfContexts; i++) {
                    SnapContext currentContext = arrayList[i];
                    contextId = currentContext.getId();
                    lvalue[i] = contextId;

                    svalue[i] = currentContext.getName() + SPLIT_CHAR + currentContext.getAuthor_name() + SPLIT_CHAR
                            + sdf.format(currentContext.getCreation_date()) + SPLIT_CHAR + currentContext.getReason()
                            + SPLIT_CHAR + currentContext.getDescription();
                }
                ret.lvalue = lvalue;
                ret.svalue = svalue;
            }
        }
        return ret;
    }

    @Override
    public List<String> getSnapshotAttributeList() throws DevFailed {
        List<String> snapshotAttributeList = new ArrayList<>();
        Map<Integer, SnapContext> allSnapContext = getAllSnapContext();
        if (allSnapContext != null && !allSnapContext.isEmpty()) {
            Collection<SnapContext> values = allSnapContext.values();
            List<String> attributeForContext = null;
            Criterions criterions = new Criterions();
            for (SnapContext ctxt : values) {
                attributeForContext = getAttributeForContext(ctxt, criterions);
                if (attributeForContext != null) {
                    for (String attributeName : attributeForContext) {
                        if (!snapshotAttributeList.contains(attributeName.toLowerCase())) {
                            snapshotAttributeList.add(attributeName.toLowerCase());
                        }
                    }
                }
            }
        }
        return snapshotAttributeList;
    }

}
