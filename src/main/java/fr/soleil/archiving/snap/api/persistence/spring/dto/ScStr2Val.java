package fr.soleil.archiving.snap.api.persistence.spring.dto;

import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.persistance.AnyAttribute;
import fr.soleil.archiving.snap.api.persistence.context.SnapshotPersistenceContext;

/**
 * 2 string values.
 * 
 * @author CLAISSE
 */
public class ScStr2Val extends Val {
    private String readValue;
    private String writeValue;

    public ScStr2Val() {

    }

    public ScStr2Val(AnyAttribute attribute, SnapshotPersistenceContext context) {
        super(attribute, context);

        String[] val = attribute.getConvertedStringValuesTable();
        if (val == null) {
            readValue = null;
            writeValue = null;
        } else {
            readValue = val.length < 1 ? null : val[0];
            writeValue = val.length < 2 ? null : val[1];
        }
    }

    /**
     * @return the readValue
     */
    public String getReadValue() {
        return readValue;
    }

    /**
     * @param readValue
     *            the readValue to set
     */
    public void setReadValue(String readValue) {
        this.readValue = readValue;
    }

    /**
     * @return the writeValue
     */
    public String getWriteValue() {
        return writeValue;
    }

    /**
     * @param writeValue
     *            the writeValue to set
     */
    public void setWriteValue(String writeValue) {
        this.writeValue = writeValue;
    }
}
