package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.soleil.archiving.snap.api.persistence.spring.dto.CompositeId;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Val;

public abstract class AbstractValDAO<V extends Val> implements ValDAO<V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractValDAO.class);
	@Autowired
	SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public AbstractValDAO() {

	}

	public AbstractValDAO(final SessionFactory sessionFactory) {

    }

    @Override
    public V create(final V line) {
        LOGGER.debug("saving hibernate " + line);
        try {
			getCurrentSession().save(line);
        } catch (Exception e) {
            LOGGER.error("hibernate save error", e);
        }
        // throw new RuntimeException(); Uncomment to test correct rollback
        // management
        return line;
    }

    @Override
    public V findByKey(final CompositeId compositeId) {
        V line = null;
        try {
            final Class<V> valueClass = this.getValueClass();
			final Object res = getCurrentSession().get(valueClass, compositeId);
            line = valueClass.cast(res);
        } catch (Exception e) {
            LOGGER.error("hibernate find error", e);
        }
        return line;
    }

	@Override
	public void delete(final V item) {
		try {
			getCurrentSession().delete(item);
		} catch (Exception e) {
			LOGGER.error("hibernate find error", e);
		}
	}

    protected abstract Class<V> getValueClass();
}
