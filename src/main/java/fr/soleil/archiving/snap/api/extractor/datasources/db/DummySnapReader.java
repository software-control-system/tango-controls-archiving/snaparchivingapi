/*
 * Synchrotron Soleil
 * 
 * File : DummySnapReader.java
 * 
 * Project : snapExtractorAPI
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 23 janv. 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: DummySnapReader.java,v
 */
/*
 * Created on 23 janv. 2006
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.snap.api.extractor.datasources.db;

import java.util.List;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;

/**
 * A dummy implementation
 * 
 * @author CLAISSE
 */
public class DummySnapReader implements ISnapReader {
    DummySnapReader() {
        super();
        // TODO Auto-generated constructor stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see snapextractor.api.datasources.db.ISnapReader#openConnection()
     */
    @Override
    public void openConnection() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see snapextractor.api.datasources.db.ISnapReader#closeConnection()
     */
    @Override
    public void closeConnection() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * snapextractor.api.datasources.db.ISnapReader#getSnap(java.lang.String[])
     */
    @Override
    public SnapAttributeExtract[] getSnap(final int id) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * snapextractor.api.datasources.db.ISnapReader#getSnapshotsForContext(int)
     */
    @Override
    public DevVarLongStringArray getSnapshotsForContext(final int contextId) {
        // TODO Auto-generated method stub
        return null;
    }

    public void openConnection(final String string, final String string2) throws DevFailed {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getSnapshotsID(final int ctxID, final String[] criterions) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SnapAttributeExtract[] getSnapValues(final int id, final String... attributeName) throws DevFailed {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DevVarLongStringArray getAllContexts() throws DevFailed {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getAttributeForContext(int contextId) throws DevFailed {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DevVarLongStringArray getContextListForAttribute(String argin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getSnapshotAttributeList() {
        // TODO Auto-generated method stub
        return null;
    }

}
