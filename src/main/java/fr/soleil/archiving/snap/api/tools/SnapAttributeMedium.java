//+======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoSnapshoting/SnapshotingTools/Tools/SnapAttributeMedium.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SnapAttributeMedium.
//						(Chinkumo Jean) - Mar 24, 2004
//
// $Author: chinkumo $
//
// $Revision: 1.2 $
//
// $Log: SnapAttributeMedium.java,v $
// Revision 1.2  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.1.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.1  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.snap.api.tools;

import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.lib.project.ObjectUtils;

/**
 * SnapAttributeMedium
 * 
 * @author chinkumo
 */
public class SnapAttributeMedium extends AttributeLight {
    private int id_snap = -1; // Identifier for this snapshot
    private java.sql.Timestamp snap_date = null; // Timestamp asociated to this

    // snapshot

    public SnapAttributeMedium(String attribute_complete_name, int data_type, int data_format, int writable,
            int id_context, int id_snap, java.sql.Timestamp snap_date) {
        setAttributeCompleteName(attribute_complete_name);
        setDataType(data_type);
        setDataFormat(data_format);
        setWritable(writable);

        this.id_snap = id_snap;
        this.snap_date = snap_date;
    }

    public SnapAttributeMedium(AttributeLight snapAttributeLight, int id_context, int id_snap,
            java.sql.Timestamp snap_date) {
        setAttributeCompleteName(snapAttributeLight.getAttributeCompleteName());
        setAttributeId(snapAttributeLight.getAttributeId());
        setDataType(snapAttributeLight.getDataType());
        setDataFormat(snapAttributeLight.getDataFormat());
        setWritable(snapAttributeLight.getWritable());

        this.id_snap = id_snap;
        this.snap_date = snap_date;
    }

    public int getId_snap() {
        return id_snap;
    }

    public void setId_snap(int id_snap) {
        this.id_snap = id_snap;
    }

    public java.sql.Timestamp getSnap_date() {
        return snap_date;
    }

    public void setSnap_date(java.sql.Timestamp snap_date) {
        this.snap_date = snap_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof SnapAttributeMedium))
            return false;

        final SnapAttributeMedium snapAttributeMedium = (SnapAttributeMedium) o;

        if (getDataFormat() != snapAttributeMedium.getDataFormat())
            return false;
        if (getDataType() != snapAttributeMedium.getDataType())
            return false;
        if (getAttributeId() != snapAttributeMedium.getAttributeId())
            return false;
        if (id_snap != snapAttributeMedium.id_snap)
            return false;
        if (getWritable() != snapAttributeMedium.getWritable())
            return false;
        if (!getAttributeCompleteName().equals(snapAttributeMedium.getAttributeCompleteName()))
            return false;
        if (!snap_date.equals(snapAttributeMedium.snap_date))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = getAttributeCompleteName().hashCode();
        result = 29 * result + getAttributeId();
        result = 29 * result + id_snap;
        result = 29 * result + snap_date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String snapString = ObjectUtils.EMPTY_STRING;
        snapString = "Attribut : " + getAttributeCompleteName() + "\r\n" + "\t" + "Attribute Id : \t" + getAttributeId()
                + "\r\n" + "\t" + "data_type : \t" + getDataType() + "\r\n" + "\t" + "data_format : \t"
                + getDataFormat() + "\r\n" + "\t" + "writable : \t" + getWritable() + "\r\n" + "\t" + "Snapshot Id : \t"
                + id_snap + "\r\n" + "\t" + "SnapShot time : \t" + snap_date.toString() + "\r\n";
        return snapString;
    }

}
