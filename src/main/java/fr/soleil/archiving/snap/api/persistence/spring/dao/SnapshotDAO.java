package fr.soleil.archiving.snap.api.persistence.spring.dao;

import fr.soleil.archiving.snap.api.model.Snapshot;

public interface SnapshotDAO extends AbstractHibernateDAO<Snapshot> {

}
