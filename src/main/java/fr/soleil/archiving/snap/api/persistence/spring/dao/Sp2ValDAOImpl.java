package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.Sp2Val;

@Service("sp2ValDAO")
public class Sp2ValDAOImpl extends AbstractValDAO<Sp2Val> {
	public Sp2ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<Sp2Val> getValueClass() {
		return Sp2Val.class;
	}
}
