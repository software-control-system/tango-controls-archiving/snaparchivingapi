package fr.soleil.archiving.snap.api;

import fr.soleil.lib.project.ObjectUtils;

/**
 * <B>File</B> : ConfigConst.java<br/>
 * <B>Project</B> : Configuration java classes (hdbconfig package)<br/>
 * <B>Description</B> : This file contains all the constants and functions used
 * by all other classes of the package<br/>
 * 
 * @author chinkumo, ounsy
 */
public class ConfigConst {
    /*
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * DataBase defaults parameters
     * ||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     */

    // ----------------------------------- >> Historical DataBase
    /**
     * Parameter that represents the default database host
     */
    public static final String DEFAULT_S_HOST = "localhost";
    /**
     * Parameter that represents the default database names
     */
    public static final String DEFAULT_S_BD = "snap";
    /**
     * Parameter that represents the default database's schema name
     */
    public static final String DEFAULT_S_SCHEMA = "snap"; //
    /**
     * Parameter that represents the default database manager user id
     * (operators...)
     */
    public static final String DEFAULT_SM_USER = "snap"; // "manager";
    /**
     * Parameter that represents the default database manager user password
     */
    public static final String DEFAULT_SM_PASSWD = "snap"; // "manager";
    /**
     * Parameter that represents the default database archiver user id
     * (archivers...)
     */
    public static final String DEFAULT_SA_USER = "archiver";
    /**
     * Parameter that represents the default database archiver user password
     */
    public static final String DEFAULT_SA_PASSWD = "archiver";
    /**
     * Parameter that represents the default database archiver user password
     */
    public static final String DEFAULT_SA_BEANS_FILE_NAME = "beans.xml";
    /**
     * Parameter that represents the default database browser user id
     */
    public static final String DEFAULT_SB_USER = "browser";
    /**
     * Parameter that represents the default database browser user password (for
     * the default user...)
     */
    public static final String DEFAULT_SB_PASSWD = "browser";

    // ----------------------------------- >> DataBase Type
    /**
     * Parameter that represents the MySQL database type
     */
    public static final int BD_MYSQL = 0;
    /**
     * Parameter that represents the Oracle database type
     */
    public static final int BD_ORACLE = 1;
    /**
     * Parameter that represents the PostGreSQL database type
     */
    public static final int BD_POSTGRESQL = 2;

    // ----------------------------------- >> Drivers Types
    /**
     * Parameter that represents the MySQL database driver
     */
    public static final String DRIVER_MYSQL = "jdbc:mysql";
    /**
     * Parameter that represents the Oracle database driver
     */
    public static final String DRIVER_ORACLE = "jdbc:oracle:thin";
    /**
     * Parameter that represents the PostGreSQL database driver
     */
    // TODO Give a value to the PostGres JDBC driver
    public static final String DRIVER_POSTGRESQL = ObjectUtils.EMPTY_STRING;
    /**
     * Port number for the connection
     */
    public final static String ORACLE_PORT = "1521";

}
