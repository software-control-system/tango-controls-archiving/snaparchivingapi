package fr.soleil.archiving.snap.api.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.lib.project.ObjectUtils;

/**
 * <p/>
 * <B>Description :</B><BR>
 * The <I>SnapContext</I> object describes the context of a snapshot. To have a meaning, a snapshot must be accompanied
 * with a certain number of informations. This information is thus included in this object.<BR>
 * An <I>SnapContext</I> contains :
 * <ul>
 * <li>the context <I>author's name</I>,
 * <li>the context <I>name</I>
 * <li>the context <I>identifier</I>
 * <li>the context <I>creation date</I>
 * <li>the context <I>reason</I>
 * <li>the context <I>description</I>
 * <li>the <I>list of attributes</I> that are included in the context.
 * </ul>
 * <B>Date :<B> Jan 22, 2004
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version 1.0
 * @see fr.soleil.archiving.snap.api.tools.SnapAttributeHeavy
 */
public class SnapContext {
    private String author_name = ObjectUtils.EMPTY_STRING;
    private String name = ObjectUtils.EMPTY_STRING;
    private int id = 0;
    private Date creation_date;
    private String reason = ObjectUtils.EMPTY_STRING;
    private String description = ObjectUtils.EMPTY_STRING;
    private List<AttributeLight> attributeList = new ArrayList<AttributeLight>();

    /**
     * Default constructor Creates a new instance of SnapAttributeHeavy
     * 
     * @see #SnapContext(String author_name, String name, java.sql.Date creation_date)
     * @see #SnapContext(String author_name, String name, int id, java.sql.Date creation_date, String reason, String
     *      description)
     */
    public SnapContext() {
    }

    /**
     * This constructor takes several parameters as inputs.
     * 
     * @param author_name the context <I>author's name</I>
     * @param name the context <I>name</I>
     * @param creation_date the context <I>creation date</I>
     * @see #SnapContext()
     * @see #SnapContext(String author_name, String name, Date creation_date)
     * @see #SnapContext(String... argin)
     */
    public SnapContext(final String author_name, final String name, final Date creation_date) {
        this.author_name = author_name;
        this.name = name;
        this.creation_date = creation_date;
    }

    /**
     * This constructor takes several parameters as inputs.
     * 
     * @param author_name the context <I>author's name</I>
     * @param name the context <I>name</I>
     * @param id the context <I>identifier</I>
     * @param creation_date the context <I>creation date</I>
     * @param reason the context <I>reason</I>
     * @param description the context <I>description</I>
     * @see #SnapContext()
     * @see #SnapContext(String author_name, String name, java.sql.Date creation_date)
     * @see #SnapContext(String... argin)
     */
    public SnapContext(final String author_name, final String name, final int id, final Date creation_date,
            final String reason, final String description) {
        this.author_name = author_name;
        this.name = name;
        this.id = id;
        this.creation_date = creation_date;
        this.reason = reason;
        this.description = description;
    }

    /**
     * This constructor takes several parameters as inputs.
     * 
     * @param author_name the context <I>author's name</I>
     * @param name the context <I>name</I>
     * @param id the context <I>identifier</I>
     * @param creation_date the context <I>creation date</I>
     * @param reason the context <I>reason</I>
     * @param description the context <I>description</I>
     * @param attributeList the <I>list of attributes</I> that are included in the context.
     * @see #SnapContext()
     * @see #SnapContext(String author_name, String name, Date creation_date)
     * @see #SnapContext(String author_name, String name, int id, Date creation_date, String reason, String description,
     *      ArrayList attributeList)
     * @see #SnapContext(String... argin)
     */
    public SnapContext(final String author_name, final String name, final int id, final Date creation_date,
            final String reason, final String description, final List<AttributeLight> attributeList) {
        this.author_name = author_name;
        this.name = name;
        this.id = id;
        this.creation_date = creation_date;
        this.reason = reason;
        this.description = description;
        this.attributeList = attributeList;
    }

    /**
     * This constructor builds an SnapContext from an array
     * 
     * @param argin an array that contains the SnapContext's author's name, name, identifier, creation date, reason,
     *            description and, the <I>list of attributes</I> that are included in the context.
     */
    public SnapContext(final String... argin) throws SnapshotingException {
        if (argin.length < 6) {
            throw new SnapshotingException("argin size must be at least 6: " + Arrays.toString(argin));
        }
        setAuthor_name(argin[0]);
        setName(argin[1]);
        setId(Integer.parseInt(argin[2]));
        setCreation_date(java.sql.Date.valueOf(argin[3]));
        setReason(argin[4]);
        setDescription(argin[5]);

        // Attribute list construction
        for (int i = 6; i < argin.length; i++) {
            attributeList.add(new AttributeLight(argin[i]));
        }
    }

    /**
     * Returns the SnapContext's <I>author's name</I>.
     * 
     * @return the SnapContext's <I>author's name</I>.
     */
    public String getAuthor_name() {
        return author_name;
    }

    /**
     * Sets the context <I>author's name</I>.
     * 
     * @param author_name
     *            the context <I>author's name</I>.
     */
    public void setAuthor_name(final String author_name) {
        this.author_name = author_name;
    }

    /**
     * Returns the SnapContext's <I>name</I>.
     * 
     * @return the SnapContext's <I>name</I>.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the context <I>name</I>.
     * 
     * @param name
     *            the context <I>name</I>.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Returns the SnapContext's <I>identifier</I>.
     * 
     * @return the SnapContext's <I>identifier</I>.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the context <I>identifier</I>.
     * 
     * @param id
     *            the context <I>identifier</I>.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Returns the SnapContext's <I>creation date</I>.
     * 
     * @return the SnapContext's <I>creation date</I>.
     */
    public Date getCreation_date() {
        return creation_date;
    }

    /**
     * Sets the context <I>creation date</I>.
     * 
     * @param creation_date
     *            the context <I>creation date</I>.
     */
    public void setCreation_date(final Date creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * Returns the SnapContext's <I>reason</I>.
     * 
     * @return the SnapContext's <I>reason</I>.
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the context <I>reason</I>.
     * 
     * @param reason
     *            the context <I>reason</I>.
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * Returns the SnapContext's <I>description</I>.
     * 
     * @return the SnapContext's <I>description</I>.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the context <I>description</I>.
     * 
     * @param description
     *            the context <I>description</I>.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Returns the <I>list of attributes</I> that are included in the context.
     * 
     * @return the <I>list of attributes</I> that are included in the context.
     */
    public List<AttributeLight> getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the SnapContext's <I>list of attributes</I> included in the context.
     * 
     * @param attributeList
     *            the <I>list of attributes</I> that are included in the
     *            context.
     */
    public void setAttributeList(final List<AttributeLight> attributeList) {
        this.attributeList = attributeList;
    }

    /**
     * Returns an array representation of the object <I>SnapContext</I>. In this
     * order, array fields are :
     * <ol>
     * <li>the context <I>author's name</I>,
     * <li>the context <I>name</I>
     * <li>the context <I>identifier</I>
     * <li>the context <I>creation date</I>
     * <li>the context <I>reason</I>
     * <li>the context <I>description</I>
     * <li>The suite table describes the context's <I>list of attributes</I>
     * </ol>
     * 
     * @return an array representation of the object <I>SnapContext</I>.
     */
    public String[] toArray() {
        String[] snapContext;
        snapContext = new String[6 + attributeList.size()];

        snapContext[0] = author_name;
        snapContext[1] = name;
        snapContext[2] = Integer.toString(id);
        snapContext[3] = creation_date.toString();
        snapContext[4] = reason;
        snapContext[5] = description;

        for (int i = 0; i < attributeList.size(); i++) {
            snapContext[i + 6] = attributeList.get(i).getAttributeCompleteName();
        }

        return snapContext;
    }

    @Override
    public String toString() {
        String snapString = new String(ObjectUtils.EMPTY_STRING);
        snapString = "Context" + "\r\n" + "Id : \t" + id + "\r\n" + "Author name : \t" + author_name + "\r\n"
                + "Context name : \t" + name + "\r\n" + "Creation date : \t" + creation_date + "\r\n" + "Reason : \t"
                + reason + "\r\n" + "Description : \t" + description + "\r\n" + "Attribute(s) : " + "\r\n";
        if (attributeList != null) {
            for (int i = 0; i < attributeList.size(); i++) {
                snapString = snapString + "\t\t" + "[" + i + "]" + "\t" + attributeList.get(i) + "\r\n";
            }
        } else {
            snapString = snapString + "\t\t" + "NULL" + "\r\n";
        }
        return snapString;
    }

}
