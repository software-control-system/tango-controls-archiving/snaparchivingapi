package fr.soleil.archiving.snap.api.tools;

import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.lib.project.ObjectUtils;

public class SnapTool {

    private SnapTool() {
        // hide constructor
    }

    /**
     * Returns the SQL clause described by a Criterion in the case of a field of the ContextTable.
     * 
     * @param criterions The Criterion
     * @return SQL clause described by the Criterion in the case of a field of the ContextTable
     * @throws SnapshotingException
     */
    public static String getContextClause(Criterions criterions) throws SnapshotingException {
        String clause = ObjectUtils.EMPTY_STRING;
        if (criterions != null) {
            String table = SnapConst.CONTEXT;
            String[] field = SnapConst.TAB_CONTEXT;
            int count = 0;
            // Cas de l'ID_context.
            Condition[] conditions = criterions.getConditions(field[0]);
            if (conditions != null) {
                // Une seule condition sur l'id_context.
                if (conditions.length == 1) {
                    // Mise en forme du predicat de la requete SQL.
                    String predicat = getPredicatInt(conditions[0]);

                    // Mise en forme de la requete SQL.
                    if (count == 0) {
                        clause = clause + " WHERE " + table + "." + field[0] + predicat;
                        count++;
                    } else {
                        clause = clause + " AND " + table + "." + field[0] + predicat;
                    }
                } else {
                    String message = ObjectUtils.EMPTY_STRING;
                    message = SnapConst.ERROR_SQL_OPERATOR;
                    String reason = SnapConst.ERROR_SQL_OPERATOR;
                    String desc = "Failed while executing Criterions.getSnapshotClause() method...";
                    throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc,
                            criterions.getClass().getName());
                }
            }

            // Pour les autres champs.
            for (int i = 1; i < field.length; i++) {
                // Parcours de tous les champs de la table des contextes, sauf l'ID_context.
                conditions = criterions.getConditions(field[i]);
                if (conditions != null) {
                    for (int j = 0; j < conditions.length; j++) {
                        // Mise en forme du predicat de la requete SQL.
                        String predicat = getPredicat(conditions[j]);

                        // Mise en forme de la requete SQL.
                        if (count == 0) {
                            clause = clause + " WHERE " + table + "." + field[i] + predicat;
                            count++;
                        } else {
                            clause = clause + " AND " + table + "." + field[i] + predicat;
                        }
                    }
                }

            }
        }
        return clause;
    }

    /**
     * Returns the SQL clause described by a Criterion in the case of a field of the SnapshotTable.
     * 
     * @param criterions The Criterion
     * @return SQL clause described by the Criterion in the case of a field of the SnashotTable
     * @throws SnapshotingException
     */
    public static String getSnapshotClause(Criterions criterions) throws SnapshotingException {
        String clause = ObjectUtils.EMPTY_STRING;
        if (criterions != null) {
            String table = SnapConst.SNAPSHOT;
            String[] field = SnapConst.TAB_SNAP;
            int count = 0;
            // Cas de l'id_snap.
            Condition[] conditions = criterions.getConditions(field[0]);
            if (conditions != null) {
                if (conditions.length == 1) {
                    // Mise en forme du predicat de la requete SQL.
                    String predicat = getPredicatInt(conditions[0]);

                    // Mise en forme de la requete SQL.
                    if (count == 0) {
                        clause = clause + " WHERE " + table + "." + field[0] + predicat;
                        count++;
                    } else {
                        clause = clause + " AND " + table + "." + field[0] + predicat;
                    }
                } else {
                    String message = ObjectUtils.EMPTY_STRING;
                    message = SnapConst.ERROR_SQL_OPERATOR;
                    String reason = SnapConst.ERROR_SQL_OPERATOR;
                    String desc = "Failed while executing Criterions.getSnapshotClause() method...";
                    throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc,
                            criterions.getClass().getName());
                }

            }
            // Cas de l'ID_context.
            conditions = criterions.getConditions(field[1]);
            if (conditions != null) {
                // Une seule condition sur l'id_context.
                if (conditions.length == 1) {
                    // Mise en forme du predicat de la requete SQL.
                    String predicat = getPredicatInt(conditions[0]);

                    // Mise en forme de la requete SQL.
                    if (count == 0) {
                        clause = clause + " WHERE " + table + "." + field[1] + predicat;
                        count++;
                    } else {
                        clause = clause + " AND " + table + "." + field[1] + predicat;
                    }
                } else {
                    String message = ObjectUtils.EMPTY_STRING;
                    message = SnapConst.ERROR_SQL_OPERATOR;
                    String reason = SnapConst.ERROR_SQL_OPERATOR;
                    String desc = "Failed while executing Criterions.getSnapshotClause() method...";
                    throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc,
                            criterions.getClass().getName());
                }

            }

            for (int i = 2; i < field.length; i++) {
                // Parcours de tous les champs de la table des contextes.
                conditions = criterions.getConditions(field[i]);
                if (conditions != null) {
                    for (int j = 0; j < conditions.length; j++) {
                        // Mise en forme du predicat de la requete SQL.
                        String predicat = getPredicat(conditions[j]);
                        // Mise en forme de la requete SQL.
                        if (count == 0) {
                            clause = clause + " WHERE " + table + "." + field[i] + predicat;
                            count++;
                        } else {
                            clause = clause + " AND " + table + "." + field[i] + predicat;
                        }
                    }
                }
            }
        }
        return clause;
    }

    /**
     * Returns the id of a context of the SnapTable.
     * 
     * @param criterions The Criterion to recover the context
     * @return id of a context of the SnapTable
     * @throws SnapshotingException
     */
    public static int getIdContextSnapTable(Criterions criterions) throws SnapshotingException {
        int id_context = -1; // value if no condition on this id.
        if (criterions != null) {
            Condition[] id_condition = criterions.getConditions(SnapConst.TAB_SNAP[1]);
            if (id_condition != null) {
                id_context = Integer.parseInt(id_condition[0].getValue());
            }
        }
        return id_context;
    }

    /**
     * Returns the id of a context.
     * 
     * @param criterions The Criterion to recover the context
     * @return the id of a context
     * @throws SnapshotingException
     */
    public static int getIdContextContextTable(Criterions criterions) throws SnapshotingException {
        int id_context = -1; // value if no condition on this id.
        if (criterions != null) {
            Condition[] id_condition = criterions.getConditions(SnapConst.ID_CONTEXT);
            if (id_condition != null) {
                id_context = Integer.parseInt(id_condition[0].getValue());
            }
        }
        return id_context;
    }

    /**
     * Returns the id of a snapshot.
     * 
     * @param criterions The Criterion to recover the snapshot
     * @return the id of a snapshot
     * @throws SnapshotingException
     */
    public static int getIdSnap(Criterions criterions) throws SnapshotingException {
        int id_snap = -1; // value if no condition on this id.
        if (criterions != null) {
            Condition[] id_condition = criterions.getConditions(SnapConst.ID_SNAP);
            if (id_condition != null) {
                id_snap = Integer.parseInt(id_condition[0].getValue());
            }
        }
        return id_snap;
    }

    /**
     * Returns the SQL clause described by a Criterion in the case of a field of the AttribteTable.
     * 
     * @param criterions The Criterion
     * @return SQL clause described by the Criterion in the case of a field of the AttributeTable
     * @throws SnapshotingException
     */
    public static String getAttributeClause(Criterions criterions) throws SnapshotingException {
        String clause = ObjectUtils.EMPTY_STRING;
        if (criterions != null) {
            String table = SnapConst.AST;
            String[] field = SnapConst.TAB_DEF;
            int count = 0;
            for (int i = 4; i < 8; i++) {
                // Parcours des champs de la table des definitions.
                Condition[] conditions = criterions.getConditions(field[i]);
                if (conditions != null) {
                    for (int j = 0; j < conditions.length; j++) {
                        // Mise en forme du predicat de la requete SQL.
                        String predicat = getPredicatFullName(conditions[j]);
                        // Mise en forme de la requete SQL.
                        if (!predicat.isEmpty()) {
                            if (count == 0) {
                                clause = clause + " WHERE " + table + "." + field[i] + predicat;
                                count++;
                            } else {
                                clause = clause + " AND " + table + "." + field[i] + predicat;
                            }
                        }
                    }
                }
            }
        }
        return clause;
    }

    private static String getNotNullString(String value) {
        return value == null ? ObjectUtils.EMPTY_STRING : value;
    }

    /**
     * Returns the predicat of the SQL request for a condition.
     * 
     * @param condition The condition
     * @return Predicat of the SQL request for the condition
     * @throws SnapshotingException
     */
    public static String getPredicat(Condition condition) throws SnapshotingException {
        String predicat = ObjectUtils.EMPTY_STRING;
        if (condition != null) {
            // Casts the Condition's operator and value in a SQL predicat.
            String operator = getNotNullString(condition.getOperator());
            switch (operator) {
                case SnapConst.OP_EQUALS:
                    predicat = " = '" + condition.getValue() + "'";
                    break;
                case SnapConst.OP_LOWER_THAN_STRICT:
                    predicat = " < '" + condition.getValue() + "'";
                    break;
                case SnapConst.OP_LOWER_THAN:
                    predicat = " <= '" + condition.getValue() + "'";
                    break;
                case SnapConst.OP_GREATER_THAN_STRICT:
                    predicat = " > '" + condition.getValue() + "'";
                    break;
                case SnapConst.OP_GREATER_THAN:
                    predicat = " >= '" + condition.getValue() + "'";
                    break;
                case SnapConst.OP_CONTAINS:
                    predicat = " LIKE '%" + condition.getValue() + "%'";
                    break;
                case SnapConst.OP_STARTS_WITH:
                    predicat = " LIKE '" + condition.getValue() + "%'";
                    break;
                case SnapConst.OP_ENDS_WITH:
                    predicat = " LIKE '%" + condition.getValue() + "'";
                    break;
                default:
                    String message = ObjectUtils.EMPTY_STRING;
                    message = SnapConst.ERROR_SQL_OPERATOR;
                    String reason = SnapConst.ERROR_SQL_OPERATOR;
                    String desc = "Failed while executing DataBaseApi.getPredicat() method...";
                    throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc,
                            condition.getClass().getName());
            }
        }
        return predicat;
    }

    /**
     * Returns the predicat of the SQL request for a condition in the case of an integer value.
     * 
     * @param condition The condition
     * @return Predicat of the SQL request for the condition in the case of an integer value
     * @throws SnapshotingException
     */
    public static String getPredicatInt(Condition condition) throws SnapshotingException {
        String predicat = ObjectUtils.EMPTY_STRING;
        if (condition != null) {
            // Casts the Condition's operator and value in a SQL predicat.
            String operator = getNotNullString(condition.getOperator());
            switch (operator) {
                case SnapConst.OP_EQUALS:
                    predicat = " = ?";
                    break;
                case SnapConst.OP_LOWER_THAN_STRICT:
                    predicat = " < ?";
                    break;
                case SnapConst.OP_LOWER_THAN:
                    predicat = " <= ?";
                    break;
                case SnapConst.OP_GREATER_THAN_STRICT:
                    predicat = " > ?";
                    break;
                case SnapConst.OP_GREATER_THAN:
                    predicat = " >= ?";
                    break;
                default:
                    String message = ObjectUtils.EMPTY_STRING;
                    message = SnapConst.ERROR_SQL_OPERATOR;
                    String reason = SnapConst.ERROR_SQL_OPERATOR;
                    String desc = "Failed while executing DataBaseApi.getPredicatInt() method...";
                    throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc,
                            condition.getClass().getName());
            }
        }
        return predicat;
    }

    /**
     * Returns the predicat of the SQL request for a condition in the case of the full name of an attribute.
     * 
     * @param condition The condition
     * @return Predicat of the SQL request for the condition in the case of the full name of an attribute
     * @throws SnapshotingException
     */
    public static String getPredicatFullName(Condition condition) throws SnapshotingException {
        String predicat = ObjectUtils.EMPTY_STRING;
        if (condition != null) {
            // Casts the Condition's operator and value in a SQL predicat.
            String value = getNotNullString(condition.getValue());
            if (value.equals("*")) {
                predicat = ObjectUtils.EMPTY_STRING;
            } else if (value.startsWith("*") && value.endsWith("*")) {
                predicat = " LIKE '%" + value.substring(1, value.length() - 1) + "%'";
            } else if (value.endsWith("*")) {
                predicat = " LIKE '" + value.substring(0, value.length() - 1) + "%'";
            } else if (value.startsWith("*")) {
                predicat = " LIKE '%" + value.substring(1) + "'";
            } else if (!value.isEmpty()) {
                predicat = " = '" + value + "'";
            } else {
                String message = ObjectUtils.EMPTY_STRING;
                message = SnapConst.ERROR_SQL_OPERATOR;
                String reason = SnapConst.ERROR_SQL_OPERATOR;
                String desc = "Failed while executing DataBaseApi.getPredicatFullName() method...";
                throw new SnapshotingException(message, reason, ErrSeverity.WARN, desc, condition.getClass().getName());
            }
        }
        return predicat;
    }

}
