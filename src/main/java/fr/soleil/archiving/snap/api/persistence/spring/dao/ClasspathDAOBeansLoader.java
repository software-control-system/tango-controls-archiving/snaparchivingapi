package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.soleil.archiving.snap.api.persistence.spring.dto.Im1Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Im2Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScNum1Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScNum2Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr1Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.ScStr2Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Sp1Val;
import fr.soleil.archiving.snap.api.persistence.spring.dto.Sp2Val;

public class ClasspathDAOBeansLoader implements DAOBeansLoader {
    private static final XLogger LOGGER = XLoggerFactory.getXLogger(ClasspathDAOBeansLoader.class);
    private static final String DEFAULT_BEANS_FILE_NAME = "beans.xml";
    private final String resourceName;
    private final ApplicationContext applicationContext;

    private ValDAO<ScNum1Val> scNum1ValDAO;
    private ValDAO<ScNum2Val> scNum2ValDAO;
    private ValDAO<ScStr1Val> scStr1ValDAO;
    private ValDAO<ScStr2Val> scStr2ValDAO;
    private ValDAO<Sp1Val> sp1ValDAO;
    private ValDAO<Sp2Val> sp2ValDAO;
    private ValDAO<Im1Val> im1ValDAO;
    private ValDAO<Im2Val> im2ValDAO;

    public ClasspathDAOBeansLoader(String _resourceName, String userName, String password) {
        LOGGER.entry(_resourceName, userName, password);
        boolean defaultResource = _resourceName == null || _resourceName.trim().length() == 0;
        this.resourceName = defaultResource ? DEFAULT_BEANS_FILE_NAME : _resourceName;
        LOGGER.info("configure hibernate resource: " + resourceName);
        System.setProperty("user", userName);
        System.setProperty("password", password);
        applicationContext = new ClassPathXmlApplicationContext(this.resourceName);
        instantiateBeans(applicationContext);
        LOGGER.exit();
    }

    @SuppressWarnings("unchecked")
    private void instantiateBeans(BeanFactory ctx) {
        LOGGER.entry(ctx);
        scNum1ValDAO = (ValDAO<ScNum1Val>) ctx.getBean("scNum1ValDAO");
        scNum2ValDAO = (ValDAO<ScNum2Val>) ctx.getBean("scNum2ValDAO");
        scStr1ValDAO = (ValDAO<ScStr1Val>) ctx.getBean("scStr1ValDAO");
        scStr2ValDAO = (ValDAO<ScStr2Val>) ctx.getBean("scStr2ValDAO");
        sp1ValDAO = (ValDAO<Sp1Val>) ctx.getBean("sp1ValDAO");
        sp2ValDAO = (ValDAO<Sp2Val>) ctx.getBean("sp2ValDAO");
        im1ValDAO = (ValDAO<Im1Val>) ctx.getBean("im1ValDAO");
        im2ValDAO = (ValDAO<Im2Val>) ctx.getBean("im2ValDAO");
        LOGGER.exit();
    }

    /**
     * @return the im1ValDAO
     */
    @Override
    public ValDAO<Im1Val> getIm1ValDAO() {
        return this.im1ValDAO;
    }

    /**
     * @return the im2ValDAO
     */
    @Override
    public ValDAO<Im2Val> getIm2ValDAO() {
        return this.im2ValDAO;
    }

    /**
     * @return the scNum1ValDAO
     */
    @Override
    public ValDAO<ScNum1Val> getScNum1ValDAO() {
        return this.scNum1ValDAO;
    }

    /**
     * @return the scNum2ValDAO
     */
    @Override
    public ValDAO<ScNum2Val> getScNum2ValDAO() {
        return this.scNum2ValDAO;
    }

    /**
     * @return the scStr1ValDAO
     */
    @Override
    public ValDAO<ScStr1Val> getScStr1ValDAO() {
        return this.scStr1ValDAO;
    }

    /**
     * @return the scStr2ValDAO
     */
    @Override
    public ValDAO<ScStr2Val> getScStr2ValDAO() {
        return this.scStr2ValDAO;
    }

    /**
     * @return the sp1ValDAO
     */
    @Override
    public ValDAO<Sp1Val> getSp1ValDAO() {
        return this.sp1ValDAO;
    }

    /**
     * @return the sp2ValDAO
     */
    @Override
    public ValDAO<Sp2Val> getSp2ValDAO() {
        return this.sp2ValDAO;
    }

    @Override
    public String getResourceName() {
        return this.resourceName;
    }

    @Override
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
