package fr.soleil.archiving.snap.api.extractor.lifecycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.snap.api.extractor.convert.ConverterFactory;
import fr.soleil.archiving.snap.api.extractor.datasources.db.ISnapReader;
import fr.soleil.archiving.snap.api.extractor.datasources.db.SnapReaderFactory;
import fr.soleil.archiving.snap.api.extractor.devicelink.Warnable;
import fr.soleil.archiving.snap.api.extractor.naming.DynamicAttributeNamerFactory;

/**
 * The default implementation. Extends Thread so that it can run in a separate
 * thread at the request of the device
 * 
 * @author CLAISSE
 */
public class DefaultLifeCycleManager extends Thread implements LifeCycleManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLifeCycleManager.class);
    /**
     * A reference to the device that instantiated this, if the application is
     * running in device mode
     */
    protected Warnable watcherToWarn;
    private ISnapReader snapReader;

    DefaultLifeCycleManager() {
        this.setName("watcherThread");
    }

    @Override
    public synchronized void applicationWillStart() {
        LOGGER.info(".....INITIALIZING APPLICATION");

        try {
            startFactories();

            snapReader = SnapReaderFactory.getCurrentImpl();
            snapReader.openConnection();
        } catch (Throwable t) {
            t.printStackTrace();
            this.warnWatcherFault();
        }

        LOGGER.info(".....APPLICATION INITIALIZED");
    }

    /**
     * 5 juil. 2005
     */
    private void startFactories() {
        SnapReaderFactory.getImpl(SnapReaderFactory.REAL);
        ConverterFactory.getImpl(ConverterFactory.DEFAULT);
        DynamicAttributeNamerFactory.getImpl(DynamicAttributeNamerFactory.DEFAULT);
    }

    @Override
    public synchronized void applicationWillClose() {
        try {
            // begin do stuff
            LOGGER.info("Application will close !");

            snapReader = SnapReaderFactory.getCurrentImpl();
            snapReader.closeConnection();

            LOGGER.info("Application closed");
            // end do stuff
            System.exit(0);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void run() {
        try {
            this.applicationWillStart();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see archwatch.lifecycle.LifeCycleManager#getAsThread()
     */
    @Override
    public Thread getAsThread() {
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * archwatch.strategy.delay.IDelayManager#setWatcherToWarn(ArchivingWatcher
     * .ArchivingWatcher)
     */
    @Override
    public synchronized void setWatcherToWarn(Warnable _watcher) {
        this.watcherToWarn = _watcher;
    }

    /**
     * Warns the device it should go into Fault state
     */
    protected synchronized void warnWatcherFault() {
        if (this.watcherToWarn == null) {
            // standalone mode, do nothing
            return;
        }

        this.watcherToWarn.warnFault();
    }

    /**
     * Warns the device it should go into Alarm state
     */
    protected synchronized void warnWatcherAlarm() {
        if (this.watcherToWarn == null) {
            // standalone mode, do nothing
            return;
        }

        this.watcherToWarn.warnAlarm();
    }

    /**
     * Warns the device it should go into Init state
     */
    protected synchronized void warnWatcherInit() {
        if (this.watcherToWarn == null) {
            // standalone mode, do nothing
            return;
        }

        this.watcherToWarn.warnInit();
    }

    /**
     * Warns the device it should go into Off state
     */
    protected synchronized void warnWatcherOff() {
        if (this.watcherToWarn == null) {
            // standalone mode, do nothing
            return;
        }

        this.watcherToWarn.warnOff();
    }

    /*
     * (non-Javadoc)
     * 
     * @see snapextractor.api.devicelink.Warner#getWatcherToWarn()
     */
    @Override
    public Warnable getWatcherToWarn() {
        return this.watcherToWarn;
    }
}
