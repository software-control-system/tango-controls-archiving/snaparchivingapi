/*
 * Synchrotron Soleil
 * 
 * File : IArchivedAttributesReader.java
 * 
 * Project : archiving_watcher
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 28 nov. 2005
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: IArchivedAttributesReader.java,v
 */
/*
 * Created on 28 nov. 2005
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.snap.api.extractor.datasources.db;

import java.util.List;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;

/**
 * Contains data access methods used by for extracting snapshots
 * 
 * @author CLAISSE
 */
public interface ISnapReader {
    /**
     * Opens a connection to the Snap database
     * 
     * @throws DevFailed
     */
    public void openConnection() throws DevFailed;

    /**
     * Closes the connection to the Snap database
     */
    public void closeConnection();

    /**
     * Extracts a given snapshot's data
     * 
     * @param id
     *            The ID of the snapshot to extract
     * @return The snapshot's data
     * @throws DevFailed
     */
    public SnapAttributeExtract[] getSnap(int id) throws DevFailed;

    /**
     * Extracts values of a snapshot for an attributes
     * 
     * @param id
     * @param attributeName
     * @return
     * @throws DevFailed
     */
    public SnapAttributeExtract[] getSnapValues(int id, String... attributeNames) throws DevFailed;

    /**
     * Returns the list of snapshots associated to a given context
     * 
     * @param contextId
     *            The context's ID
     * @return A DevVarLongStringArray list where the Long elements are the
     *         snapshots' IDs, and the String elements are descriptions of each
     *         snapshot (typically, the concatenation of the snapshot's date and
     *         comment fields)
     * @throws DevFailed
     */
    public DevVarLongStringArray getSnapshotsForContext(int contextId) throws DevFailed;

    public int[] getSnapshotsID(int ctxID, String[] criterions);

    /**
     * Returns the list of all contexts
     * 
     * @return A DevVarLongStringArray list where the Long elements are the
     *         context' IDs, and the String elements are descriptions of each
     *         context (typically, the concatenation of the context's name, author name, creation date(dd/MM/yyyy),
     *         reason,
     *         description fields and the attributs of the context: id, name, dataFormat and dataType )
     *         Example: lvalue = (contextId)^contextNb
     *         svalue =
     *         (contextName|authorName|creationDate|reason|description|attributId1|attributName1|attributDataFormat1|
     *         attributDataType1|attributId2|attributName2|attributDataFormat2|attributDataType2...)^contextNb
     * @throws DevFailed
     * 
     */
    public DevVarLongStringArray getAllContexts() throws DevFailed;
    
    public List<String> getAttributeForContext(int contextId) throws DevFailed;
    
    public DevVarLongStringArray getContextListForAttribute(String argin) throws DevFailed;

    public List<String> getSnapshotAttributeList() throws DevFailed;
}
