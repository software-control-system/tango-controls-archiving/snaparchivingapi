package fr.soleil.archiving.snap.api.tools;

import java.util.ArrayList;
import java.util.List;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.soleil.lib.project.ObjectUtils;

/**
 * <p/>
 * <B>Description :</B><BR>
 * The <I>SnapShot</I> object describes a snapshot. A snapshot ... *
 * <ul>
 * <li>... is related to a context <I>(context ID)</I>,
 * <li>... has an identifier <I>(Snapshot ID)</I>
 * <li>... is triggered at a given <I>timestamp</I>
 * <li>... concerns a <I>list of attributes</I>
 * </ul>
 * <B>Date :<B> Mar 22, 2004
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version 1.0
 * @see SnapContext
 */
public class Snapshot {
    private int id_context = -1; // Identifier for the related context
    private int id_snap = -1; // Identifier for this snapshot
    private java.sql.Timestamp snap_date = null; // Timestamp asociated to this
    // snapshot
    private List<SnapAttributeExtract> attribute_List = new ArrayList<SnapAttributeExtract>();
    private boolean filtered = false;

    // this snapshot

    /**
     * Default constructor Creates a new instance of SnapShot
     * 
     * @see #SnapShot(int, java.sql.Timestamp)
     * @see #SnapShot(String[])
     */
    public Snapshot() {
    }

    /**
     * This constructor takes several parameters as inputs.
     * 
     * @param idContext
     *            the associated context identifier
     * @param snapDate
     *            the timestamp associated to this snapshot
     * @see #SnapShot()
     * @see #SnapShot(String[])
     */
    public Snapshot(int idContext, java.sql.Timestamp snapDate) {
        id_context = idContext;
        snap_date = snapDate;
    }

    /**
     * This constructor takes several parameters as inputs.
     * 
     * @param idContext
     *            the associated context identifier
     * @param snapDate
     *            the timestamp associated to this snapshot
     * @param attributeList
     *            the attribute list associated to this snapshot
     * @see #SnapShot()
     * @see #SnapShot(String[])
     */
    public Snapshot(int idContext, java.sql.Timestamp snapDate, List<SnapAttributeExtract> attributeList) {
        id_context = idContext;
        snap_date = snapDate;
        attribute_List = attributeList;
    }

    /**
     * This constructor builds an SnapContext from an array
     * 
     * @param argin
     *            an array that contains the SnapContext's author's name, name,
     *            identifier, creation date, reason, description and, the
     *            <I>list of attributes</I> that are included in the context.
     */
    public Snapshot(String[] argin) {
        setId_context(Integer.parseInt(argin[0]));
        setId_snap(Integer.parseInt(argin[1]));
        setSnap_date(java.sql.Timestamp.valueOf(argin[2]));
        attribute_List = new ArrayList<SnapAttributeExtract>((argin.length - 3) / 7);

        int k = 3;

        while (k < argin.length) {
            String[] snapAttributeExtractArray = { argin[k], argin[k + 1], argin[k + 2], argin[k + 3], argin[k + 4],
                    argin[k + 5], argin[k + 6] };
            SnapAttributeExtract snapAttributeExtract = new SnapAttributeExtract(snapAttributeExtractArray);
            attribute_List.add(snapAttributeExtract);
            k = k + 7;
        }
    }

    public static Snapshot getPartialSnapShot(String[] argin, int snapId,
            SnapAttributeExtract[] snapAttributeExtractArray) throws SnapshotingException {
        Snapshot snapShot = new Snapshot();
        snapShot.attribute_List = new ArrayList<SnapAttributeExtract>();

        SnapshotingException snapExcept = new SnapshotingException("CONFIGURATION_ERROR", "Wrong parameter",
                ErrSeverity.ERR, "SnapShot.getPartialSnapShot", "SnapManager.SetEquipments");

        snapShot.id_snap = snapId;

        int option = 0;
        int attributesNumberToSet = 0;
        if (SnapConst.STORED_READ_VALUE.equals(argin[1]) || SnapConst.STORED_WRITE_VALUE.equals(argin[1])) {
            option = 1;
        } else {
            try {
                attributesNumberToSet = Integer.parseInt(argin[1]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw snapExcept;
            }
            option = 2;
        }

        try {
            switch (option) {
                case 1:
                    String storedValueType = argin[1];

                    if (SnapConst.STORED_READ_VALUE.equals(storedValueType)) {
                        for (int i = 0; i < snapAttributeExtractArray.length; i++) {
                            if (snapAttributeExtractArray[i].getWritable() != AttrWriteType._WRITE
                                    && snapAttributeExtractArray[i].getWritable() != AttrWriteType._READ) {
                                snapAttributeExtractArray[i].setWriteValue(snapAttributeExtractArray[i].getReadValue(),
                                        snapAttributeExtractArray[i].getNullElementsRead());
                            }
                            snapShot.attribute_List.add(snapAttributeExtractArray[i]);

                        }

                    } else if (SnapConst.STORED_WRITE_VALUE.equals(storedValueType)) {
                        for (int i = 0; i < snapAttributeExtractArray.length; i++) {
                            if (snapAttributeExtractArray[i].getWritable() != AttrWriteType._READ) {
                                snapShot.attribute_List.add(snapAttributeExtractArray[i]);
                            }
                        }
                    }
                    break;

                case 2:
                    if (argin.length <= 2) {
                        throw snapExcept;
                    }
                    // int attributesNumberToSet = Integer.parseInt(argin[1]);
                    int attributesNumber = 0, arginI = 2;

                    do {
                        for (int snapAttrI = 0; snapAttrI < snapAttributeExtractArray.length
                                && attributesNumber < attributesNumberToSet && arginI < argin.length; snapAttrI++) {

                            if (snapAttributeExtractArray[snapAttrI].getAttributeCompleteName()
                                    .equals(argin[arginI + 1])) {
                                if (snapAttributeExtractArray[snapAttrI].getWritable() != AttrWriteType._READ) {
                                    Object newValue = null;
                                    Object nullElements = null;

                                    if (SnapConst.NEW_VALUE.equals(argin[arginI])) {
                                        newValue = snapAttributeExtractArray[snapAttrI].getNewValue(argin[arginI + 2]);
                                    } else if (SnapConst.STORED_READ_VALUE.equals(argin[arginI])) {
                                        newValue = snapAttributeExtractArray[snapAttrI].getReadValue();
                                        nullElements = snapAttributeExtractArray[snapAttrI].getNullElementsRead();
                                    } else if (SnapConst.STORED_WRITE_VALUE.equals(argin[arginI])) {
                                        newValue = snapAttributeExtractArray[snapAttrI].getWriteValue();
                                        nullElements = snapAttributeExtractArray[snapAttrI].getNullElementsWrite();
                                    }

                                    snapAttributeExtractArray[snapAttrI].setWriteValue(newValue, nullElements);
                                    snapShot.attribute_List.add(snapAttributeExtractArray[snapAttrI]);
                                }
                                attributesNumber++;

                                if (SnapConst.NEW_VALUE.equals(argin[arginI])) {
                                    arginI += 3;
                                } else {
                                    arginI += 2;
                                }
                            }

                        }
                    } while (attributesNumber < attributesNumberToSet && arginI < argin.length);
                    break;

                default:
                    break;
            }

        } catch (SnapshotingException e) {
            throw e;
        } catch (Throwable t) {
            // Tools.printIfDevFailed ( t );
            if (t instanceof DevFailed) {
                // throw (DevFailed) t;
            } else {
                throw snapExcept;
            }
        }

        return snapShot;
    }

    /**
     * Returns the identifier of the context associated to this Snapshot
     * 
     * @return the identifier of the context associated to this Snapshot
     */
    public int getId_context() {
        return id_context;
    }

    /**
     * Sets the identifier of the context associated to this Snapshot
     * 
     * @param id_context
     *            the identifier of the context associated to this Snapshot
     */
    public void setId_context(int id_context) {
        this.id_context = id_context;
    }

    /**
     * Returns the identifier associated to this Snapshot
     * 
     * @return the identifier associated to this Snapshot
     */
    public int getId_snap() {
        return id_snap;
    }

    /**
     * Sets the identifier associated to this Snapshot
     * 
     * @param id_snap
     *            the identifier associated to this Snapshot
     */
    public void setId_snap(int id_snap) {
        this.id_snap = id_snap;
    }

    public boolean isFiltered() {
        return filtered;
    }

    public void setFiltered(boolean filtered) {
        this.filtered = filtered;
    }

    /**
     * Returns the timestamp associated to this Snapshot
     * 
     * @return the timestamp associated to this Snapshot
     */
    public java.sql.Timestamp getSnap_date() {
        return snap_date;
    }

    /**
     * Sets the timestamp associated to this Snapshot
     * 
     * @param snap_date
     *            the timestamp associated to this Snapshot
     */
    public void setSnap_date(java.sql.Timestamp snap_date) {
        this.snap_date = snap_date;
    }

    /**
     * Returns the attribute list associated to this Snapshot
     * 
     * @return the attribute list associated to this Snapshot
     */
    public List<SnapAttributeExtract> getAttribute_List() {
        return attribute_List;
    }

    /**
     * Sets the attribute list associated to this Snapshot
     * 
     * @param attribute_List
     *            the attribute list associated to this Snapshot
     */
    public void setAttribute_List(List<SnapAttributeExtract> attribute_List) {
        this.attribute_List = attribute_List;
    }

    /**
     * Returns an array representation of the object <I>Snapshot</I>. In this
     * order, array fields are :
     * <ol>
     * <li>the <I>identifier of the associated context</I>,
     * <li>the <I>identifier of this snapshot</I>,
     * <li>the <I>Timestamp</I> asociated to this snapshot
     * <li>The <I>Attribute list associated to this snapshot</I> (with all the
     * needed informations (data type, data format, writable))
     * </ol>
     * 
     * @return an array representation of the object <I>SnapContext</I>.
     */
    public String[] toArray() {
        String[] snapShot;
        snapShot = new String[3 + 7 * attribute_List.size()]; // Multiplied by
        // 5 because the
        // size of an
        // attribute is
        // 5.

        snapShot[0] = Integer.toString(id_context);
        snapShot[1] = Integer.toString(id_snap);
        snapShot[2] = snap_date.toString();
        int k = 3;

        for (int i = 0; i < attribute_List.size(); i++) {
            SnapAttributeExtract snapAttributeExtract = attribute_List.get(i);
            String[] snapAttributeExtractArray = snapAttributeExtract.toArray();
            snapShot[k] = snapAttributeExtractArray[0];
            snapShot[k + 1] = snapAttributeExtractArray[1];
            snapShot[k + 2] = snapAttributeExtractArray[2];
            snapShot[k + 3] = snapAttributeExtractArray[3];
            snapShot[k + 4] = snapAttributeExtractArray[4];
            snapShot[k + 5] = snapAttributeExtractArray[5];
            snapShot[k + 6] = snapAttributeExtractArray[6];
            k = k + 7;
        }
        return snapShot;
    }

    @Override
    public String toString() {
        String snapString = ObjectUtils.EMPTY_STRING;
        snapString = "SnapShot" + "\r\n" + "Associated  Context Id : \t" + id_context + "\r\n" + "Snapshot Id : \t"
                + id_snap + "\r\n" + "SnapShot time : \t" + snap_date.toString() + "\r\n" + "Attribute(s) : " + "\r\n";
        if (attribute_List != null) {
            for (int i = 0; i < attribute_List.size(); i++) {
                SnapAttributeExtract snapAttributeExtract = attribute_List.get(i);
                snapString = snapString + "\t\t" + "[" + i + "]" + "\t" + snapAttributeExtract.toString() + "\r\n";
            }
        } else {
            snapString = snapString + "\t\t" + "NULL" + "\r\n";
        }
        return snapString;
    }

}
