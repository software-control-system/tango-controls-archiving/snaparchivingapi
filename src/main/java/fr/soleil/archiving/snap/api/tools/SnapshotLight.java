package fr.soleil.archiving.snap.api.tools;

import fr.soleil.lib.project.ObjectUtils;

/**
 * SnapshotLight
 * 
 * @author chinkumo
 */
public class SnapshotLight {
    private int id_snap = -1; // Identifier for this snapshot
    private java.sql.Timestamp snap_date = null; // Timestamp asociated to this
    // snapshot
    private String comment = ObjectUtils.EMPTY_STRING;

    public SnapshotLight() {
    }

    public SnapshotLight(int id_snap, java.sql.Timestamp snap_date, String comment) {
        this.id_snap = id_snap;
        this.snap_date = snap_date;
        this.comment = comment;
    }

    public SnapshotLight(String[] argin) {
        setId_snap(Integer.parseInt(argin[0]));
        setSnap_date(java.sql.Timestamp.valueOf(argin[1]));
        setComment(argin[2]);
    }

    public java.sql.Timestamp getSnap_date() {
        return snap_date;
    }

    public void setSnap_date(java.sql.Timestamp snap_date) {
        this.snap_date = snap_date;
    }

    public int getId_snap() {
        return id_snap;
    }

    public void setId_snap(int id_snap) {
        this.id_snap = id_snap;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String[] toArray() {
        String[] snapShot;
        snapShot = new String[2];
        snapShot[0] = Integer.toString(id_snap);
        snapShot[1] = snap_date.toString();
        snapShot[2] = comment;
        return snapShot;
    }

    @Override
    public String toString() {
        String snapL = "Identifier :  " + id_snap + "\r\n" + "Record time : " + snap_date + "\r\n" + "Comment : "
                + comment;
        return snapL;
    }
}
