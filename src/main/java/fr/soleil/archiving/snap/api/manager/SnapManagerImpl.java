package fr.soleil.archiving.snap.api.manager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.Snapshot;
import fr.soleil.archiving.snap.api.tools.SnapshotLight;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Java source code for the class SnapManagerImpl.
 * 
 * @author chinkumo, GARDA
 */
public class SnapManagerImpl implements ISnapManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(SnapManagerImpl.class);

    public SnapManagerImpl() {
    }

    @Override
    public SnapAttributeExtract[] findSnapshotAttributes(final SnapshotLight snapshot) throws SnapshotingException {
        return findSnapshotAttributes(snapshot, -1);
    }

    @Override
    public SnapAttributeExtract[] findSnapshotAttributes(final SnapshotLight snapshot, final int contextID)
            throws SnapshotingException {

        // Gets the list of attributes associated to the given snapshot
        final List<SnapAttributeExtract> arrayList = SnapManagerApi.getSnapshotAssociatedAttributes(snapshot,
                contextID);

        return arrayList.toArray(new SnapAttributeExtract[arrayList.size()]);
    }

    @Override
    public SnapAttributeExtract[] getSnapValues(final int id, final String... attributeNames)
            throws SnapshotingException {
        return SnapManagerApi.getSnapValues(id, attributeNames);
    }

    @Override
    public SnapContext saveContext(final SnapContext context) throws SnapshotingException {

        // The context is registered into the database
        try {
            final int context_id = SnapManagerApi.createContext2Manager(context);
            context.setId(context_id);
            // PB : Pas d'enregistrement de la relation id_context <-> id_att
            // dans la table 'list'.
        } catch (final SnapshotingException e) {
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.ERROR_SNAPPATTERN_CREATION;
            final String reason = "Failed while executing SnapManagerApi.createContext2Manager() method...";
            final String desc = ObjectUtils.EMPTY_STRING;
            throw new SnapshotingException(message, reason, ErrSeverity.ERR, desc, ObjectUtils.EMPTY_STRING, e);
        }

        return context;
    }

    @Override
    public Snapshot launchSnapshot(final SnapContext context) throws SnapshotingException {

        int snapId = 0;
        try {
            snapId = SnapManagerApi.launchSnap(context.getId());
        } catch (final DevFailed dFEx)// Failure of 'launchSnap2Archiver'.
        {
            DevFailedUtils.printDevFailed(dFEx);
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.ERROR_LAUNCHINGSNAP;
            final String reason = "Failed while executing SnapManagerApi.launchSnap2Archiver() method...";
            String desc = "No SnapArchiver Found";
            final SnapshotingException snapEx = new SnapshotingException(message, reason, ErrSeverity.ERR, desc,
                    ObjectUtils.EMPTY_STRING);
            try {
                snapEx.initCause(dFEx);

            } catch (final IllegalStateException e) {
                LOGGER.error("CLA/Failed during initCause!");
                e.printStackTrace();
                // do nothing
            }
            final boolean computeIsDueToATimeOut = snapEx.computeIsDueToATimeOut();
            if (computeIsDueToATimeOut) {
                desc += "\n--Time out";
            }
            throw snapEx;
        }

        // Gets the snapshot identifier
        final Condition[] condition = new Condition[1];
        condition[0] = new Condition(SnapConst.ID_SNAP, SnapConst.OP_EQUALS, snapId + ObjectUtils.EMPTY_STRING);
        final Criterions criterions = new Criterions(condition);
        final SnapshotLight[] snapShotLight = findSnapshots(criterions);

        if (snapShotLight.length > 0) {
            // Gets the attributes and theirs values
            final SnapshotLight snap = snapShotLight[0];
            final List<SnapAttributeExtract> arrayList = SnapManagerApi.getSnapshotAssociatedAttributes(snap,
                    context.getId());
            final String[] tableau = { ObjectUtils.EMPTY_STRING + context.getId(),
                    ObjectUtils.EMPTY_STRING + snap.getId_snap(), ObjectUtils.EMPTY_STRING + snap.getSnap_date() };
            final Snapshot snapShot = new Snapshot(tableau);
            snapShot.setAttribute_List(arrayList);
            return snapShot;
        } else {
            // Pas de snapshot correspondant.
            final String message = SnapConst.SNAPSHOTING_ERROR_PREFIX + " : " + SnapConst.ERROR_RET_SNAP;
            final String reason = "Failed while executing SnapManagerApi.getSnapshotAssociatedAttributes() method...";
            final String desc = ObjectUtils.EMPTY_STRING;
            throw new SnapshotingException(message, reason, ErrSeverity.ERR, desc, ObjectUtils.EMPTY_STRING);
        }
    }

    @Override
    public SnapshotLight updateCommentOfSnapshot(final SnapshotLight context, final String comment)
            throws SnapshotingException {
        // checkConnection();

        // Updates the comment of the given snapshot
        final int id_snap = context.getId_snap();
        SnapManagerApi.updateSnapComment(id_snap, comment);

        // Gets the updated snapshot
        final Condition[] condition = new Condition[1];
        final String id_snap_str = ObjectUtils.EMPTY_STRING + id_snap;
        condition[0] = new Condition(SnapConst.ID_SNAP, SnapConst.OP_EQUALS, id_snap_str);
        final Criterions criterions = new Criterions(condition);

        final SnapshotLight[] updatedContexts = findSnapshots(criterions);
        return updatedContexts[0];
    }

    @Override
    public void setEquipmentsWithSnapshot(final Snapshot snapshot) throws SnapshotingException {
        SnapManagerApi.setEquipmentsWithSnapshot(snapshot);
    }

    @Override
    public SnapContext[] findContexts(final Criterions criterions) throws SnapshotingException {
        // Gets the contexts which subscribe to the given conditions
        final List<SnapContext> arrayList = SnapManagerApi.getContext(criterions);

        // Cast of the found contexts
        final SnapContext[] snapContext = arrayList.toArray(new SnapContext[arrayList.size()]);
        return snapContext;
    }

    @Override
    public SnapshotLight[] findSnapshots(final Criterions criterions) throws SnapshotingException {
        // Gets the snapshots which subscribe to the given conditions
        final List<SnapshotLight> arrayList = SnapManagerApi.getContextAssociatedSnapshots(criterions);

        // Cast of the found snapshots
        final SnapshotLight[] snapShots = new SnapshotLight[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            snapShots[i] = arrayList.get(i);
        }
        return snapShots;
    }

    @Override
    public AttributeHeavy[] findContextAttributes(final SnapContext context, final Criterions criterions)
            throws SnapshotingException {
        // Gets the attributes which are associated to the given context and
        // subscribe to the given conditions
        final List<AttributeHeavy> arrayList = SnapManagerApi.getContextAssociatedAttributes(context.getId(),
                criterions);
        return arrayList.toArray(new AttributeHeavy[arrayList.size()]);
    }

    @Override
    public SnapAttributeExtract[] getSnap(final int id) throws SnapshotingException {
        final Condition condition = new Condition(SnapConst.ID_SNAP, "=", String.valueOf(id));
        final Criterions searchCriterions = new Criterions();
        searchCriterions.addCondition(condition);
        final SnapshotLight snapshotLight = new SnapshotLight();
        snapshotLight.setId_snap(id);
        final SnapAttributeExtract[] sae = this.findSnapshotAttributes(snapshotLight);
        if (sae == null || sae.length == 0) {
            return null;
        }
        return sae;

    }

    @Override
    public String setEquipmentsWithCommand(final String cmd_name, final String option, final Snapshot snapShot)
            throws SnapshotingException {
        return SnapManagerApi.setEquipmentWithCommand(cmd_name, option, snapShot);

    }

    @Override
    public int findContextId(final int snapshotId) throws SnapshotingException {
        final int contextId = SnapManagerApi.getContextID(snapshotId);
        return contextId;
    }
}
