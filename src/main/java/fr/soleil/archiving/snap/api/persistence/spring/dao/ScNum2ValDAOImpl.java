package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import fr.soleil.archiving.snap.api.persistence.spring.dto.ScNum2Val;

@Service("scNum2ValDAO")
public class ScNum2ValDAOImpl extends AbstractValDAO<ScNum2Val> {
	public ScNum2ValDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	protected Class<ScNum2Val> getValueClass() {
		return ScNum2Val.class;
	}
}
