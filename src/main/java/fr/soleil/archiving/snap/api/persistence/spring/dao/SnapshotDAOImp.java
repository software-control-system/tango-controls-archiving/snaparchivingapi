package fr.soleil.archiving.snap.api.persistence.spring.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.soleil.archiving.snap.api.model.Snapshot;

@Repository
@Transactional
public class SnapshotDAOImp extends AbstractHibernateDAOImp<Snapshot> implements SnapshotDAO {


}
